﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Moq
Imports System.Collections.Generic

<TestClass()>
Public Class EmailSettingsCheckerTests

    <TestMethod()>
    <ExpectedException(GetType(ArgumentOutOfRangeException))>
    Public Sub GetSetting_ShouldThrowException_WhenLetterTypeGroupIsInvalid()
        Dim fakeEmailSettings As New List(Of EmailSettings)
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 0, .Invoice = True})

        Dim dataProvider As New Mock(Of IEmailSettingsDataProvider)(MockBehavior.Strict)

        dataProvider.Setup(Function(d) d.GetData()).Returns(fakeEmailSettings)
        dataProvider.Setup(Function(d) d.CustomerOptionAllowed(It.IsAny(Of Long))).Returns(True)

        Dim checker As New EmailSettingsChecker(dataProvider.Object)

        Assert.IsTrue(checker.GetSetting(0, 10000, CType(100, LetterTypeGroup)))
    End Sub

    <TestMethod()>
    Public Sub GetSetting_ShouldReturnTrue_WhenSettingIsTrue()
        Dim fakeEmailSettings As New List(Of EmailSettings)
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 0, .Invoice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 1, .LateNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 2, .PreliminaryLienNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 3, .LienSaleNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 4, .RateChangeNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 5, .InsuranceCancellationNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 6, .ReturnedCheckNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 7, .WelcomeLetter = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 8, .PartialPaymentLetter = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 9, .OneTimeLetter = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 10, .Leases = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 11, .ReceiptStandard = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 12, .ReceiptCustom = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 13, .OtherUndefined = True})

        Dim dataProvider As New Mock(Of IEmailSettingsDataProvider)(MockBehavior.Strict)

        dataProvider.Setup(Function(d) d.GetData()).Returns(fakeEmailSettings)
        dataProvider.Setup(Function(d) d.CustomerOptionAllowed(It.IsAny(Of Long))).Returns(True)

        Dim checker As New EmailSettingsChecker(dataProvider.Object)

        Assert.IsTrue(checker.GetSetting(0, 10000, LetterTypeGroup.Invoice))
        Assert.IsTrue(checker.GetSetting(1, 10000, LetterTypeGroup.LateNotice))
        Assert.IsTrue(checker.GetSetting(2, 10000, LetterTypeGroup.PreliminaryLienNotice))
        Assert.IsTrue(checker.GetSetting(3, 10000, LetterTypeGroup.LienSaleNotice))
        Assert.IsTrue(checker.GetSetting(4, 10000, LetterTypeGroup.RateChangeNotice))
        Assert.IsTrue(checker.GetSetting(5, 10000, LetterTypeGroup.InsuranceCancellationNotice))
        Assert.IsTrue(checker.GetSetting(6, 10000, LetterTypeGroup.ReturnedCheckNotice))
        Assert.IsTrue(checker.GetSetting(7, 10000, LetterTypeGroup.WelcomeLetter))
        Assert.IsTrue(checker.GetSetting(8, 10000, LetterTypeGroup.PartialPaymentLetter))
        Assert.IsTrue(checker.GetSetting(9, 10000, LetterTypeGroup.OneTimeLetter))
        Assert.IsTrue(checker.GetSetting(10, 10000, LetterTypeGroup.Leases))
        Assert.IsTrue(checker.GetSetting(11, 10000, LetterTypeGroup.ReceiptStandard))
        Assert.IsTrue(checker.GetSetting(12, 10000, LetterTypeGroup.ReceiptCustom))
        Assert.IsTrue(checker.GetSetting(13, 10000, LetterTypeGroup.OtherUndefined))
    End Sub

    <TestMethod()>
    Public Sub GetSetting_ShouldReturnFalse_WhenCustomerEmailOptionNotAllowed()
        Dim fakeEmailSettings As New List(Of EmailSettings)
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 0, .Invoice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 1, .LateNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 2, .PreliminaryLienNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 3, .LienSaleNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 4, .RateChangeNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 5, .InsuranceCancellationNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 6, .ReturnedCheckNotice = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 7, .WelcomeLetter = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 8, .PartialPaymentLetter = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 9, .OneTimeLetter = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 10, .Leases = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 11, .ReceiptStandard = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 12, .ReceiptCustom = True})
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 13, .OtherUndefined = True})

        Dim dataProvider As New Mock(Of IEmailSettingsDataProvider)(MockBehavior.Strict)

        dataProvider.Setup(Function(d) d.GetData()).Returns(fakeEmailSettings)
        dataProvider.Setup(Function(d) d.CustomerOptionAllowed(It.IsAny(Of Long))).Returns(False)

        Dim checker As New EmailSettingsChecker(dataProvider.Object)

        Assert.IsFalse(checker.GetSetting(0, 10000, LetterTypeGroup.Invoice))
        Assert.IsFalse(checker.GetSetting(1, 10000, LetterTypeGroup.LateNotice))
        Assert.IsFalse(checker.GetSetting(2, 10000, LetterTypeGroup.PreliminaryLienNotice))
        Assert.IsFalse(checker.GetSetting(3, 10000, LetterTypeGroup.LienSaleNotice))
        Assert.IsFalse(checker.GetSetting(4, 10000, LetterTypeGroup.RateChangeNotice))
        Assert.IsFalse(checker.GetSetting(5, 10000, LetterTypeGroup.InsuranceCancellationNotice))
        Assert.IsFalse(checker.GetSetting(6, 10000, LetterTypeGroup.ReturnedCheckNotice))
        Assert.IsFalse(checker.GetSetting(7, 10000, LetterTypeGroup.WelcomeLetter))
        Assert.IsFalse(checker.GetSetting(8, 10000, LetterTypeGroup.PartialPaymentLetter))
        Assert.IsFalse(checker.GetSetting(9, 10000, LetterTypeGroup.OneTimeLetter))
        Assert.IsFalse(checker.GetSetting(10, 10000, LetterTypeGroup.Leases))
        Assert.IsFalse(checker.GetSetting(11, 10000, LetterTypeGroup.ReceiptStandard))
        Assert.IsFalse(checker.GetSetting(12, 10000, LetterTypeGroup.ReceiptCustom))
        Assert.IsFalse(checker.GetSetting(13, 10000, LetterTypeGroup.OtherUndefined))
    End Sub

    <TestMethod()>
    Public Sub GetSetting_ShouldReturnFalse_WhenSettingIsFalse()
        Dim fakeEmailSettings As New List(Of EmailSettings)
        fakeEmailSettings.Add(GetAllTrueEmailSettings(0, LetterTypeGroup.Invoice))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(1, LetterTypeGroup.LateNotice))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(2, LetterTypeGroup.PreliminaryLienNotice))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(3, LetterTypeGroup.LienSaleNotice))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(4, LetterTypeGroup.RateChangeNotice))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(5, LetterTypeGroup.InsuranceCancellationNotice))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(6, LetterTypeGroup.ReturnedCheckNotice))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(7, LetterTypeGroup.WelcomeLetter))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(8, LetterTypeGroup.PartialPaymentLetter))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(9, LetterTypeGroup.OneTimeLetter))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(10, LetterTypeGroup.Leases))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(11, LetterTypeGroup.ReceiptStandard))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(12, LetterTypeGroup.ReceiptCustom))
        fakeEmailSettings.Add(GetAllTrueEmailSettings(13, LetterTypeGroup.OtherUndefined))

        Dim dataProvider As New Mock(Of IEmailSettingsDataProvider)(MockBehavior.Strict)

        dataProvider.Setup(Function(d) d.GetData()).Returns(fakeEmailSettings)
        dataProvider.Setup(Function(d) d.CustomerOptionAllowed(It.IsAny(Of Long))).Returns(True)

        Dim checker As New EmailSettingsChecker(dataProvider.Object)

        Assert.IsFalse(checker.GetSetting(0, 10000, LetterTypeGroup.Invoice))
        Assert.IsFalse(checker.GetSetting(1, 10000, LetterTypeGroup.LateNotice))
        Assert.IsFalse(checker.GetSetting(2, 10000, LetterTypeGroup.PreliminaryLienNotice))
        Assert.IsFalse(checker.GetSetting(3, 10000, LetterTypeGroup.LienSaleNotice))
        Assert.IsFalse(checker.GetSetting(4, 10000, LetterTypeGroup.RateChangeNotice))
        Assert.IsFalse(checker.GetSetting(5, 10000, LetterTypeGroup.InsuranceCancellationNotice))
        Assert.IsFalse(checker.GetSetting(6, 10000, LetterTypeGroup.ReturnedCheckNotice))
        Assert.IsFalse(checker.GetSetting(7, 10000, LetterTypeGroup.WelcomeLetter))
        Assert.IsFalse(checker.GetSetting(8, 10000, LetterTypeGroup.PartialPaymentLetter))
        Assert.IsFalse(checker.GetSetting(9, 10000, LetterTypeGroup.OneTimeLetter))
        Assert.IsFalse(checker.GetSetting(10, 10000, LetterTypeGroup.Leases))
        Assert.IsFalse(checker.GetSetting(11, 10000, LetterTypeGroup.ReceiptStandard))
        Assert.IsFalse(checker.GetSetting(12, 10000, LetterTypeGroup.ReceiptCustom))
        Assert.IsFalse(checker.GetSetting(13, 10000, LetterTypeGroup.OtherUndefined))
    End Sub


    <TestMethod()>
    Public Sub GetSetting_ShouldReturnFalse_WhenRentalHasNoSettings()
        Dim fakeEmailSettings As New List(Of EmailSettings)
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 1, .Invoice = True})

        Dim dataProvider As New Mock(Of IEmailSettingsDataProvider)(MockBehavior.Strict)

        dataProvider.Setup(Function(d) d.GetData()).Returns(fakeEmailSettings)
        dataProvider.Setup(Function(d) d.CustomerOptionAllowed(It.IsAny(Of Long))).Returns(True)

        Dim checker As New EmailSettingsChecker(dataProvider.Object)

        Assert.IsFalse(checker.GetSetting(0, 10000, LetterTypeGroup.Invoice))
    End Sub

    <TestMethod()>
    Public Sub GetSetting_ShouldReturnFalse_WhenRentalIDIsNothing()
        Dim fakeEmailSettings As New List(Of EmailSettings)
        fakeEmailSettings.Add(New EmailSettings With {.RentalID = 0, .Invoice = True})

        Dim dataProvider As New Mock(Of IEmailSettingsDataProvider)(MockBehavior.Strict)

        dataProvider.Setup(Function(d) d.GetData()).Returns(fakeEmailSettings)
        dataProvider.Setup(Function(d) d.CustomerOptionAllowed(It.IsAny(Of Long))).Returns(True)

        Dim checker As New EmailSettingsChecker(dataProvider.Object)

        Assert.IsFalse(checker.GetSetting(Nothing, 10000, LetterTypeGroup.Invoice))
    End Sub


#Region "Helper Methods"

    Private Function GetAllTrueEmailSettings(ByVal rentalID As Long, ByVal falseLetterTypeGroup As LetterTypeGroup) As EmailSettings
        Dim emailSettings As New EmailSettings With {.RentalID = rentalID}

        emailSettings.Invoice = falseLetterTypeGroup <> LetterTypeGroup.Invoice
        emailSettings.LateNotice = falseLetterTypeGroup <> LetterTypeGroup.LateNotice
        emailSettings.PreliminaryLienNotice = falseLetterTypeGroup <> LetterTypeGroup.PreliminaryLienNotice
        emailSettings.LienSaleNotice = falseLetterTypeGroup <> LetterTypeGroup.LienSaleNotice
        emailSettings.RateChangeNotice = falseLetterTypeGroup <> LetterTypeGroup.RateChangeNotice
        emailSettings.InsuranceCancellationNotice = falseLetterTypeGroup <> LetterTypeGroup.InsuranceCancellationNotice
        emailSettings.ReturnedCheckNotice = falseLetterTypeGroup <> LetterTypeGroup.ReturnedCheckNotice
        emailSettings.WelcomeLetter = falseLetterTypeGroup <> LetterTypeGroup.WelcomeLetter
        emailSettings.PartialPaymentLetter = falseLetterTypeGroup <> LetterTypeGroup.PartialPaymentLetter
        emailSettings.OneTimeLetter = falseLetterTypeGroup <> LetterTypeGroup.OneTimeLetter
        emailSettings.Leases = falseLetterTypeGroup <> LetterTypeGroup.Leases
        emailSettings.ReceiptStandard = falseLetterTypeGroup <> LetterTypeGroup.ReceiptStandard
        emailSettings.ReceiptCustom = falseLetterTypeGroup <> LetterTypeGroup.ReceiptCustom
        emailSettings.OtherUndefined = falseLetterTypeGroup <> LetterTypeGroup.OtherUndefined

        Return emailSettings
    End Function

#End Region


End Class