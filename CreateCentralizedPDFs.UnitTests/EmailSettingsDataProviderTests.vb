﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Moq

<TestClass()>
Public Class EmailSettingsDataProviderTests

#If DEBUG Then

    <TestMethod()>
    Public Sub GetData_ShouldLoadData()
        Dim rentalID = 1003197288L
        Dim siteID = 1000000384L
        Dim dataProvder As New EmailSettingsDataProvider(New Long?() {rentalID}, New Long() {siteID})

        Dim results = dataProvder.GetData()

        Assert.IsTrue(results.Count > 0)

        With results.First()
            Assert.AreEqual(rentalID, .RentalID)
        End With
    End Sub

    <TestMethod()>
    Public Sub GetData_ShouldNotLoadData_WhenRentalIDNotFound()
        Dim rentalID = 0L
        Dim siteID = 1000000384L
        Dim dataProvder As New EmailSettingsDataProvider(New Long?() {rentalID}, New Long() {siteID})

        Dim results = dataProvder.GetData()

        Assert.AreEqual(0, results.Count())
    End Sub

    <TestMethod()>
    Public Sub GetData_ShouldNotLoadData_WhenRentalIDIsNull()
        Dim rentalID As Long? = Nothing
        Dim siteID = 1000000384L
        Dim dataProvder As New EmailSettingsDataProvider(New Long?() {rentalID}, New Long() {siteID})

        Dim results = dataProvder.GetData()

        Assert.AreEqual(0, results.Count())
    End Sub

    <TestMethod()>
    Public Sub CustomerOptionAllowed_ShouldReturnTrue_WhenRuleIsSetToTrue()
        Dim rentalID = 1003197256L
        Dim siteID = 1000000384L
        Dim dataProvder As New EmailSettingsDataProvider(New Long?() {rentalID}, New Long() {siteID})

        Assert.IsTrue(dataProvder.CustomerOptionAllowed(siteID))
    End Sub

    <TestMethod()>
    Public Sub CustomerOptionAllowed_ShouldReturnFalse_WhenRuleIsSetToFalse()
        Dim rentalID = 1003197256L
        Dim siteID = 1L
        Dim dataProvder As New EmailSettingsDataProvider(New Long?() {rentalID}, New Long() {siteID})

        Assert.IsFalse(dataProvder.CustomerOptionAllowed(siteID))
    End Sub

    <TestMethod()>
    Public Sub CustomerOptionAllowed_ShouldReturnFalse_WhenSiteIDNotOriginallyPassedIn()
        Dim rentalID = 1003197256L
        Dim siteID = 1000000384L
        Dim dataProvder As New EmailSettingsDataProvider(New Long?() {rentalID}, New Long() {siteID})

        Assert.IsFalse(dataProvder.CustomerOptionAllowed(2L))
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException))>
    Public Sub Constructor_ShouldThrowException_WhenRentalIDsIsNothing()
        Dim siteID = 1000000384L
        Dim dataProvder As New EmailSettingsDataProvider(Nothing, New Long() {siteID})
    End Sub

    <TestMethod()>
    <ExpectedException(GetType(ArgumentNullException))>
    Public Sub Constructor_ShouldThrowException_WhenSiteIDsIsNothing()
        Dim rentalID = 1003197256L
        Dim dataProvder As New EmailSettingsDataProvider(New Long?() {rentalID}, Nothing)
    End Sub

#End If

End Class