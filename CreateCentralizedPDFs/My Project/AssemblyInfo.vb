Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("CreateCentralizedPDFs")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Centershift")> 
<Assembly: AssemblyProduct("STORE")> 
<Assembly: AssemblyCopyright("Copyright © Centershift 2011")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("e0fac3a9-1afd-47c7-baf4-1018d1fcfdf7")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("4.1.0.0")> 
<Assembly: AssemblyFileVersion("4.1.0.0")> 
