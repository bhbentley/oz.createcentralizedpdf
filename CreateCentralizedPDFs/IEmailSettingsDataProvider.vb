﻿Public Interface IEmailSettingsDataProvider
    Function CustomerOptionAllowed(ByVal siteID As Long) As Boolean
    Function GetData() As List(Of EmailSettings)
End Interface
