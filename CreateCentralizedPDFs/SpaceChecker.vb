﻿Imports System.IO

Public Class SpaceChecker
    Private workingDirectory As String
    Private spaceRequirement As Long

    Public Sub New(workingDirectory As String, spaceRequirement As Long)
        Me.workingDirectory = workingDirectory
        Me.spaceRequirement = spaceRequirement
    End Sub

    Public Function CheckMinFree() As Boolean
        Dim freeSpace As Long = 0

        For Each d As DriveInfo In My.Computer.FileSystem.Drives
            If workingDirectory.StartsWith(d.Name, StringComparison.InvariantCultureIgnoreCase) Then
                freeSpace = d.TotalFreeSpace
                Exit For
            End If
        Next

        Return freeSpace > spaceRequirement
    End Function
End Class
