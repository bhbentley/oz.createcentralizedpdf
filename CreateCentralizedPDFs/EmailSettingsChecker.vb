﻿Imports System.Linq

Public Class EmailSettingsChecker

    Private _emailSettings As List(Of EmailSettings) = Nothing
    Private ReadOnly Property EmailSettings As List(Of EmailSettings)
        Get
            If _emailSettings Is Nothing Then
                _emailSettings = Me.EmailSettingsDataProvider.GetData()
            End If
            Return _emailSettings
        End Get
    End Property

    Private Property EmailSettingsDataProvider As IEmailSettingsDataProvider

    Public Sub New(ByVal emailSettingsDataProvider As IEmailSettingsDataProvider)
        If emailSettingsDataProvider Is Nothing Then
            Throw New ArgumentNullException("emailSettingsDataProvider")
        End If

        Me.EmailSettingsDataProvider = emailSettingsDataProvider
    End Sub

    Public Function GetSetting(ByVal rentalID As Long?, ByVal siteID As Long, ByVal letterTypeGroup As LetterTypeGroup) As Boolean
        If Not rentalID.HasValue Then
            Return False
        End If

        If Not Me.EmailSettingsDataProvider.CustomerOptionAllowed(siteID) Then
            Return False
        End If

        Return Me.EmailSettings.Where(Function(e) e.RentalID = rentalID.Value).Select(GetSelectLambda(letterTypeGroup)).FirstOrDefault()
    End Function

    Private Function GetSelectLambda(ByVal letterTypeGroup As LetterTypeGroup) As Func(Of EmailSettings, Boolean)
        Select Case letterTypeGroup
            Case CreateCentralizedPDFs.LetterTypeGroup.Invoice
                Return Function(e) e.Invoice

            Case CreateCentralizedPDFs.LetterTypeGroup.LateNotice
                Return Function(e) e.LateNotice

            Case CreateCentralizedPDFs.LetterTypeGroup.PreliminaryLienNotice
                Return Function(e) e.PreliminaryLienNotice

            Case CreateCentralizedPDFs.LetterTypeGroup.LienSaleNotice
                Return Function(e) e.LienSaleNotice

            Case CreateCentralizedPDFs.LetterTypeGroup.RateChangeNotice
                Return Function(e) e.RateChangeNotice

            Case CreateCentralizedPDFs.LetterTypeGroup.InsuranceCancellationNotice
                Return Function(e) e.InsuranceCancellationNotice

            Case CreateCentralizedPDFs.LetterTypeGroup.ReturnedCheckNotice
                Return Function(e) e.ReturnedCheckNotice

            Case CreateCentralizedPDFs.LetterTypeGroup.WelcomeLetter
                Return Function(e) e.WelcomeLetter

            Case CreateCentralizedPDFs.LetterTypeGroup.PartialPaymentLetter
                Return Function(e) e.PartialPaymentLetter

            Case CreateCentralizedPDFs.LetterTypeGroup.OneTimeLetter
                Return Function(e) e.OneTimeLetter

            Case CreateCentralizedPDFs.LetterTypeGroup.Leases
                Return Function(e) e.Leases

            Case CreateCentralizedPDFs.LetterTypeGroup.ReceiptStandard
                Return Function(e) e.ReceiptStandard

            Case CreateCentralizedPDFs.LetterTypeGroup.ReceiptCustom
                Return Function(e) e.ReceiptCustom

            Case CreateCentralizedPDFs.LetterTypeGroup.OtherUndefined
                Return Function(e) e.OtherUndefined

            Case Else
                Throw New ArgumentOutOfRangeException("letterTypeGroup")

        End Select
    End Function

End Class
