﻿Public Class EmailSettings
    Public Property RentalID As Long
    Public Property Invoice As Boolean
    Public Property LateNotice As Boolean
    Public Property PreliminaryLienNotice As Boolean
    Public Property LienSaleNotice As Boolean
    Public Property RateChangeNotice As Boolean
    Public Property InsuranceCancellationNotice As Boolean
    Public Property ReturnedCheckNotice As Boolean
    Public Property WelcomeLetter As Boolean
    Public Property PartialPaymentLetter As Boolean
    Public Property OneTimeLetter As Boolean
    Public Property Leases As Boolean
    Public Property ReceiptStandard As Boolean
    Public Property ReceiptCustom As Boolean
    Public Property OtherUndefined As Boolean
End Class
