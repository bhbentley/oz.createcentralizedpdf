﻿Public Enum LetterTypeGroup
    Invoice = 1
    LateNotice = 2
    PreliminaryLienNotice = 3
    LienSaleNotice = 4
    RateChangeNotice = 5
    InsuranceCancellationNotice = 6
    ReturnedCheckNotice = 7
    WelcomeLetter = 8
    PartialPaymentLetter = 9
    OneTimeLetter = 10
    Leases = 11
    ReceiptStandard = 12
    ReceiptCustom = 13
    OtherUndefined = 99
End Enum
