﻿Imports System.Collections.Specialized
Imports System.IO
Imports System.Linq
Imports System.Reflection
Imports System.Text.RegularExpressions

' WFO upload info: sftp.workflowone.com centapvd 2SaCleGv

Public Class Program
    Public Class Constants
        Public Class Command
            Public Const Batch As String = "Batch"
            Public Const Bundle As String = "Bundle"
            Public Const Clean As String = "Clean"
            Public Const Print As String = "Print"
            Public Const PrintTemplate As String = "PrintTemplate"
            Public Const Install As String = "Install"
            Public Const RePrint As String = "RePrint"
            Public Const RePrintTemplate As String = "RePrintTemplate"
            Public Const ReRun As String = "ReRun"
            Public Const Run As String = "Run"
            Public Const Upload As String = "Upload"
            Public Const Help As String = "Help"
        End Class

        Public Class EventLog
            Public Const ApplicationLogSource As String = "CreateCentralizedPDFs"
            Public Const StoreLogSource As String = "CentralizedLetters"
            Public Const ApplicationLog As String = "Application"
            Public Const ApplicationLogListenerName As String = "ApplicationEventLog"
            Public Const StoreLog As String = "STORE40"
            Public Const StoreLogListenerName As String = "StoreEventLog"
            Public Shared ReadOnly Logs As IDictionary(Of String, String) = New Dictionary(Of String, String)() From { _
                {Constants.EventLog.ApplicationLogListenerName, Constants.EventLog.ApplicationLog}, _
                {Constants.EventLog.StoreLogListenerName, Constants.EventLog.StoreLog} _
            }
        End Class

        Public Const HelpTextResource As String = "help.txt"
        Public Const HelpPanic As String = "No help is available"
        Public Const FileArgumentPrefix As String = "#"
        Public Const FolderArgumentPrefix As String = "@"
        Public Const InvalidFolder As String = "Folder does not exist: {0}"

        Public Const TemplateGroupName As String = "templateid"
        Public Const VersionGroupName As String = "version"
        Public Const TemplatePattern As String = "(?<" & TemplateGroupName & ">\d+):?(?<" & VersionGroupName & ">\d+)?"
        Public Shared ReadOnly TemplateRex As New Regex(TemplatePattern, RegexOptions.Compiled)
    End Class

    Public Class EventLogInfo
        Public LastWrite As DateTime
        Public IntervalStart As DateTime
        Public AttemptCount As Integer
    End Class

    Private Shared evi As New EventLogInfo()
    Private Shared etr As New ErrorTracker()

    Private ReadOnly commandMap As IDictionary(Of String, Action) = New Dictionary(Of String, Action)() From { _
        {Constants.Command.Batch.ToLowerInvariant(), AddressOf Batch}, _
        {Constants.Command.Bundle.ToLowerInvariant(), AddressOf Bundle}, _
        {Constants.Command.Clean.ToLowerInvariant(), AddressOf Clean}, _
        {Constants.Command.Print.ToLowerInvariant(), AddressOf Print}, _
        {Constants.Command.PrintTemplate.ToLowerInvariant(), AddressOf PrintTemplate}, _
        {Constants.Command.Install.ToLowerInvariant(), AddressOf Install}, _
        {Constants.Command.RePrint.ToLowerInvariant(), AddressOf RePrint}, _
        {Constants.Command.RePrintTemplate.ToLowerInvariant(), AddressOf RePrintTemplate}, _
        {Constants.Command.ReRun.ToLowerInvariant(), AddressOf ReRun}, _
        {Constants.Command.Help.ToLowerInvariant(), AddressOf Usage}, _
        {Constants.Command.Upload.ToLowerInvariant(), AddressOf Upload} _
    }

    Private args As String()
    Private processed As Letters.ProcessedFlag
    Private updateProcessed As Boolean
    Private exportId As Long
    Private templates As IList(Of Letters.TemplateKey)
    Private workingFolder As String

    Public Shared Sub Main(args As String())
        Dim prog As New Program(args)

        Environment.ExitCode = 1
        Try
            etr.ReportErrorMethod = AddressOf ReportErrorToEventLog
            prog.Run()
            Environment.ExitCode = 0
        Catch ex As Exception
            ReportError(ex)
            etr.AddError(ex)
        End Try
        etr.SendEmail()
    End Sub

    Private Shared Sub ReportError(ex As Exception)
        Dim tracer As TraceSource = Nothing

        Try
            tracer = New TraceSource(TraceSources.[Error])
            tracer.TraceEvent(TraceEventType.Error, -1, "{0}", ex)
        Finally
            If Not tracer Is Nothing Then
                tracer.Flush()
                tracer.Close()
                tracer = Nothing
            End If
        End Try
    End Sub

    Private Shared Sub ReportErrorToEventLog(ex As Exception)
        Dim tracer As TraceSource = Nothing
        Dim writeEvent As Boolean = Not LimitEventReporting()
        Dim listeners As New List(Of TraceListener)()

        Try
            tracer = New TraceSource(TraceSources.[Error])
            For Each listener As TraceListener In tracer.Listeners
                If TypeOf listener Is EventLogTraceListener Then
                    If writeEvent Then
                        ConfigureEventLogListener(DirectCast(listener, EventLogTraceListener))
                        listeners.Add(listener)
                    End If
                End If
            Next
            If listeners.Any() Then
                tracer.Listeners.Clear()
                tracer.Listeners.AddRange(listeners.ToArray())
                tracer.TraceData(TraceEventType.Critical, 1, ex)
                If 0 = evi.AttemptCount Then evi.IntervalStart = DateTime.Now
                evi.LastWrite = DateTime.Now
            End If
        Finally
            If Not tracer Is Nothing Then
                tracer.Close()
                tracer = Nothing
            End If
        End Try

        evi.AttemptCount += 1
    End Sub

    Private Shared Function LimitEventReporting() As Boolean
        Dim limitEvent As Boolean = True

        If evi.AttemptCount > My.Settings.EventLogSuppressionCount Then
            If DateTime.Now - evi.LastWrite > My.Settings.EventLogSuppressionTime Then
                evi.AttemptCount = 0
                limitEvent = False
            End If
        ElseIf DateTime.Now - evi.IntervalStart > My.Settings.EventLogSuppressionInterval Then
            evi.AttemptCount = 0
            limitEvent = False
        End If

        Return limitEvent
    End Function

    Private Shared Sub ConfigureEventLogListener(listener As EventLogTraceListener)
        Dim logName As String = Nothing
        If Constants.EventLog.Logs.TryGetValue(listener.Name, logName) Then listener.EventLog = New EventLog(logName) With {.Source = listener.EventLog.Source}
    End Sub

    Public Sub New(args As String())
        Me.args = args
        Me.processed = Letters.ProcessedFlag.Unprocessed
        Me.updateProcessed = True
        Me.exportId = 0
        Me.templates = New List(Of Letters.TemplateKey)()
        Me.workingFolder = My.Settings.WorkingFolder
    End Sub

    Public Sub Batch()
        RunBatcher()
        RunBundler()
    End Sub

    Private Sub RunBatcher()
        Dim batchInfo As New Letters.BatchInfo() With { _
            .WorkingDirectory = New DirectoryInfo(workingFolder), _
            .LetterDateRange = New Letters.DateRange() With {.StartDate = DateTime.Today.AddDays(-My.Settings.PriorDays), .EndDate = DateTime.Today}, _
            .WhiteOutType = My.Settings.WhiteOutType, _
            .MaximumPageCountPerLetterType = New Dictionary(Of Letters.LetterType, Integer)() From { _
                {Letters.LetterType.PlainLetter, My.Settings.LetterPageMax}, _
                {Letters.LetterType.CertifiedLetter, My.Settings.LetterPageMax}, _
                {Letters.LetterType.Postcard, My.Settings.PostcardPageMax} _
            }, _
            .Processed = processed, _
            .Update = updateProcessed _
        }
        Dim batcher As Letters.LetterBatcher = CType(Letters.Utility.LoadInstance(My.Settings.BatcherType), Letters.LetterBatcher)
        Dim checker As New SpaceChecker(workingFolder, My.Settings.MinFreeSpace)

        If Not checker.CheckMinFree() Then Throw New IOException(String.Format("Insufficient disk space (expect to have {0} free bytes)", My.Settings.MinFreeSpace))
        batcher.ReportErrorMethod = AddressOf ReportError
        If templates.Any() Then batchInfo.Templates = templates
        If 0 <> exportId Then
            With batchInfo
                .ExportID = exportId
                .Processed = Letters.ProcessedFlag.Unspecified
                .LetterDateRange.StartDate = DateTime.MinValue
            End With
        End If
        RunUnderObservation(batcher, batchInfo, My.Settings.BatchWatchers)
    End Sub

    Private Sub RunUnderObservation(Of T)(runner As Letters.Runnable(Of T), info As T, watchList As StringCollection)
        Dim watchers As IList(Of Watcher) = New List(Of Watcher)()

        Try
            AddWatchers(CType(runner, Letters.Watchable), watchList, watchers)
            runner.Run(info)
        Finally
            For Each w In watchers
                RemoveHandler runner.Notice, AddressOf w.HandleNotice
                w.Dispose()
            Next
            watchers.Clear()
        End Try
    End Sub

    Private Sub AddWatchers(target As Letters.Watchable, watchList As StringCollection, watchers As IList(Of Watcher))
        Dim w As Watcher

        w = New ErrorWatcher(etr)
        AddHandler target.Notice, AddressOf w.HandleNotice
        watchers.Add(w)
        For Each n In watchList
            w = CType(Letters.Utility.LoadInstance(n), Watcher)
            AddHandler target.Notice, AddressOf w.HandleNotice
            watchers.Add(w)
        Next
    End Sub

    Private Sub RunBundler()
        Dim bundleInfo As New Letters.BundleInfo() With { _
            .LettersPerBundle = My.Settings.LettersPerBundle, _
            .LetterFolderName = workingFolder, _
            .FileIdentifier = My.Settings.FileIdentifier _
        }
        Dim bundler As Letters.LetterBundler = CType(Letters.Utility.LoadInstance(My.Settings.BundlerType), Letters.LetterBundler)

        RunUnderObservation(bundler, bundleInfo, My.Settings.BundleWatchers)
    End Sub

    Public Sub Bundle()
        Dim folder As String = workingFolder

        If 1 < args.Length Then
            folder = args(1)
            If Not Directory.Exists(folder) Then Throw New ArgumentException(String.Format(Constants.InvalidFolder, folder))
            workingFolder = folder
        End If
        RunBundler()
    End Sub

    Public Sub Clean()
        Dim dead As New DirectoryInfo(workingFolder)
        If Not dead.Exists Then Return
        If 0 = StringComparer.InvariantCultureIgnoreCase.Compare(Environment.CurrentDirectory, workingFolder) Then Environment.CurrentDirectory = dead.Parent.FullName
        dead.Delete(True)
    End Sub

    Private Sub Install()
        ' Should be run in an administrative context (i.e., elevated command prompt).
        If Not EventLog.SourceExists(Constants.EventLog.ApplicationLogSource) Then EventLog.CreateEventSource(Constants.EventLog.ApplicationLogSource, Constants.EventLog.ApplicationLog)
        If Not EventLog.SourceExists(Constants.EventLog.StoreLogSource) Then EventLog.CreateEventSource(Constants.EventLog.StoreLogSource, Constants.EventLog.StoreLog)
    End Sub

    Public Sub Print()
        Dim tmp As Long

        updateProcessed = False
        If 1 < args.Length Then
            If Int64.TryParse(args(1), tmp) Then exportId = tmp
        End If
        RunBatcher()
    End Sub

    Public Sub PrintTemplate()
        Dim tmp As Long

        updateProcessed = False
        If 2 > args.Length Then
            Usage()
            Return
        End If

        If Not ParseTemplateArgument(args(1)) Then
            Usage()
            Return
        End If

        If 2 < args.Length Then
            If Int64.TryParse(args(2), tmp) Then exportId = tmp
        End If

        RunBatcher()
    End Sub

    Private Function ParseTemplateArgument(s As String) As Boolean
        Dim tmpl = ParseTemplate(s)

        If tmpl Is Nothing Then
            If Not s.StartsWith(Constants.FileArgumentPrefix) Then Return False
            Return ReadTemplateListFile(s.Substring(Constants.FileArgumentPrefix.Length))
        Else
            templates.Add(tmpl)
        End If

        Return True
    End Function

    Private Function ParseTemplate(s As String) As Letters.TemplateKey
        Dim m As Match = Constants.TemplateRex.Match(s)
        Dim tmpl As Letters.TemplateKey

        If Not m.Success Then Return Nothing
        If 0 = m.Groups(Constants.TemplateGroupName).Length Then Return Nothing
        tmpl = New Letters.TemplateKey() With {.Identifier = Int64.Parse(m.Groups(Constants.TemplateGroupName).Value), .Version = 0}
        If 0 <> m.Groups(Constants.VersionGroupName).Length Then tmpl.Version = Int32.Parse(m.Groups(Constants.VersionGroupName).Value)

        Return tmpl
    End Function

    Private Function ReadTemplateListFile(fn As String) As Boolean
        Dim fi As New FileInfo(fn)

        If Not fi.Exists() Then Return False
        Using rdr As StreamReader = fi.OpenText()
            Do Until rdr.EndOfStream
                templates.Add(ParseTemplate(rdr.ReadLine()))
            Loop
        End Using
        templates = templates.Where(Function(x) Not x Is Nothing).ToList()

        Return templates.Any()
    End Function

    Public Sub Run()
        Dim cmd As Action = Nothing

        If args Is Nothing OrElse 0 = args.Length Then args = New String() {Constants.Command.Help}
        If commandMap.TryGetValue(args(0).ToLower(), cmd) Then
            cmd()
            Return
        End If

        If 0 <> StringComparer.InvariantCultureIgnoreCase.Compare(Constants.Command.Run, args(0)) Then
            Usage()
            Return
        End If

        InternalRun()
    End Sub

    Private Sub InternalRun()
        args = args.Take(1).ToArray()
        etr.EnableSend()
        Batch()
        Upload()
        If My.Settings.CleanUpWhenDone Then Clean()
    End Sub

    Public Sub RePrint()
        processed = Letters.ProcessedFlag.Processed
        Print()
    End Sub

    Public Sub RePrintTemplate()
        processed = Letters.ProcessedFlag.Processed
        PrintTemplate()
    End Sub

    Public Sub ReRun()
        updateProcessed = False
        processed = Letters.ProcessedFlag.Processed
        InternalRun()
    End Sub

    Public Sub Upload()
        Dim settings As New Uploader.Settings() With { _
            .Server = My.Settings.UploadServer, _
            .Username = My.Settings.UploadUsername, _
            .Password = My.Settings.UploadPassword, _
            .Path = My.Settings.UploadPath, _
            .FileIdentifier = My.Settings.FileIdentifier, _
            .WorkingFolder = workingFolder, _
            .Files = args.Skip(1).ToList() _
        }

        ParseUploadArguments(settings)
        InternalUpload(settings)
    End Sub

    Private Sub ParseUploadArguments(settings As Uploader.Settings)
        Dim s As String

        If Not settings.Files.Any() Then Return
        s = settings.Files.First()
        If s.StartsWith(Constants.FolderArgumentPrefix) Then
            settings.WorkingFolder = s.Substring(Constants.FolderArgumentPrefix.Length)
            CheckFolderExists(settings.WorkingFolder)
            settings.Files = settings.Files.Skip(1).ToList()
            s = settings.Files.FirstOrDefault()
        End If

        If String.IsNullOrEmpty(s) Then Return
        If s.StartsWith(Constants.FileArgumentPrefix) Then
            ReadFileList(settings.Files.First().Substring(Constants.FileArgumentPrefix.Length), settings.Files)
            settings.Files = settings.Files.Skip(1).ToList()
        End If
    End Sub

    Private Sub CheckFolderExists(fn As String)
        If Not Directory.Exists(fn) Then Throw New ArgumentException(String.Format(Constants.InvalidFolder, fn))
    End Sub

    Private Sub ReadFileList(fn As String, files As IList(Of String))
        Dim fi As New FileInfo(fn)
        Dim s As String

        If Not fi.Exists() Then Return
        Using rdr As StreamReader = fi.OpenText()
            s = rdr.ReadLine()
            If Not String.IsNullOrWhiteSpace(s) Then files.Add(s)
        End Using
    End Sub

    Private Sub InternalUpload(settings As Uploader.Settings)
        Dim upd As New Uploader(settings)
        RunUnderObservation(upd, 0, My.Settings.UploadWatchers)
    End Sub

    Public Sub Usage()
        Dim assy As Assembly = Assembly.GetEntryAssembly()
        Dim exeName As String = Path.GetFileName(assy.Location)
        Dim progName As String = Path.GetFileNameWithoutExtension(assy.Location)
        Dim commands As IList(Of String) = New List(Of String)() From { _
                Constants.Command.Batch, _
                Constants.Command.Clean, _
                Constants.Command.Run, _
                Constants.Command.Upload, _
                Constants.Command.Help, _
                Constants.Command.Print, _
                Constants.Command.PrintTemplate, _
                Constants.Command.Bundle _
        }
        Dim helpText As String = FindHelpText(assy)
        Dim args As Object()

        If String.IsNullOrEmpty(helpText) Then
            helpText = Constants.HelpPanic
            args = New Object() {}
        Else
            ' The order of these arguments must match up with the replacement tokens
            ' in the help text resource.
            args = New Object() { _
                progName, _
                exeName, _
                String.Join(" ", commands)
            }
            args = args.Concat(commands).ToArray()
        End If

        Console.WriteLine(helpText, args)
    End Sub

    Private Function FindHelpText(assy As Assembly) As String
        Dim s As String = Nothing
        For Each rn As String In assy.GetManifestResourceNames()
            Console.WriteLine(rn)
            If rn.EndsWith(Constants.HelpTextResource) Then
                Using sr As New StreamReader(assy.GetManifestResourceStream(rn))
                    s = sr.ReadToEnd()
                End Using
                Exit For
            End If
        Next
        Return s
    End Function
End Class
