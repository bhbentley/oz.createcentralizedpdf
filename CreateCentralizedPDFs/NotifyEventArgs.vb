﻿Imports System.Linq

Public Class NotifyEventArgs
    Inherits EventArgs

    Private at As DateTime
    Private id As Integer
    Private ex As Exception
    Private eventData As IDictionary(Of String, Object)
    Private senders As IList(Of Object)

    Public Sub New(eventId As Integer)
        Init(eventId)
    End Sub

    Private Sub Init(eventId As Integer)
        at = System.DateTime.Now
        id = eventId
        eventData = Nothing
        senders = New List(Of Object)()
    End Sub

    Public Sub New(eventId As Integer, data As IDictionary(Of String, Object))
        Init(eventId)
        eventData = data
    End Sub

    Public Sub New(eventId As Integer, [error] As Exception)
        Init(eventId)
        ex = [error]
    End Sub

    Public Sub New(eventId As Integer, [error] As Exception, data As IDictionary(Of String, Object))
        Init(eventId)
        ex = [error]
        eventData = New Dictionary(Of String, Object)(data)
    End Sub

    Public ReadOnly Property DateTime As DateTime
        Get
            Return at
        End Get
    End Property

    Public ReadOnly Property EventID As Integer
        Get
            Return id
        End Get
    End Property

    Public ReadOnly Property [Error] As Exception
        Get
            Return ex
        End Get
    End Property

    Public ReadOnly Property Data As IDictionary(Of String, Object)
        Get
            Return New Dictionary(Of String, Object)(eventData)
        End Get
    End Property

    Public ReadOnly Property SenderChain As IList(Of Object)
        Get
            Return New List(Of Object)(senders)
        End Get
    End Property
End Class
