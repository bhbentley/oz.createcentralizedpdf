﻿Imports System.Linq
Imports System.Data
Imports Framework.Database.APPL
Imports Framework.Logic.Tools

Public Class EmailSettingsDataProvider
    Implements IEmailSettingsDataProvider

    Private Const CUSTOMER_EMAIL_OPTION_RULE_ID As Integer = 370

    Private rentalIDs As Long?()
    Private siteIDs As Long()

    Private _siteRuleValues As Dictionary(Of Long, Boolean)
    Private ReadOnly Property SiteRuleValues As Dictionary(Of Long, Boolean)
        Get
            If _siteRuleValues Is Nothing Then
                _siteRuleValues = LoadRuleValues()
            End If
            Return _siteRuleValues
        End Get
    End Property


    Private _emailSettings As List(Of EmailSettings)
    Private ReadOnly Property EmailSettings As List(Of EmailSettings)
        Get
            If _emailSettings Is Nothing Then
                _emailSettings = GetEmailSettings()
            End If

            Return _emailSettings
        End Get
    End Property

    Public Sub New(ByVal rentalIDs As Long?(), ByVal siteIDs As Long())
        If rentalIDs Is Nothing Then Throw New ArgumentNullException("rentalIDs")
        If siteIDs Is Nothing Then Throw New ArgumentNullException("siteIDs")

        Me.rentalIDs = rentalIDs
        Me.siteIDs = siteIDs
    End Sub

    Public Function CustomerOptionAllowed(ByVal siteID As Long) As Boolean Implements IEmailSettingsDataProvider.CustomerOptionAllowed
        If SiteRuleValues.ContainsKey(siteID) Then
            Return SiteRuleValues(siteID)
        End If

        Return False
    End Function

    Public Function GetData() As List(Of EmailSettings) Implements IEmailSettingsDataProvider.GetData
        Return Me.EmailSettings
    End Function

    Private Function LoadRuleValues() As Dictionary(Of Long, Boolean)
        Dim siteRuleValueDictionary As New Dictionary(Of Long, Boolean)

        Dim ruleValues As New RULE_VALUES.Collection
        ruleValues.LoadByPARENT_IDAndRULE_IDAndRULE_LEVEL(Me.siteIDs, RDX.DQE.BasicFilter.Operators.EqualTo, CUSTOMER_EMAIL_OPTION_RULE_ID, RDX.DQE.BasicFilter.Operators.EqualTo, RuleLevel.Site.ToString.ToUpper)

        For Each ruleValue In ruleValues
            Dim result = False
            Boolean.TryParse(ruleValue.SITE_VALUE, result)
            siteRuleValueDictionary.Add(ruleValue.PARENT_ID.Value, result)
        Next

        Return siteRuleValueDictionary
    End Function

    Private Function GetEmailSettings() As List(Of EmailSettings)
        If Me.rentalIDs.Count = 0 Then
            Return New List(Of EmailSettings)
        End If

        Dim sel As New RDX.DQE.SelectStatement("TRAN", "TRAN_EMAIL_SETTINGS")

        With RDX.DQE.Filter.Builder
            sel.Filter = .In("RENTAL_ID", Me.rentalIDs)
        End With

        Dim dt = RDX.Current.Provider.GetTable(sel)

        Return dt.AsEnumerable().Select(Function(dr) New EmailSettings With
                                                              {
                                                                  .RentalID = dr.Field(Of Long)("RENTAL_ID"),
                                                                  .Invoice = CType(dr.Field(Of Short)("INVOICE"), Boolean),
                                                                  .LateNotice = CType(dr.Field(Of Short)("LATE_NOTICE"), Boolean),
                                                                  .PreliminaryLienNotice = CType(dr.Field(Of Short)("PRE_LIEN_NOTICE"), Boolean),
                                                                  .LienSaleNotice = CType(dr.Field(Of Short)("LIEN_SALE_NOTICE"), Boolean),
                                                                  .RateChangeNotice = CType(dr.Field(Of Short)("RATE_CHANGE_NOTICE"), Boolean),
                                                                  .InsuranceCancellationNotice = CType(dr.Field(Of Short)("INS_CANCEL_NOTICE"), Boolean),
                                                                  .ReturnedCheckNotice = CType(dr.Field(Of Short)("RET_CHECK_NOTICE"), Boolean),
                                                                  .WelcomeLetter = CType(dr.Field(Of Short)("WELCOME_LETTER"), Boolean),
                                                                  .PartialPaymentLetter = CType(dr.Field(Of Short)("PART_PAY_LETTER"), Boolean),
                                                                  .OneTimeLetter = CType(dr.Field(Of Short)("ONE_TIME_LETTER"), Boolean),
                                                                  .Leases = CType(dr.Field(Of Short)("LEASES"), Boolean),
                                                                  .ReceiptStandard = CType(dr.Field(Of Short)("RECEIPT_STANDARD"), Boolean),
                                                                  .ReceiptCustom = CType(dr.Field(Of Short)("RECEIPT_CUSTOM"), Boolean),
                                                                  .OtherUndefined = CType(dr.Field(Of Short)("OTHER_UNDEFINED"), Boolean)
                                                              }).ToList()

    End Function

End Class
