﻿Namespace Letters

    Public Enum EventID
        Unknown = 0
        BatchStarted = 1001
        BatchFinished
        BatchFailed
        BadTemplate
        DroppedLetter
        BundlingStarted
        BundlingFinished
        BundleCreated
        BundleFailed
        LetterDataLoaded
        TemplatesLoaded
        LetterCreated
        LettersDone
    End Enum

    Public Class EventData
        Public Const LetterNumber As String = "LetterNumber"
        Public Const Count As String = "Count"
        Public Const Info As String = "Info"
        Public Const FileName As String = "FileName"
        Public Const TemplateKey As String = "TemplateKey"
    End Class
End Namespace
