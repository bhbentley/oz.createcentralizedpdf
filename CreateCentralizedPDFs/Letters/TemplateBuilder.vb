﻿Namespace Letters

    Public Interface TemplateBuilder
        Function Build(definition As TemplateDefinition) As LetterTemplate
    End Interface

End Namespace
