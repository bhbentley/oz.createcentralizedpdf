﻿Imports System.IO
Imports System.Text

Namespace Letters

    Public Class DateRange
        Public StartDate As Date
        Public EndDate As Date
    End Class

    <Flags()>
    Public Enum ProcessedFlag
        Unspecified = 0
        Unprocessed = 1
        Processed = 2
    End Enum

    Public Class BatchInfo
        Public WorkingDirectory As DirectoryInfo
        Public MaximumPageCountPerLetterType As IDictionary(Of LetterType, Integer)
        Public WhiteOutType As String
        Public LetterDateRange As DateRange
        Public Processed As ProcessedFlag
        Public ExportID As Long
        Public Update As Boolean
        Public Templates As IList(Of Letters.TemplateKey)
    End Class

    Public Interface LetterBatcher
        Inherits Runnable(Of BatchInfo)
        WriteOnly Property ReportErrorMethod As Action(Of Exception)
    End Interface

End Namespace
