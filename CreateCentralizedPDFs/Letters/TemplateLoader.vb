﻿Namespace Letters

    Public Interface TemplateLoader
        ReadOnly Property Definitions As IDictionary(Of TemplateKey, TemplateDefinition)
        Sub Load()
    End Interface

End Namespace
