﻿Namespace Letters

    Public Class TemplateKey
        Public Identifier As Long
        Public Version As Integer

        Public Overrides Function Equals(obj As Object) As Boolean
            If obj Is Nothing Then Return False
            Dim t As TemplateKey = TryCast(obj, TemplateKey)
            If t Is Nothing Then Return False
            Return (t.Identifier = Identifier) And (t.Version = Version)
        End Function

        Public Overrides Function GetHashCode() As Integer
            Return Identifier.GetHashCode() Xor Version.GetHashCode()
        End Function
    End Class

    Public Class FieldDefinition
        Public Identifier As Long
        Public Number As Integer
        Public Name As String
        Public IsRepeat As Boolean
        Public RepeatMax As Integer
    End Class

    Public Class TemplateDefinition
        Public pdf As Byte()
        Public Fields As IList(Of FieldDefinition)
        Public MaximumPageCountPerLetterType As IDictionary(Of Letters.LetterType, Integer)
    End Class

    Public Class TemplateFactory(Of T)
        Public Class Details
            Public Builder As TemplateBuilder
            Public Loader As TemplateLoader
            Public FieldMapper As FieldMapper(Of T)
            Public Cache As TemplateCache
        End Class

        Private definitions As IDictionary(Of TemplateKey, TemplateDefinition)
        Private builder As TemplateBuilder
        Private cache As TemplateCache
        Private mapper As FieldMapper(Of T)

        Public Sub New(impl As Details)
            impl.Loader.Load()
            definitions = impl.Loader.Definitions
            Me.builder = impl.Builder
            Me.cache = impl.Cache
            Me.mapper = impl.FieldMapper
        End Sub

        Public Function CreateTemplate(key As TemplateKey) As LetterTemplate
            Dim tmpl As LetterTemplate = cache.GetTemplate(key)

            If tmpl Is Nothing Then
                Dim definition As TemplateDefinition = Nothing

                If definitions.TryGetValue(key, definition) Then
                    tmpl = builder.Build(definition)
                    cache.Add(key, tmpl)
                End If
            End If

            Return tmpl
        End Function

        Public Function CreateFieldMapper(key As TemplateKey) As FieldMapper(Of T)
            Dim definition As TemplateDefinition = Nothing

            If definitions.TryGetValue(key, definition) Then
                mapper.Init(key, definition.Fields)
                Return mapper
            End If

            Return Nothing
        End Function

    End Class

End Namespace
