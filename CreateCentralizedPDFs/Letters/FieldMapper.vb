﻿Namespace Letters

    Public Interface FieldMapper(Of T)
        Sub Init(key As TemplateKey, definitions As IList(Of FieldDefinition))
        Function Map(letterData As T) As FieldData
    End Interface

End Namespace
