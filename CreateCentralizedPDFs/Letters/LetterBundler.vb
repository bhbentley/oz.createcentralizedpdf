﻿Namespace Letters

    Public Class BundleInfo
        Public LetterFolderName As String
        Public LettersPerBundle As Integer
        Public FileIdentifier As String
    End Class

    Public Interface LetterBundler
        Inherits Runnable(Of BundleInfo)
    End Interface

End Namespace
