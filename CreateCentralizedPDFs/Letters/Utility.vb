﻿Imports System.Reflection

Namespace Letters

    Public Module Utility

        Public Function LoadInstance(typeName As String) As Object
            Dim o As Object = Nothing
            Dim assy As Assembly

            If String.IsNullOrEmpty(typeName) Then Return Nothing
            assy = FindAssembly(typeName)
            If Not assy Is Nothing Then o = assy.CreateInstance(typeName)
            Return o
        End Function

        Private Function FindAssembly(typeName As String) As Assembly
            Dim assy As Assembly = Nothing

            For Each a As Assembly In AppDomain.CurrentDomain.GetAssemblies()
                Dim t As Type = a.GetType(typeName, False, True)
                If Not t Is Nothing Then
                    assy = a
                    Exit For
                End If
            Next

            Return assy
        End Function

    End Module

End Namespace
