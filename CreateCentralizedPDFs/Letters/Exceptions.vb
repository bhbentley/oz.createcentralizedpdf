﻿Imports System.Runtime.Serialization

Namespace Letters

    Public Class TemplateException
        Inherits Exception

        Public Sub New()
            MyBase.new()
        End Sub

        Public Sub New(message As String)
            MyBase.new(message)
        End Sub

        Protected Sub New(info As SerializationInfo, context As StreamingContext)
            MyBase.new(info, context)
        End Sub

        Public Sub New(message As String, exception As Exception)
            MyBase.new(message, exception)
        End Sub
    End Class

End Namespace
