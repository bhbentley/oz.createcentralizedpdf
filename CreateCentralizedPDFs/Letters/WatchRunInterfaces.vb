﻿Namespace Letters

    Public Interface Watchable
        Event Notice(sender As Object, e As NotifyEventArgs)
    End Interface

    Public Interface Runnable(Of T)
        Inherits Watchable

        Sub Run(info As T)
    End Interface

End Namespace
