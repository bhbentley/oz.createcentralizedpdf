﻿Namespace Letters

    Public Class FieldData
        Public RegularFields As IDictionary(Of String, String)
        Public RepeatingFields As IDictionary(Of String, IList(Of String))
    End Class

    Public Interface LetterTemplate
        Inherits IDisposable
        Function MakeLetter(type As LetterType, data As FieldData, replaceLabel As Boolean) As Letter
    End Interface

End Namespace