﻿Namespace Letters

    Public Interface TemplateCache
        Sub Add(key As TemplateKey, tmpl As LetterTemplate)
        Sub Remove(key As TemplateKey)
        Function GetTemplate(key As TemplateKey) As LetterTemplate
    End Interface

    Public Class MostRecentTemplateCache
        Implements TemplateCache

        Private key As TemplateKey
        Private tmpl As LetterTemplate

        Public Sub New()
            Me.key = New TemplateKey()
            Me.tmpl = Nothing
        End Sub

        Public Sub Add(key As TemplateKey, tmpl As LetterTemplate) Implements TemplateCache.Add
            If Not Me.key.Equals(key) Then
                Remove(Me.key)
                Me.key = key
                Me.tmpl = tmpl
            End If
        End Sub

        Public Function GetTemplate(key As TemplateKey) As LetterTemplate Implements TemplateCache.GetTemplate
            If Me.key.Equals(key) Then Return tmpl
            Return Nothing
        End Function

        Public Sub Remove(key As TemplateKey) Implements TemplateCache.Remove
            key = Nothing
            If Not tmpl Is Nothing Then tmpl.Dispose()
            tmpl = Nothing
        End Sub
    End Class

End Namespace
