﻿Imports System.Drawing
Imports System.Linq
Imports System.Text

Namespace Letters
    Namespace Pdf

        Public Class FieldReplacement
            Public Class PageInfo
                Public LetterType As LetterType
                Public PageProperties As Page.Props
                Public PageNumber As Integer
            End Class
            Public Info As PageInfo
            Public ChunkBounds As RectangleF
            Public Fields As IList(Of IDictionary(Of String, String))
        End Class

        Public Interface ContentChunk
            ReadOnly Property Length As Integer
            Function CopyTo(b As Byte(), index As Integer) As Integer
            Sub ReplaceFields(replacement As FieldReplacement, replaceLabel As Boolean)
            Sub Render(fonts As IDictionary(Of String, FontObject))
            Function Replicate() As ContentChunk
            Sub SetHorizontalBounds(left As Single, width As Single, wrap As Single)
        End Interface

        Public Class LiteralChunk
            Implements ContentChunk

            Private buffer As Byte()
            Private pos As Integer
            Private len As Integer

            Public Sub New(content As Byte(), start As Integer, length As Integer)
                Me.buffer = content
                Me.pos = start
                Me.len = length
            End Sub

            Public ReadOnly Property Length As Integer Implements ContentChunk.Length
                Get
                    Return len
                End Get
            End Property

            Public Function CopyTo(b As Byte(), index As Integer) As Integer Implements ContentChunk.CopyTo
                Array.Copy(buffer, pos, b, index, len)
                Return len
            End Function

            Public Sub ReplaceFields(replacement As FieldReplacement, replaceLabel As Boolean) Implements ContentChunk.ReplaceFields
            End Sub

            Public Sub Render(fonts As IDictionary(Of String, FontObject)) Implements ContentChunk.Render
            End Sub

            Public Function Replicate() As ContentChunk Implements ContentChunk.Replicate
                Return Me
            End Function

            Public Sub SetHorizontalBounds(left As Single, width As Single, wrap As Single) Implements ContentChunk.SetHorizontalBounds
            End Sub
        End Class

        Public Interface WhiteOutChunk
            Inherits ContentChunk
            Property LetterType As LetterType
            Property PageNumber As Integer
        End Interface

        Public Class WfoWhiteOutChunk
            Implements WhiteOutChunk

            Public Class Constants
                Public Const RenderPreamble As String = "1.0 1.0 1.0 RG{0}1 1 1 rg{0}1 w{0}[] 0 d{0}"
                Public Shared ReadOnly Empty As Byte() = New Byte() {}

                Public Class Letter
                    Public Const RenderPageMarks As String = "0 560 53 197 re{0}B{0}579 593 32 32 re{0}B{0}"
                    Public Shared ReadOnly RenderedMarks As Byte() = ASCIIEncoding.Default.GetBytes(String.Format(String.Join(String.Empty, Constants.RenderPreamble, RenderPageMarks), vbLf))
                End Class

                Public Class PlainLetter
                    Public Const RenderAddressLabels As String = "0 689 278 68 re{0}B{0}0 560 278 90 re{0}B{0}"
                    Public Shared ReadOnly RenderedLabels As Byte() = ASCIIEncoding.Default.GetBytes(String.Format(String.Join(String.Empty, Constants.RenderPreamble, RenderAddressLabels), vbLf))
                End Class

                Public Class CertifiedLetter
                    Public Const RenderAddressLabels As String = "0 689 257 68 re{0}B{0}0 560 278 90 re{0}B{0}257 670 157 105 re{0}B{0}"
                    Public Shared ReadOnly RenderedLabels As Byte() = ASCIIEncoding.Default.GetBytes(String.Format(String.Join(String.Empty, Constants.RenderPreamble, RenderAddressLabels), vbLf))
                End Class

                Public Class Postcard
                    Public Const RenderAddressLabels As String = "45 50 224 74 re{0}B{0}"
                    Public Shared ReadOnly RenderedLabels As Byte() = ASCIIEncoding.Default.GetBytes(String.Format(String.Join(String.Empty, Constants.RenderPreamble, RenderAddressLabels), vbLf))
                End Class

                Public Shared ReadOnly LabelWhiteOut As IDictionary(Of LetterType, Byte()) = New Dictionary(Of LetterType, Byte())() From { _
                    {LetterType.PlainLetter, PlainLetter.RenderedLabels}, _
                    {LetterType.CertifiedLetter, CertifiedLetter.RenderedLabels}, _
                    {LetterType.Postcard, Postcard.RenderedLabels} _
                }
            End Class

            Private type As LetterType
            Private number As Integer

            Public Property LetterType As LetterType Implements WhiteOutChunk.LetterType
                Get
                    Return type
                End Get
                Set(value As LetterType)
                    type = value
                End Set
            End Property

            Public Property PageNumber As Integer Implements WhiteOutChunk.PageNumber
                Get
                    Return number
                End Get
                Set(value As Integer)
                    number = value
                End Set
            End Property

            Public Function CopyTo(b() As Byte, index As Integer) As Integer Implements ContentChunk.CopyTo
                Dim rendered As Byte() = GetLabelWhiteOut()
                Dim len As Integer = rendered.Length

                If 0 <> rendered.Length Then Array.Copy(rendered, 0, b, index, rendered.Length)
                rendered = GetMarksWhiteOut()
                If 0 <> rendered.Length Then Array.Copy(rendered, 0, b, index + len, rendered.Length)
                len += rendered.Length

                Return len
            End Function

            Private Function GetLabelWhiteOut() As Byte()
                If 1 = number Then Return Constants.LabelWhiteOut(type)
                Return Constants.Empty
            End Function

            Private Function GetMarksWhiteOut() As Byte()
                If LetterType.Postcard = type Then Return Constants.Empty
                Return Constants.Letter.RenderedMarks
            End Function

            Public ReadOnly Property Length As Integer Implements ContentChunk.Length
                Get
                    Return GetLabelWhiteOut().Length + GetMarksWhiteOut().Length
                End Get
            End Property

            Public Sub Render(fonts As IDictionary(Of String, FontObject)) Implements ContentChunk.Render
            End Sub

            Public Sub ReplaceFields(replacement As FieldReplacement, replaceLabel As Boolean) Implements ContentChunk.ReplaceFields
            End Sub

            Public Function Replicate() As ContentChunk Implements ContentChunk.Replicate
                Return New WfoWhiteOutChunk() With {.LetterType = Me.LetterType, .PageNumber = Me.PageNumber}
            End Function

            Public Sub SetHorizontalBounds(left As Single, width As Single, wrap As Single) Implements ContentChunk.SetHorizontalBounds
            End Sub
        End Class

        Public Class TextChunk
            Implements ContentChunk

            Private bounds As RectangleF
            Private wrapWidth As Single
            Private paragraphs As IList(Of Paragraph)
            Private rendered As Byte()
            Private mapContents As IList(Of Integer)

            Public Sub New(bounds As RectangleF, paragraphs As IList(Of Paragraph))
                Init(bounds, paragraphs)
            End Sub

            Private Sub Init(bounds As RectangleF, paragraphs As IList(Of Paragraph))
                Me.bounds = bounds
                Me.wrapWidth = bounds.Width
                Me.paragraphs = paragraphs
                Me.rendered = Nothing
                Me.mapContents = Nothing
            End Sub

            Public Sub New(o As TextChunk)
                Init(o.bounds, o.paragraphs)
                Me.wrapWidth = o.wrapWidth
            End Sub

            Public ReadOnly Property Length As Integer Implements ContentChunk.Length
                Get
                    If rendered Is Nothing Then Throw New InvalidOperationException("Chunk not rendered")
                    Return rendered.Length
                End Get
            End Property

            Public Function CopyTo(b As Byte(), index As Integer) As Integer Implements ContentChunk.CopyTo
                If rendered Is Nothing Then Throw New InvalidOperationException("Chunk not rendered")
                If 0 <> rendered.Length Then Array.Copy(rendered, 0, b, index, rendered.Length)
                Return rendered.Length
            End Function

            Public Sub Render(fonts As IDictionary(Of String, FontObject)) Implements ContentChunk.Render
                Dim sb As New StringBuilder()
                Dim i As Integer = 1
                Dim last As Integer = ContentMap.LastOrDefault(Function(x) 0 <> x)
                Dim widths As New Tuple(Of Single, Single)(bounds.Width, CSng(IIf(bounds.Width > wrapWidth, wrapWidth, bounds.Width)))

                DrawUtility.Origin.Reset()
                For Each p In paragraphs
                    If i > last Then
                        AddEndTextMarker(sb)
                        Exit For
                    End If
                    If DrawUtility.Origin.Reflowed Then p.ForceReflow()
                    p.Render(fonts, bounds.Left, widths, sb)
                    i += 1
                Next
                If 0 <> sb.Length Then
                    ' This is not the right encoding according to the spec but it's the one used by the template to encode its content.
                    rendered = ASCIIEncoding.Default.GetBytes(sb.ToString())
                Else
                    rendered = New Byte() {}
                End If
            End Sub

            Private ReadOnly Property ContentMap As IList(Of Integer)
                Get
                    If mapContents Is Nothing Then mapContents = paragraphs.Select(Function(x, i)
                                                                                       If x.HasText Then Return i + 1
                                                                                       Return 0
                                                                                   End Function).ToList()
                    Return mapContents
                End Get
            End Property

            Private Sub AddEndTextMarker(sb As StringBuilder)
                Dim p As Paragraph = paragraphs.LastOrDefault()
                Dim et As EndTextMarker

                If p Is Nothing Then Return
                If Not p.HasEndText Then Return
                et = New EndTextMarker
                et.Render(sb)
            End Sub

            Public Sub ReplaceFields(replacement As FieldReplacement, replaceLabel As Boolean) Implements ContentChunk.ReplaceFields
                Dim newContents As Boolean = False
                ' Bounds are in the fourth quadrant (zero-zero in upper left corner, typical of computer screen graphics).
                ' We need to translate them to the first quadrant (zero-zero in bottom left corner).
                replacement.ChunkBounds = New RectangleF(bounds.Left, replacement.Info.PageProperties.PageSize.Height - bounds.Top, bounds.Width, bounds.Height)
                For Each p As Paragraph In paragraphs
                    newContents = newContents Or p.ReplaceFields(replacement, replaceLabel)
                Next
                If newContents Then mapContents = Nothing
            End Sub

            Public Function Replicate() As ContentChunk Implements ContentChunk.Replicate
                Return New TextChunk(Me)
            End Function

            Public Sub SetHorizontalBounds(left As Single, width As Single, wrap As Single) Implements ContentChunk.SetHorizontalBounds
                If bounds.IsEmpty Then
                    bounds.X = CInt(Math.Floor(left))
                    bounds.Width = CInt(Math.Ceiling(width))
                End If
                wrapWidth = wrap
            End Sub
        End Class

    End Namespace
End Namespace
