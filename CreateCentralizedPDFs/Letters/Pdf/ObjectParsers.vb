﻿Imports System.Drawing
Imports System.IO
Imports System.Linq
Imports System.Text
Imports System.Text.RegularExpressions

Namespace Letters
    Namespace Pdf
        Namespace TemplateParser

            Public Interface ObjectParser
                ReadOnly Property Order As Integer
                Sub Parse()
            End Interface

            Public Class HeaderParser
                Implements ObjectParser

                Private Const HeaderText As String = "%PDF-1.6 CS"

                Private reader As BinaryReader

                Public Sub New(reader As BinaryReader)
                    Me.reader = reader
                End Sub

                Public ReadOnly Property Order As Integer Implements ObjectParser.Order
                    Get
                        Return Constants.ParseOrder.Header
                    End Get
                End Property

                Public Sub Parse() Implements ObjectParser.Parse
                    Dim firstLine As String = Utility.ReadLine(reader)

                    If 0 <> StringComparer.InvariantCulture.Compare(HeaderText, firstLine) Then Throw New TemplateException("first line is not a proper header")
                    If reader.BaseStream.Length <= reader.BaseStream.Position Then Throw New TemplateException("no data")
                End Sub
            End Class

            Public Class PageTreeParser
                Implements ObjectParser

                Private Const CountPattern As String = "/Count (\d+)"

                Private reader As BinaryReader
                Private npages As Integer

                Public Sub New(reader As BinaryReader)
                    Me.reader = reader
                    Me.npages = 0
                End Sub

                Public ReadOnly Property PageCount As Integer
                    Get
                        Return npages
                    End Get
                End Property

                Public ReadOnly Property Order As Integer Implements ObjectParser.Order
                    Get
                        Return Constants.ParseOrder.PageTree
                    End Get
                End Property

                Public Sub Parse() Implements ObjectParser.Parse
                    Dim s As String = Utility.ReadObject(reader)
                    Dim m As Match = Regex.Match(s, CountPattern)

                    If Not m.Success Then Throw New TemplateException("invalid page tree")
                    npages = CInt(m.Groups(1).Value)
                End Sub
            End Class

            Public Class CatalogParser
                Implements ObjectParser

                Private Const PagesPattern As String = "/Pages (\d+)"

                Private reader As BinaryReader

                Public Sub New(reader As BinaryReader)
                    Me.reader = reader
                End Sub

                Public ReadOnly Property Order As Integer Implements ObjectParser.Order
                    Get
                        Return Constants.ParseOrder.Catalog
                    End Get
                End Property

                Public Sub Parse() Implements ObjectParser.Parse
                    Dim s As String = Utility.ReadObject(reader)
                    Dim m As Match = Regex.Match(s, PagesPattern)

                    If Not m.Success Then Throw New TemplateException("invalid catalog")
                End Sub
            End Class

            Public Class PageObjectParser
                Implements ObjectParser

                Private Const Pattern As String = "/Type\s+/Page\s+/MediaBox\s+\[\s+(?<mediabox>\d+\s+\d+\s+\d+\s+\d+)\s+]\s+/Parent\s+(?<parent>\d+\s+\d\s+R)\s+/Resources\s+\<\<\s+(?<resources>.*)\s+\>\>\s+/Contents\s+(?<contents>(?<contentobjectnumber>\d+)\s+\d+\s+R)"
                Private Const ProcSetToken As String = "/ProcSet"
                Private Const FontToken As String = "/Font"
                Private Const NamedObjectParts As Integer = 4

                Private reader As BinaryReader
                Private pages As IList(Of PageObject)

                Public Sub New(reader As BinaryReader)
                    Me.reader = reader
                    Me.pages = New List(Of PageObject)
                End Sub

                Public ReadOnly Property PageObjects As IList(Of PageObject)
                    Get
                        Return pages
                    End Get
                End Property

                Public ReadOnly Property Order As Integer Implements ObjectParser.Order
                    Get
                        Return Constants.ParseOrder.PageObjects
                    End Get
                End Property

                Public Sub Parse() Implements ObjectParser.Parse
                    Dim re As New Regex(Pattern, RegexOptions.Compiled)
                    Dim pos As Integer
                    Dim s As String
                    Dim m As Match

                    Do
                        pos = CInt(reader.BaseStream.Position)
                        s = Utility.ReadObject(reader)
                        m = re.Match(s)
                        If Not m.Success Then
                            If Not pages.Any() Then Throw New TemplateException("no pages")
                            reader.BaseStream.Seek(pos, SeekOrigin.Begin)
                            Return
                        End If
                        AddPageObject(m)
                    Loop While reader.BaseStream.Length > reader.BaseStream.Position
                End Sub

                Private Sub AddPageObject(m As Match)
                    Dim o As New PageObject() With { _
                        .MediaBox = ParseMediaBox(m.Groups("mediabox").Value), _
                        .Resources = ParseResources(m.Groups("resources").Value), _
                        .Content = CInt(m.Groups("contentobjectnumber").Value) _
                    }
                    pages.Add(o)
                End Sub

                Private Function ParseMediaBox(s As String) As BoundingBox
                    Dim parts As String() = s.Split(" "c)
                    Dim box As New BoundingBox() With { _
                        .LowerLeft = New PointF() With { _
                            .x = CSng(parts(0)), _
                            .y = CSng(parts(1)) _
                        }, _
                        .UpperRight = New PointF() With { _
                            .x = CSng(parts(2)), _
                            .y = CSng(parts(3)) _
                        } _
                    }

                    Return box
                End Function

                Private Function ParseResources(s As String) As Resources
                    Dim r As New Resources() With { _
                        .ExternalObjects = ParseNamedObjects(s, Constants.Token.ExternalObject), _
                        .Fonts = ParseNamedObjects(s, FontToken), _
                        .ProcessingSet = ParseProcSet(s) _
                    }
                    Return r
                End Function

                Private Function ParseNamedObjects(s As String, token As String) As IList(Of NamedObject)
                    Dim xol As New List(Of NamedObject)()
                    Dim parts As IList(Of String) = Utility.GetDictionary(s, token)
                    Dim skip As Integer = 0
                    Dim xo As String()

                    Do Until parts.Count = skip
                        xo = parts.Skip(skip).Take(NamedObjectParts).ToArray()
                        xol.Add(New NamedObject() With { _
                            .Name = xo(0), _
                            .ObjectNumber = CInt(xo(1)) _
                        })
                        skip += NamedObjectParts
                    Loop

                    Return xol
                End Function

                Private Function ParseProcSet(s As String) As IList(Of String)
                    Return Utility.GetArray(s, ProcSetToken)
                End Function
            End Class

            Public Class ImageStreamParser
                Implements ObjectParser

                Private Const FilenamePattern As String = "(?<objectnumber>\d+)\s+\d+\s+obj\s+%FILENAME:"
                Private Const DictionaryPattern As String = "\<\<\s+/Type\s+/XObject\s+/Subtype\s+/Image\s+/Name\s+/X\s+/Width\s+(?<width>\d+)\s+/Height\s+(?<height>\d+)\s+/BitsPerComponent\s+8\s+/ColorSpace\s+/DeviceRGB\s+/Filter\s+/DCTDecode\s+/Length\s+(?<length>\d+)\s+\>\>"

                Private reader As BinaryReader
                Private images As IList(Of ImageStream)

                Public Sub New(reader As BinaryReader)
                    Me.reader = reader
                    Me.images = New List(Of ImageStream)()
                End Sub

                Public ReadOnly Property Streams As IList(Of ImageStream)
                    Get
                        Return images
                    End Get
                End Property

                Public ReadOnly Property Order As Integer Implements ObjectParser.Order
                    Get
                        Return Constants.ParseOrder.ImageStreams
                    End Get
                End Property

                Public Sub Parse() Implements ObjectParser.Parse
                    Dim reFn As New Regex(FilenamePattern, RegexOptions.Compiled)
                    Dim reDct As New Regex(DictionaryPattern, RegexOptions.Compiled)
                    Dim pos As Integer
                    Dim s As String
                    Dim m As Match
                    Dim n As Integer, len As Integer

                    Do
                        pos = CInt(reader.BaseStream.Position)
                        s = Utility.ReadLine(reader)
                        m = reFn.Match(s)
                        If Not m.Success Then
                            reader.BaseStream.Seek(pos, SeekOrigin.Begin)
                            Return
                        End If
                        n = CInt(m.Groups("objectnumber").Value)
                        pos = CInt(reader.BaseStream.Position)
                        s = Utility.ReadStream(reader)
                        m = reDct.Match(s)
                        If Not m.Success Then Throw New TemplateException("invalid image")
                        len = CInt(m.Groups("length").Value)
                        images.Add(New ImageStream() With { _
                            .ObjectNumber = n, _
                            .Height = CSng(m.Groups("height").Value), _
                            .Width = CSng(m.Groups("width").Value), _
                            .Content = reader.ReadBytes(len) _
                        })
                        ' Take us to the end of the image object.
                        s = Utility.ReadObject(reader)
                    Loop
                End Sub
            End Class

            Public Class FontParser
                Implements ObjectParser

                Private Const Pattern As String = "(?<objectnumber>\d+)\s+\d+\s+obj\s+\<\<\s+/Type\s+/Font\s+/Subtype\s+/Type1\s+/Name\s+(?<name>/F\d+)\s+/BaseFont\s+/(?<basefont>Arial|CourierNew|TimesNewRoman),?(?<style>Bold|Italic|BoldItalic)?\s+/Encoding\s+/WinAnsiEncoding\s+\>\>"
                Private Const BoldStyle As String = "Bold"
                Private Const ItalicStyle As String = "Italic"

                Private reader As BinaryReader
                Private fonts As IList(Of FontObject)

                Public Sub New(reader As BinaryReader)
                    Me.reader = reader
                    Me.fonts = New List(Of FontObject)()
                End Sub

                Public ReadOnly Property AvailableFonts As IList(Of FontObject)
                    Get
                        Return fonts
                    End Get
                End Property

                Public ReadOnly Property Order As Integer Implements ObjectParser.Order
                    Get
                        Return Constants.ParseOrder.Fonts
                    End Get
                End Property

                Public Sub Parse() Implements ObjectParser.Parse
                    Dim re As New Regex(Pattern, RegexOptions.Compiled)
                    Dim pos As Integer
                    Dim s As String
                    Dim m As Match
                    Dim o As FontObject

                    Do
                        pos = CInt(reader.BaseStream.Position)
                        s = Utility.ReadObject(reader)
                        m = re.Match(s)
                        If Not m.Success Then
                            reader.BaseStream.Seek(pos, SeekOrigin.Begin)
                            Return
                        End If
                        o = New FontObject() With { _
                            .ObjectNumber = CInt(m.Groups("objectnumber").Value), _
                            .BaseFont = m.Groups("basefont").Value _
                        }
                        s = m.Groups("style").Value
                        If Not String.IsNullOrEmpty(s) Then
                            If s.Contains(BoldStyle) Then o.Style = o.Style Or FontStyle.Bold
                            If s.Contains(ItalicStyle) Then o.Style = o.Style Or FontStyle.Italic
                        End If
                        fonts.Add(o)
                    Loop
                End Sub
            End Class

            Public Class PageStreamParser
                Implements ObjectParser

                Private Const Pattern As String = "(?<objectnumber>\d+)\s+\d+\s+obj\s*\<\<\s*/Filter\s+/FlateDecode\s+/Length\s+(?<length>\d+)\s*\>\>"

                Private reader As BinaryReader
                Private contents As IList(Of PageStream)

                Public Sub New(reader As BinaryReader)
                    Me.reader = reader
                    Me.contents = New List(Of PageStream)()
                End Sub

                Public ReadOnly Property Streams As IList(Of PageStream)
                    Get
                        Return contents
                    End Get
                End Property

                Public ReadOnly Property Order As Integer Implements ObjectParser.Order
                    Get
                        Return Constants.ParseOrder.PageStreams
                    End Get
                End Property

                Public Sub Parse() Implements ObjectParser.Parse
                    Dim re As New Regex(Pattern, RegexOptions.Compiled)
                    Dim pos As Integer
                    Dim s As String
                    Dim m As Match
                    Dim len As Integer
                    Dim o As PageStream
                    Dim b As Byte()

                    Do
                        pos = CInt(reader.BaseStream.Position)
                        s = Utility.ReadLine(reader)
                        m = re.Match(s)
                        If Not m.Success Then
                            reader.BaseStream.Seek(pos, SeekOrigin.Begin)
                            Return
                        End If
                        len = CInt(m.Groups("length").Value)
                        s = Utility.ReadStream(reader)
                        o = New PageStream() With {.ObjectNumber = CInt(m.Groups("objectnumber").Value)}
                        b = New Byte(len) {}
                        len = Flate.Decompress(reader.ReadBytes(len), len, b)
                        ParseContent(b, len, o)
                        contents.Add(o)
                        ' Take us to the end of the page object.
                        s = Utility.ReadObject(reader)
                    Loop
                End Sub

                Private Sub ParseContent(b As Byte(), len As Integer, o As PageStream)
                    Dim p As New ContentParser(b, len)
                    p.Parse()
                    For Each chunk As ContentChunk In p.Chunks
                        o.AddChunk(chunk)
                    Next
                    o.FieldNames = p.FieldNames
                End Sub
            End Class

            Public Class UserSettingsParser
                Implements ObjectParser

                Private Const UserSettingsTag As String = "500 0 obj"

                Private reader As BinaryReader
                Private settings As UserSettings

                Public Sub New(reader As BinaryReader)
                    Me.reader = reader
                End Sub

                Public ReadOnly Property UserSettings As UserSettings
                    Get
                        Return settings
                    End Get
                End Property

                Public ReadOnly Property Order As Integer Implements ObjectParser.Order
                    Get
                        Return Constants.ParseOrder.UserSettings
                    End Get
                End Property

                Public Sub Parse() Implements ObjectParser.Parse
                    Dim pos As Integer = CInt(reader.BaseStream.Position)
                    Dim s = Utility.ReadLine(reader)
                    Dim sb As StringBuilder

                    If Not s.Contains(UserSettingsTag) Then
                        reader.BaseStream.Seek(pos, SeekOrigin.Begin)
                        Return
                    End If
                    sb = New StringBuilder(Utility.ReadObject(reader))
                    sb.Replace(Constants.Token.ObjectEnd, "")
                    settings = Deserializer.XmlToObject(Of UserSettings)(sb.ToString())
                End Sub
            End Class

            Public Class XrefParser
                Implements ObjectParser

                Private reader As BinaryReader

                Public Sub New(reader As BinaryReader)
                    Me.reader = reader
                End Sub

                Public ReadOnly Property Order As Integer Implements ObjectParser.Order
                    Get
                        Return Constants.ParseOrder.XRef
                    End Get
                End Property

                Public Sub Parse() Implements ObjectParser.Parse
                    Dim s As String = Utility.ReadEof(reader)
                End Sub
            End Class

            Public Class TrailingCommentParser
                Implements ObjectParser

                Private reader As BinaryReader

                Public Sub New(reader As BinaryReader)
                    Me.reader = reader
                End Sub

                Public ReadOnly Property Order As Integer Implements ObjectParser.Order
                    Get
                        Return Constants.ParseOrder.TrailingComments
                    End Get
                End Property

                Public Sub Parse() Implements ObjectParser.Parse
                    Dim s As String = Utility.ReadLine(reader)
                End Sub
            End Class
        End Namespace
    End Namespace
End Namespace
