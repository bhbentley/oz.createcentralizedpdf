﻿Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Text.RegularExpressions

Namespace Letters
    Namespace Pdf

        Public Class Span

            Private formatSegments As IList(Of FormattingSegment)
            Private textSegments As IList(Of TextSegment)
            Private fieldSegments As IList(Of FieldSegment)
            Private otherSegments As IList(Of Segment)
            Private scaleFactor As Single
            Private newLine As Boolean

            Public Event ScaleChanged(span As Span, scaleFactor As Single)
            Public Event AlignmentChanged(span As Span, alignment As TextAlignment)
            Public Event WordSpacingChanged(wordSpacing As Single)

            Public FontSpec As FontSpec
            Public Leading As Single

            Public ReadOnly Property IsEmpty As Boolean
                Get
                    Return 0 = textSegments.Count
                End Get
            End Property

            Public ReadOnly Property HasText As Boolean
                Get
                    Return textSegments.Any(Function(x) Not String.IsNullOrEmpty(x.Text))
                End Get
            End Property

            Public ReadOnly Property HasFont As Boolean
                Get
                    Return Not FontSpec.IsEmpty
                End Get
            End Property

            Public ReadOnly Property HasEndText As Boolean
                Get
                    Return otherSegments.Any(Function(x) TypeOf (x) Is EndTextMarker)
                End Get
            End Property

            Public ReadOnly Property IsNewLine As Boolean
                Get
                    Return newLine
                End Get
            End Property

            Public Sub New(fontSpec As FontSpec, scaleFactor As Single, leading As Single)
                Me.FontSpec = fontSpec
                Me.scaleFactor = scaleFactor
                Me.newLine = False
                Me.Leading = leading
                Me.formatSegments = New List(Of FormattingSegment)()
                Me.textSegments = New List(Of TextSegment)()
                Me.fieldSegments = New List(Of FieldSegment)()
                Me.otherSegments = New List(Of Segment)()
            End Sub

            Public Sub AddSegment(seg As Segment)
                If TypeOf seg Is FontSegment Then
                    HandleFontSegment(DirectCast(seg, FontSegment))
                ElseIf TypeOf seg Is FieldSegment Then
                    fieldSegments.Add(DirectCast(seg, FieldSegment))
                    textSegments.Add(DirectCast(seg, TextSegment))
                ElseIf TypeOf seg Is TextSegment Then
                    textSegments.Add(DirectCast(seg, TextSegment))
                ElseIf TypeOf seg Is FormattingSegment Then
                    HandleFormattingSegment(DirectCast(seg, FormattingSegment))
                ElseIf TypeOf seg Is NewLineMarker Then
                    If newLine Then Throw New TemplateException("Only one line break per line is allowed")
                    newLine = True
                Else
                    otherSegments.Add(seg)
                End If
            End Sub

            Private Sub HandleFontSegment(seg As FontSegment)
                FontSpec = New FontSpec(seg.FontName, seg.FontSize)
                formatSegments.Add(DirectCast(seg, FormattingSegment))
                Scale()
            End Sub

            Private Sub HandleFormattingSegment(seg As FormattingSegment)
                If TypeOf seg Is LeadingSegment Then
                    HandleLeadingSegment(DirectCast(seg, LeadingSegment))
                ElseIf TypeOf seg Is PositionSegment Then
                    HandlePositionSegment(DirectCast(seg, PositionSegment))
                ElseIf TypeOf seg Is WordSpacingSegment Then
                    HandleWordSpacingSegment(DirectCast(seg, WordSpacingSegment))
                Else
                    formatSegments.Add(seg)
                End If
            End Sub

            Private Sub HandleLeadingSegment(seg As LeadingSegment)
                If 0.0F = seg.Leading Then Return
                If 0.0F = Leading Or seg.Leading <> Leading Then
                    Leading = seg.Leading
                    formatSegments.Add(seg)
                End If
            End Sub

            Private Sub HandlePositionSegment(seg As PositionSegment)
                formatSegments.Add(seg)
                If 0.0F <> seg.ScaleFactor Then
                    Scale(seg.ScaleFactor)
                    RaiseEvent ScaleChanged(Me, seg.ScaleFactor)
                End If
                If Not TextAlignment.Unspecified = seg.Alignment Then RaiseEvent AlignmentChanged(Me, seg.Alignment)
            End Sub

            Private Sub HandleWordSpacingSegment(seg As WordSpacingSegment)
                formatSegments.Add(seg)
                RaiseEvent WordSpacingChanged(seg.WordSpacing)
            End Sub

            Private Overloads Sub Scale(scaleFactor As Single)
                Me.scaleFactor = scaleFactor
                Scale()
            End Sub

            Private Overloads Sub Scale()
                FontSpec.Scale(scaleFactor)
                For Each seg As FontSegment In formatSegments.OfType(Of FontSegment)()
                    If 0.0F < scaleFactor Then seg.FontSize = seg.OriginalFontSize * scaleFactor
                Next
            End Sub

            Public Sub ReplaceFields(replacement As FieldReplacement, replaceLabel As Boolean)
                Dim s As String = Nothing
                Dim found As Boolean

                For Each fseg As FieldSegment In fieldSegments
                    found = False
                    For Each fv As IDictionary(Of String, String) In replacement.Fields
                        If fv.TryGetValue(fseg.Name, s) Then
                            found = True
                            Exit For
                        End If
                    Next
                    If Not found Then s = String.Empty
                    fseg.Replace(s, replacement.Info, replacement.ChunkBounds.Location, replaceLabel)
                Next
            End Sub

            Public Sub CollectSegments(collector As IList(Of Segment), first As Boolean)
                AddFormattingSegments(collector, first)
                AddTextSegments(collector)
                AddOtherSegments(collector)
            End Sub

            Private Sub AddFormattingSegments(collector As IList(Of Segment), first As Boolean)
                For Each seg As Segment In formatSegments
                    If TypeOf (seg) Is LeadingSegment Then
                        CollectLeadingSegment(DirectCast(seg, LeadingSegment), collector)
                    Else
                        collector.Add(seg)
                    End If
                Next
                If first And Not formatSegments.Any(Function(x) TypeOf x Is FontSegment) Then collector.Add(New FontSegment() With {.FontName = FontSpec.FontName, .FontSize = FontSpec.FontSize})
            End Sub

            Private Sub AddTextSegments(collector As IList(Of Segment))
                For Each seg As Segment In textSegments
                    collector.Add(seg)
                Next
            End Sub

            Private Sub AddOtherSegments(collector As IList(Of Segment))
                For Each seg As Segment In otherSegments
                    collector.Add(seg)
                Next
            End Sub

            Private Sub CollectLeadingSegment(seg As LeadingSegment, collector As IList(Of Segment))
                If 0.0F = seg.Leading Then Return
                collector.Add(seg)
            End Sub

            Public Sub Render(sb As StringBuilder)
                For Each seg As Segment In formatSegments
                    seg.Render(sb)
                Next
                If newLine Then sb.Append(NewLineMarker.Constants.Token)
                sb.Append(TextSegment.Constants.TextStartToken)
                For Each seg As Segment In textSegments
                    seg.Render(sb)
                Next
                sb.Append(TextSegment.Constants.TextEndToken).Append(vbLf)
                For Each seg As Segment In otherSegments
                    seg.Render(sb)
                Next
            End Sub

            Public Sub Unescape()
                For Each seg In textSegments
                    seg.Unescape()
                Next
            End Sub
        End Class

    End Namespace
End Namespace
