﻿Imports System.Linq

Imports Framework.Database.APPL

Namespace Letters
    Namespace Pdf

        Public Class LetterLoader

            Public Class Constants
                Public Const Epoch As Date = #1/1/1999#
            End Class

            Public Class LoadSettings
                Public EarliestScheduledDate As Date
                Public ExportID As Nullable(Of Long)
                Public Processed As ProcessedFlag
                Public Templates As IList(Of TemplateKey)
            End Class

            Private settings As LoadSettings
            Private letterData As POST_CENTRALIZED_LTR_DATA.Collection

            Public Sub New(settings As LoadSettings)
                Me.settings = settings
            End Sub

            Public Function Load() As IList(Of IList(Of POST_CENTRALIZED_LTR_DATA))
                Dim noTemplates As Boolean = settings.Templates Is Nothing OrElse settings.Templates.All(Function(x) x Is Nothing)

                letterData = New POST_CENTRALIZED_LTR_DATA.Collection
                If ProcessedFlag.Unprocessed = settings.Processed Then
                    If noTemplates Then
                        LoadAllQueued()
                    Else
                        LoadQueuedForTemplate()
                    End If
                    Return GroupLetters()
                ElseIf settings.ExportID.HasValue Then
                    If noTemplates Then
                        LoadExported()
                    Else
                        LoadExportedForTemplate()
                    End If
                    Return GroupLetters()
                Else
                    LoadProcessed()
                    Return GroupLetters()
                End If
            End Function

            Private Sub LoadAllQueued()
                letterData.LoadBySCHEDULED( _
                    RDX.DQE.BasicFilter.Operators.GreaterThanOrEqualTo, _
                    settings.EarliestScheduledDate _
                )
            End Sub

            Private Sub LoadQueuedForTemplate()
                letterData.LoadByPROCESSEDandSCHEDULEDandEXPORT_IDandTemplates(False, _
                                                                RDX.DQE.BasicFilter.Operators.GreaterThanOrEqualTo, _
                                                                settings.EarliestScheduledDate, Nothing, _
                                                                LoadByTemplatesArgument)
            End Sub

            Private ReadOnly Property LoadByTemplatesArgument As IList(Of POST_CENTRALIZED_LTR_DATA.Template)
                Get
                    If settings.Templates Is Nothing OrElse settings.Templates.All(Function(x) x Is Nothing) Then Return Nothing
                    Return settings.Templates.Select(Function(x)
                                                         Dim tmpl As New POST_CENTRALIZED_LTR_DATA.Template() With { _
                                                             .LTR_TYPE_ID = x.Identifier, _
                                                             .VERSION = x.Version _
                                                         }
                                                         If 0 = tmpl.VERSION.Value Then tmpl.VERSION = Nothing
                                                         Return tmpl
                                                     End Function).ToList()
                End Get
            End Property

            Private Sub LoadExported()
                letterData.LoadByEXPORT_ID(settings.ExportID.Value)
            End Sub

            Private Sub LoadExportedForTemplate()
                letterData.LoadByPROCESSEDandSCHEDULEDandEXPORT_IDandTemplates(True, _
                                                                RDX.DQE.BasicFilter.Operators.GreaterThanOrEqualTo, _
                                                                Constants.Epoch, settings.ExportID, _
                                                                LoadByTemplatesArgument)
            End Sub

            Private Sub LoadProcessed()
                letterData.LoadByPROCESSEDandSCHEDULEDandEXPORT_IDandTemplates(True, _
                                                                             RDX.DQE.BasicFilter.Operators.GreaterThanOrEqualTo, _
                                                                             settings.EarliestScheduledDate, Nothing, _
                                                                             LoadByTemplatesArgument)
            End Sub

            Private Function GroupLetters() As IList(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA))
                Return letterData.GroupBy(Function(x) x.LTR_NUM) _
                                        .Select(Function(x) x.ToList()) _
                                        .OrderBy(Function(x) x.first().LTR_TYPE_ID) _
                                        .ThenBy(Function(x) x.first().VERSION) _
                                        .Cast(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)) _
                                        .ToList()
            End Function
        End Class

    End Namespace
End Namespace
