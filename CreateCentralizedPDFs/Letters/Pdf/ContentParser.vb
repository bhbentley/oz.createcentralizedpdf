﻿Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Text.RegularExpressions

Namespace Letters
    Namespace Pdf
        Namespace TemplateParser

            Public Enum ChunkType
                Unknown = 0
                Literal = 1
                Text = 2
            End Enum

            Public Class Chunk
                Public Start As Integer
                Public Length As Integer
                Public Type As ChunkType
            End Class

            Public Class ContentParser
                Public Class Constants
                    Public Shared ReadOnly PercentSign As Byte() = Encoding.UTF8.GetBytes("%")
                    Public Shared ReadOnly DrawObject As Byte() = Encoding.UTF8.GetBytes("%drwObj")
                    Public Shared ReadOnly ImageObject As Byte() = Encoding.UTF8.GetBytes("%imgObj")
                    Public Shared ReadOnly LabelObject As Byte() = Encoding.UTF8.GetBytes("%lbl")
                    Public Shared ReadOnly TextObject As Byte() = Encoding.UTF8.GetBytes("%txtObj")
                    Public Shared ReadOnly ChildSuffix As Byte() = Encoding.UTF8.GetBytes("-child")
                    Public Shared ReadOnly TextAlignment As Byte() = Encoding.UTF8.GetBytes("text-align")
                    Public Shared ReadOnly BaseFieldTag As Byte() = Encoding.UTF8.GetBytes(TextChunkParser.Constants.BaseFieldStartTag)
                    Public Shared ReadOnly FlexFieldTag As Byte() = Encoding.UTF8.GetBytes(TextChunkParser.Constants.FlexFieldStartTag)
                End Class

                Private content As Byte()
                Private contentLength As Integer
                Private allChunks As IList(Of ContentChunk)
                Private fields As ISet(Of String)

                Public ReadOnly Property FieldNames As ISet(Of String)
                    Get
                        Return fields
                    End Get
                End Property

                Public Sub New(content As Byte(), contentLength As Integer)
                    Me.content = content
                    Me.contentLength = contentLength
                    Me.allChunks = New List(Of ContentChunk)()
                    Me.fields = New HashSet(Of String)()
                End Sub

                Public Sub Parse()
                    SplitIntoChunks()
                End Sub

                Public ReadOnly Property Chunks As IList(Of ContentChunk)
                    Get
                        Return allChunks
                    End Get
                End Property

                Private Sub SplitIntoChunks()
                    Dim pos As Integer = 0
                    Dim chunk As Chunk = New Chunk() With {.Start = pos, .Type = GetInitialChunkType(pos)}
                    Dim t As ChunkType

                    Do Until contentLength = pos
                        pos = FindNextPercentSign(pos + 1)
                        t = GetChunkType(pos)
                        If ChunkType.Unknown <> t Then
                            chunk.Length = pos - chunk.Start
                            AddChunk(chunk)
                            chunk = New Chunk() With {.Start = pos, .Type = t}
                        End If
                    Loop
                    chunk.Length = contentLength - chunk.Start
                    AddChunk(chunk)
                End Sub

                Private Function GetInitialChunkType(pos As Integer) As ChunkType
                    Dim t = GetChunkType(pos)
                    If ChunkType.Text <> t Then Return ChunkType.Literal
                    Return t
                End Function

                Private Function GetChunkType(pos As Integer) As ChunkType
                    If IsTextObject(pos) Then Return ChunkType.Text
                    If IsOtherObject(pos) Then Return ChunkType.Literal
                    Return ChunkType.Unknown
                End Function

                Private Function IsTextObject(pos As Integer) As Boolean
                    Dim textObjects As IList(Of Byte()) = New List(Of Byte()) From {Constants.LabelObject, Constants.TextObject}

                    For Each o In textObjects
                        If IsToken(pos, o) Then Return True
                    Next

                    Return False
                End Function

                Private Function IsToken(pos As Integer, token As Byte()) As Boolean
                    Return IsToken(pos, contentLength, token)
                End Function

                Private Function IsToken(pos As Integer, length As Integer, token As Byte()) As Boolean
                    If pos + token.Length >= length Then Return False
                    For i As Integer = 0 To token.Length - 1
                        If token(i) <> content(pos + i) Then Return False
                    Next
                    Return True
                End Function

                Private Function IsOtherObject(pos As Integer) As Boolean
                    Dim objects As IList(Of Byte()) = New List(Of Byte()) From {Constants.DrawObject, Constants.ImageObject}

                    For Each o In objects
                        If IsToken(pos, o) Then Return True
                    Next

                    Return False
                End Function

                Private Function FindNextPercentSign(pos As Integer) As Integer
                    For i As Integer = pos To contentLength - 1
                        If IsToken(i, Constants.PercentSign) Then Return i
                    Next
                    Return contentLength
                End Function

                Private Sub AddChunk(chunk As Chunk)
                    Dim o As ContentChunk = New LiteralChunk(content, chunk.Start, chunk.Length)

                    If ChunkType.Text = chunk.Type Then
                        If ContainsField(chunk) Then
                            Dim p As New TextChunkParser(ASCIIEncoding.Default.GetString(content, chunk.Start, chunk.Length))
                            o = p.Parse()
                            fields.UnionWith(p.FieldNames)
                        End If
                    End If
                    allChunks.Add(o)
                End Sub

                Private Function ContainsField(chunk As Chunk) As Boolean
                    Dim len As Integer = chunk.Start + chunk.Length

                    For i As Integer = chunk.Start To len - 1
                        If IsToken(i, len, Constants.BaseFieldTag) Then Return True
                        If IsToken(i, len, Constants.FlexFieldTag) Then Return True
                    Next

                    Return False
                End Function
            End Class

            Public Class TextChunkParser
                Public Class Constants
                    Public Const WidthAdjustment As Integer = -4
                    Public Const HeaderPattern As String = "^%((?<label>lbl) \d+ Obj|txtObj)((?<textbox>-child)?\s+w=(?<width>-?\d+) h=(?<height>-?\d+) x=(?<left>-?\d+) y=(?<top>-?\d+))?" & vbCr
                    Public Const TextStart As String = TextSegment.Constants.TextStartToken
                    Public Const TextEnd As String = TextSegment.Constants.TextEndToken
                    Public Const LineBreak As String = NewLineMarker.Constants.Token
                    Public Const Space As String = " "
                    Public Const Escape As Char = "\"c
                    Public Const TagOpen As Char = "<"c
                    Public Const BaseFieldStartTag As String = "<base>"
                    Public Const BaseFieldEndTag As String = "</base>"
                    Public Const FlexFieldStartTag As String = "<flex>"
                    Public Const FlexFieldEndTag As String = "</flex>"
                    Public Const LineContinuation As String = "ð"
                    Public Const EndOfText As String = EndTextMarker.Constants.Token
                    Public Const LeadingOperator As String = FormattingSegment.Constants.Token.Leading
                    Public Const WordSpacingOperator As String = FormattingSegment.Constants.Token.WordSpacing
                    Public Const FontOperator As String = FormattingSegment.Constants.Token.Font
                    Public Const ColorOperator As String = "rg"
                    Public Const TextMatrixOperator As String = FormattingSegment.Constants.Token.Matrix
                    Public Const LeftAlignment As String = "%text-align:Left"
                    Public Const CenterAlignment As String = "%text-align:Center"
                    Public Const RightAlignment As String = "%text-align:Right"
                    Public Shared ReadOnly Formats As IList(Of String) = New List(Of String)() From { _
                        Constants.ColorOperator, _
                        Constants.WordSpacingOperator, _
                        Constants.LeadingOperator, _
                        Constants.FontOperator, _
                        Constants.TextMatrixOperator, _
                        Constants.LeftAlignment, _
                        Constants.CenterAlignment, _
                        Constants.RightAlignment _
                    }
                End Class

                Public Enum ParseState
                    Eol = 0
                    Format = 1
                    Text = 2
                    LineBreak = 3
                    BaseField = 4
                    FlexField = 5
                End Enum

                Private Shared ReadOnly FieldTags As IDictionary(Of ParseState, Tuple(Of String, String)) = New Dictionary(Of ParseState, Tuple(Of String, String))() From { _
                    {ParseState.BaseField, New Tuple(Of String, String)(Constants.BaseFieldStartTag, Constants.BaseFieldEndTag)}, _
                    {ParseState.FlexField, New Tuple(Of String, String)(Constants.FlexFieldStartTag, Constants.FlexFieldEndTag)} _
                }

                Private s As String
                Private fmtp As FormatParser
                Private bounds As Rectangle
                Private label As Boolean
                Private paragraphs As IList(Of Paragraph)
                Private state As ParseState
                Private pos As Integer
                Private mark As Integer
                Private done As Boolean
                Private paragraph As Paragraph
                Private formatSegments As IList(Of FormattingSegment)
                Private isContinuation As Boolean
                Private isLineBreak As Boolean
                Private isEmptyTextSegment As Boolean
                Private fields As ISet(Of String)

                Public ReadOnly Property FieldNames As ISet(Of String)
                    Get
                        Return fields
                    End Get
                End Property

                Public Sub New(s As String)
                    Me.s = s
                    Me.fmtp = New FormatParser()
                    Me.paragraphs = New List(Of Paragraph)()
                    Me.state = ParseState.Eol
                    Me.pos = 0
                    Me.done = False
                    Me.formatSegments = New List(Of FormattingSegment)()
                    Me.isContinuation = False
                    Me.isLineBreak = False
                    Me.isEmptyTextSegment = False
                    Me.fields = New HashSet(Of String)()
                End Sub

                Public Function Parse() As TextChunk
                    ParseHeader()
                    ParseSegments()
                    Return New TextChunk(bounds, paragraphs)
                End Function

                Private Sub ParseHeader()
                    Dim re As New Regex(Constants.HeaderPattern)
                    Dim m As Match = re.Match(s)

                    If Not m.Success Then Throw New TemplateException("not a text chunk")
                    label = Not String.IsNullOrEmpty(m.Groups("label").Value)
                    If Not String.IsNullOrEmpty(m.Groups("width").Value) Then
                        bounds.Location = New Point(CInt(m.Groups("left").Value), CInt(m.Groups("top").Value))
                        bounds.Size = New Size(CInt(m.Groups("width").Value) + Constants.WidthAdjustment, CInt(m.Groups("height").Value))
                    End If
                End Sub

                Private Sub ParseSegments()
                    paragraph = New Paragraph()
                    Do Until done
                        Select Case state
                            Case ParseState.Format
                                HandleFormat()
                            Case ParseState.Eol
                                HandleEol()
                            Case ParseState.Text
                                HandleText()
                            Case ParseState.LineBreak
                                If Not FindTextStart() Then Throw New TemplateException("broken page content, no text start after line break")
                                isLineBreak = True
                        End Select
                    Loop
                    AddFinalParagraph()
                End Sub

                Private Sub HandleFormat()
                    formatSegments.Add(fmtp.Parse(s.Substring(mark, pos - mark)))
                    mark = pos + 1

                    If IsLineBreakNext() Then
                        state = ParseState.LineBreak
                    ElseIf IsTextStartNext() Then
                        state = ParseState.Text
                        mark = pos + 2
                    Else
                        state = ParseState.Eol
                    End If
                End Sub

                Private Function IsLineBreakNext() As Boolean
                    If s.Length - Constants.LineBreak.Length <= pos Then Return False
                    Return -1 <> s.IndexOf(Constants.LineBreak, pos + 1, Constants.LineBreak.Length)
                End Function

                Private Function IsTextStartNext() As Boolean
                    Dim i As Integer

                    If s.Length - Constants.TextStart.Length <= pos Then Return False
                    i = s.IndexOf(Constants.TextStart, pos + 1, Constants.TextStart.Length)
                    If -1 = i Then Return False
                    If Constants.Escape <> s(pos) Then Return True
                    Return False
                End Function

                Private Sub HandleEol()
                    Dim tmpMark As Integer = pos + 1

                    done = s.Length - 1 = pos
                    FindCr()

                    If done Or IsEndTextMarker() Then
                        If Not done Then
                            paragraph.AddSegment(New EndTextMarker())
                            done = True
                        End If
                        AddParagraph()
                        Return
                    End If

                    If IsFormat() Then
                        state = ParseState.Format
                        mark = tmpMark
                    ElseIf IsLineBreakNext() Then
                        state = ParseState.LineBreak
                    ElseIf IsTextStartNext() Then
                        state = ParseState.Text
                        mark = pos + 2
                    Else
                        mark = tmpMark
                    End If
                End Sub

                Private Sub FindCr()
                    Dim i As Integer

                    If s.Length - 1 = pos Then Return
                    i = s.IndexOf(vbCr, pos + 1)
                    If -1 = i Then i = s.Length - 1
                    pos = i
                End Sub

                Private Function IsEndTextMarker() As Boolean
                    If Constants.EndOfText.Length > pos Then Return False
                    Return -1 <> s.IndexOf(Constants.EndOfText, pos - Constants.EndOfText.Length, Constants.EndOfText.Length)
                End Function

                Private Function IsLineContinuation() As Boolean
                    If 0 = pos Then Return False
                    Return Constants.LineContinuation = s(pos - 1)
                End Function

                Private Function IsFormat() As Boolean
                    For Each f In Constants.Formats
                        If f.Length < pos Then
                            If -1 <> s.IndexOf(f, pos - f.Length, f.Length) Then Return True
                        End If
                    Next
                    Return False
                End Function

                Private Sub HandleText()
                    If Not FindTextEnd() Then Throw New TemplateException("broken page content, text has no end")
                    HandleParagraph()
                    AddFormatSegments()
                    AddTextAndFieldSegments()
                    pos += Constants.TextEnd.Length
                    Do While IsSpaceNext()
                        pos += Constants.Space.Length
                    Loop
                    If IsLineBreakNext() Then
                        state = ParseState.LineBreak
                    Else
                        state = ParseState.Eol
                    End If
                End Sub

                Private Function IsSpaceNext() As Boolean
                    If s.Length - Constants.Space.Length <= pos Then Return False
                    Return -1 <> s.IndexOf(Constants.Space, pos + 1, Constants.Space.Length)
                End Function

                Private Function FindTextEnd() As Boolean
                    Dim i = s.IndexOf(Constants.TextEnd, pos + 1)

                    If -1 = i Then Return False
                    FindCr()
                    isContinuation = IsLineContinuation()
                    pos = i
                    isEmptyTextSegment = mark = pos
                    Return True
                End Function

                Private Sub HandleParagraph()
                    If isLineBreak Then
                        If Not isContinuation Then SaveAndResetParagraph()
                        paragraph.AddSegment(New NewLineMarker())
                        isLineBreak = False
                    ElseIf IsRepeatingField() Then
                        Dim addNewLine As Boolean = Not paragraph.IsEmpty
                        SaveAndResetParagraph()
                        If addNewLine Then paragraph.AddSegment(New NewLineMarker())
                    End If
                End Sub

                Private Sub SaveAndResetParagraph()
                    AddParagraph()
                    paragraph = New Paragraph(paragraph.FinalFont, paragraph.FinalLineHeight)
                End Sub

                Private Sub AddParagraph()
                    If paragraph.IsEmpty Then Return
                    paragraph.Unescape()
                    paragraphs.Add(paragraph)
                End Sub

                Private Function IsRepeatingField() As Boolean
                    If Not label Then Return False
                    Return IsFlexFieldStart(mark, pos - mark)
                End Function

                Private Sub AddFormatSegments()
                    If Not label Or 0 = paragraphs.Count() Then
                        For Each f As FormattingSegment In formatSegments
                            paragraph.AddSegment(f)
                        Next
                    End If
                    formatSegments.Clear()
                End Sub

                Private Sub AddTextAndFieldSegments()
                    Dim len As Integer
                    Dim i As Integer

                    Do
                        i = FindFieldStart()
                        If -1 = i Then
                            AddTrailingTextSegment()
                            Return
                        End If

                        len = i - mark
                        If 0 < len Then paragraph.AddSegment(New TextSegment() With {.Text = s.Substring(mark, len)})

                        Select Case state
                            Case ParseState.BaseField
                                i += Constants.BaseFieldStartTag.Length
                            Case ParseState.FlexField
                                i += Constants.FlexFieldStartTag.Length
                        End Select
                        mark = i
                        AddFieldSegment()
                    Loop
                End Sub

                Private Sub AddTrailingTextSegment()
                    Dim add As Boolean = 0 < pos - mark
                    If Not add Then add = Not paragraph.HasTextSegments
                    If Not add Then add = paragraph.IsTextContinued
                    If Not add Then add = isEmptyTextSegment
                    If add Then paragraph.AddSegment(New TextSegment() With {.Text = s.Substring(mark, pos - mark)})
                End Sub

                Private Function FindFieldStart() As Integer
                    Dim search As String = Constants.TagOpen
                    Dim len As Integer = pos - mark
                    Dim i As Integer = s.IndexOf(search, mark, len)

                    Do Until -1 = i
                        If i = pos Then
                            i = -1
                            Exit Do
                        End If
                        len = pos - i
                        If IsBaseFieldStart(i, len) Then Exit Do
                        If IsFlexFieldStart(i, len) Then Exit Do
                        i = s.IndexOf(search, i + 1, len)
                    Loop

                    Return i
                End Function

                Private Function IsBaseFieldStart(at As Integer, len As Integer) As Boolean
                    If IsFieldStart(Constants.BaseFieldStartTag, at, len) Then
                        state = ParseState.BaseField
                        Return True
                    End If
                    Return False
                End Function

                Private Function IsFieldStart(field As String, at As Integer, len As Integer) As Boolean
                    If len <= field.Length Then Return False
                    If -1 = s.IndexOf(field, at, field.Length) Then Return False
                    Return True
                End Function

                Private Function IsFlexFieldStart(at As Integer, len As Integer) As Boolean
                    If IsFieldStart(Constants.FlexFieldStartTag, at, len) Then
                        state = ParseState.FlexField
                        Return True
                    End If
                    Return False
                End Function

                Private Sub AddFieldSegment()
                    Dim len As Integer = pos - mark
                    Dim search As String = Constants.BaseFieldEndTag
                    Dim ftype As FieldSegment.FieldType = FieldSegment.FieldType.Base
                    Dim name As String
                    Dim i As Integer

                    If ParseState.FlexField = state Then
                        search = Constants.FlexFieldEndTag
                        ftype = FieldSegment.FieldType.Flex
                    End If
                    i = s.IndexOf(search, mark, len)
                    If -1 = i Then Throw New TemplateException(String.Format("unterminated {0} field", ftype))
                    name = s.Substring(mark, i - mark)
                    paragraph.AddSegment(New FieldSegment() With {.Name = name, .IsLabel = label, .Tags = FieldTags(state), .Type = ftype, .Text = String.Empty})
                    fields.Add(name)
                    mark = i + search.Length
                End Sub

                Private Function FindTextStart() As Boolean
                    Dim i As Integer

                    If s.Length - 1 = pos Then Return False
                    i = s.IndexOf(Constants.TextStart, pos + 1)
                    Do Until -1 = i
                        If 1 < i Then
                            If Constants.Escape <> s(i - 1) Then
                                pos = i
                                mark = pos + 1
                                state = ParseState.Text
                                Return True
                            End If
                        End If
                        i = s.IndexOf(Constants.TextStart)
                    Loop

                    Return False
                End Function

                Private Sub AddFinalParagraph()
                    If Not paragraphs.Any() Then
                        AddParagraph()
                    ElseIf Not Object.ReferenceEquals(paragraph, paragraphs.Last()) Then
                        AddParagraph()
                    End If
                End Sub
            End Class

            Public Class FormatParser

                Public Class Constants
                    Public Class Formats
                        Public Const Color As String = "color"
                        Public Const WordSpacing As String = "wordspacing"
                        Public Const Font As String = "font"
                        Public Const Alignment As String = "alignment"
                        Public Const Leading As String = "leading"
                        Public Const TransformMatrix As String = "textmatrix"
                    End Class

                    Public Class Patterns
                        Public Const Color As String = "(?<" & Formats.Color & ">(?<red>[.0-9]+)\s+(?<green>[.0-9]+)\s+(?<blue>[.0-9]+)\s+rg)"
                        Public Const WordSpacing As String = "(?<" & Formats.WordSpacing & ">(?<spacing>[.0-9]+)\s+Tw)"
                        Public Const Font As String = "(?<" & Formats.Font & ">(?<fontname>/F\d+)\s+(?<fontsize>[.0-9]+)\s+Tf)"
                        Public Const Alignment As String = "(?<" & Formats.Alignment & ">(?<alignx>-?[.0-9]+)\s+(?<aligny>-?[.0-9]+)\s+(?<alignop>T[dD])\s+%text-align:(?<align>(Left|Center|Right)))"
                        Public Const Leading As String = "(?<" & Formats.Leading & ">(?<lheight>-?[.0-9]+)\s+TL)"
                        Public Const TransformMatrix As String = "(?<" & Formats.TransformMatrix & ">(?<xscale>-?[.0-9]+)\s+0\s+0\s+(?<yscale>-?[.0-9]+)\s+(?<xtrans>-?[.0-9]+)\s+(?<ytrans>-?[.0-9]+)\s+(?<xfmop>Tm))"
                    End Class

                    Public Const Pattern As String = "(" & Patterns.Color & _
                                                      "|" & Patterns.WordSpacing & _
                                                      "|" & Patterns.Font & _
                                                      "|" & Patterns.Alignment & _
                                                      "|" & Patterns.Leading & _
                                                      "|" & Patterns.TransformMatrix & ")"
                End Class

                Private Shared ReadOnly re As New Regex(Constants.Pattern, RegexOptions.Compiled)
                Private ReadOnly patternMap As New Dictionary(Of String, Func(Of String, Match, FormattingSegment))() From { _
                    {Constants.Formats.Font, AddressOf ParseFont}, _
                    {Constants.Formats.Alignment, AddressOf ParseAlignment}, _
                    {Constants.Formats.Leading, AddressOf ParseLeading}, _
                    {Constants.Formats.TransformMatrix, AddressOf ParseTransformMatrix}, _
                    {Constants.Formats.WordSpacing, AddressOf ParseWordSpacing} _
                }

                Public Function Parse(s As String) As FormattingSegment
                    Dim m As Match = re.Match(s)
                    Dim text As String
                    Dim proc As Func(Of String, Match, FormattingSegment)

                    If Not m.Success Then Throw New TemplateException(String.Format("not a known format segment: {{{0}}}", s))
                    For Each kvp In patternMap
                        text = m.Groups(kvp.Key).Value
                        proc = kvp.Value
                        If Not String.IsNullOrEmpty(text) Then Return proc(text, m)
                    Next
                    Return New FormattingSegment() With {.Text = s}
                End Function

                Private Function ParseTransformMatrix(text As String, m As Match) As FormattingSegment
                    Return New PositionSegment() With { _
                        .OpString = m.Groups("xfmop").Value, _
                        .Text = text, _
                        .ScaleFactor = CSng(m.Groups("yscale").Value), _
                        .Translation = New PointF(CSng(m.Groups("xtrans").Value), CSng(m.Groups("ytrans").Value)) _
                    }
                End Function

                Private Function ParseLeading(text As String, m As Match) As FormattingSegment
                    Return New LeadingSegment() With { _
                        .Text = text, _
                        .Leading = CSng(m.Groups("lheight").Value) _
                    }
                End Function

                Private Function ParseFont(text As String, m As Match) As FormattingSegment
                    Return New FontSegment() With { _
                        .Text = text, _
                        .FontName = m.Groups("fontname").Value, _
                        .FontSize = CSng(m.Groups("fontsize").Value), _
                        .OriginalFontSize = CSng(m.Groups("fontsize").Value) _
                    }
                End Function

                Private Function ParseAlignment(text As String, m As Match) As FormattingSegment
                    Dim op As String = m.Groups("alignop").Value

                    If 0 = StringComparer.InvariantCulture.Compare(FormattingSegment.Constants.Token.DrawLeading, op) Then
                        text = text.Replace(" " & FormattingSegment.Constants.Token.DrawLeading, " " & FormattingSegment.Constants.Token.Draw)
                        op = FormattingSegment.Constants.Token.Draw
                    End If

                    Return New PositionSegment() With { _
                        .OpString = op, _
                        .Text = text, _
                        .Alignment = CType([Enum].Parse(GetType(TextAlignment), m.Groups("align").Value), TextAlignment), _
                        .ScaleFactor = 1, _
                        .Translation = New PointF(CSng(m.Groups("alignx").Value), CSng(m.Groups("aligny").Value)) _
                    }
                End Function

                Private Function ParseWordSpacing(text As String, m As Match) As FormattingSegment
                    Return New WordSpacingSegment() With {.Text = text, .WordSpacing = CSng(m.Groups("spacing").Value)}
                End Function
            End Class

        End Namespace
    End Namespace
End Namespace
