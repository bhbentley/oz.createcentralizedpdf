﻿Imports System.Drawing
Imports System.Linq
Imports System.Text.RegularExpressions

Namespace Letters
    Namespace Pdf

        Public Class WordWrapReflow

            Public Class Constants
                Public Const WidthPadding As Single = 2.0F
                Public Const InitialSpaceCount As Integer = -1
                Public Const FontAdjustmentFactor As Single = 6.0F
                Public Const SpaceAdjustmentFactor As Single = 10.0F

                Public Class DisplayFormat
                    Public Const Offset As String = "{0:##0.##} 0 " & FormattingSegment.Constants.Token.Draw
                    Public Const Leading As String = "{0:##0.##} " & FormattingSegment.Constants.Token.Leading
                    Public Const Font As String = "{0} {1} " & FormattingSegment.Constants.Token.Font
                End Class

                Public Class Punctuation
                    Public Const PunctuationGroupName As String = "punct"
                    Public Const RemainderGroupName As String = "remainder"
                    Public Const TextGroupName As String = "text"
                    Public Const Pattern As String = "^\s*(?<" & PunctuationGroupName & ">[.,!?*;'"")]+)(?<" & RemainderGroupName & ">\s*(?<" & TextGroupName & ">.*)?)?"
                    Public Shared ReadOnly Rex As New Regex(Pattern, RegexOptions.Compiled)
                End Class

                Public Class SpaceCount
                    Public Const Pattern As String = " "
                    Public Shared ReadOnly Rex As New Regex(Pattern, RegexOptions.Compiled)
                End Class

                Public Class EndSpace
                    Public Const SpaceGroupName As String = "space"
                    Public Const Pattern As String = "(?<" & SpaceGroupName & "> +)$"
                    Public Shared ReadOnly Rex As New Regex(Pattern, RegexOptions.Compiled)
                End Class
            End Class

            Public Class Group
                Public FormatSegments As IList(Of Segment)
                Public TextSegments As IList(Of Segment)
                Public ReadOnly Property HasText As Boolean
                    Get
                        Return TextSegments.Any()
                    End Get
                End Property
                Public SpaceCount As Integer
            End Class

            Private collected As IList(Of Segment)
            Private reflowed As IList(Of IList(Of Segment))

            Private groups As IList(Of Group)
            Private otherSegments As IList(Of Segment)

            Private fonts As IDictionary(Of String, FontObject)
            Private left As Single
            Private widths As Tuple(Of Single, Single)

            Private alignment As TextAlignment
            Private scaling As Tuple(Of Single, Single)
            Private font As FontSpec
            Private initialLineHeight As Single
            Private lineHeight As Single
            Private insertNewLineMarker As Boolean
            Private accLen As Single
            Private origin As Origin
            Private spaceAdjustment As Single

            Private ReadOnly Property Width As Single
                Get
                    If TextAlignment.Left = alignment Then Return widths.Item2
                    Return widths.Item1
                End Get
            End Property

            Public ReadOnly Property Collector As IList(Of Segment)
                Get
                    Return collected
                End Get
            End Property

            Public ReadOnly Property Segments As IList(Of Segment)
                Get
                    Return reflowed.SelectMany(Function(x) x).ToList()
                End Get
            End Property

            Public Sub New(lineHeight As Single)
                Me.collected = New List(Of Segment)()
                Me.reflowed = New List(Of IList(Of Segment))()
                Me.initialLineHeight = lineHeight
            End Sub

            Public Sub Reflow(fonts As IDictionary(Of String, FontObject), leftEdge As Single, widths As Tuple(Of Single, Single))
                Dim first As Boolean = True

                Init(fonts, leftEdge, widths)

                For Each seg As Segment In collected
                    If TypeOf seg Is NewLineMarker Then
                        If first Then insertNewLineMarker = True
                    ElseIf TypeOf seg Is AlignmentSegment Then
                        If TextAlignment.Unspecified = alignment Then alignment = DirectCast(seg, AlignmentSegment).Alignment
                    ElseIf TypeOf seg Is ScalingSegment Then
                        HandleScalingSegment(DirectCast(seg, ScalingSegment))
                    ElseIf TypeOf seg Is PositionSegment Then
                        HandlePositionSegment(DirectCast(seg, PositionSegment))
                    ElseIf TypeOf seg Is FormattingSegment Then
                        HandleFormattingSegment(DirectCast(seg, FormattingSegment))
                    ElseIf TypeOf seg Is TextSegment Then
                        HandleTextSegment(DirectCast(seg, TextSegment))
                    Else
                        otherSegments.Add(seg)
                    End If
                    first = False
                Next

                If groups.Any(Function(x) x.HasText) Then AddLine()
                reflowed.Add(otherSegments)
            End Sub

            Private Sub Init(fonts As IDictionary(Of String, FontObject), leftEdge As Single, widths As Tuple(Of Single, Single))
                Me.fonts = fonts
                left = leftEdge
                Me.widths = widths

                reflowed.Clear()
                InitGroups()
                otherSegments = New List(Of Segment)()

                alignment = TextAlignment.Unspecified
                scaling = New Tuple(Of Single, Single)(1.0F, 1.0F)
                font = New FontSpec()
                lineHeight = initialLineHeight
                insertNewLineMarker = False
                accLen = 0.0F
                origin = New Origin(DrawUtility.Origin)
                spaceAdjustment = 0.0F
            End Sub

            Private Sub InitGroups()
                groups = New List(Of Group)() From { _
                    New Group() With { _
                        .FormatSegments = New List(Of Segment)(), _
                        .TextSegments = New List(Of Segment)(), _
                        .SpaceCount = Constants.InitialSpaceCount _
                    } _
                }
            End Sub

            Private Sub HandleFormattingSegment(seg As FormattingSegment)
                If TypeOf seg Is FontSegment Then
                    HandleFontSegment(DirectCast(seg, FontSegment))
                ElseIf TypeOf seg Is LeadingSegment Then
                    HandleLeadingSegment(DirectCast(seg, LeadingSegment))
                Else
                    GroupFormattingSegment(seg)
                End If
            End Sub

            Private Sub GroupFormattingSegment(seg As FormattingSegment)
                If groups.Last().HasText Then
                    groups.Add(New Group() With { _
                        .FormatSegments = New List(Of Segment)(), _
                        .TextSegments = New List(Of Segment)() _
                    })
                End If
                groups.Last().FormatSegments.Add(seg)
            End Sub

            Private Sub HandleScalingSegment(seg As ScalingSegment)
                If 1.0F <> scaling.Item1 Then Return
                If 1.0F = seg.ScaleFactor And 1.0F = seg.ScaleWeight Then Return
                scaling = New Tuple(Of Single, Single)(seg.ScaleFactor, seg.ScaleWeight)
            End Sub

            Private Sub HandleFontSegment(seg As FontSegment)
                font = New FontSpec(seg.FontName, seg.FontSize)
                GroupFormattingSegment(seg)
            End Sub

            Private Sub HandleLeadingSegment(seg As LeadingSegment)
                Dim h As Single = lineHeight

                If 0.0F = seg.Leading Then Return
                If seg.Leading <> h Then
                    If 1.0F <> scaling.Item1 Then
                        If seg.Leading <> h / scaling.Item1 Then h = seg.Leading * scaling.Item1
                    Else
                        h = seg.Leading
                    End If
                End If
                If lineHeight <> h Then
                    lineHeight = h
                    GroupFormattingSegment(seg)
                End If
            End Sub

            Private Sub HandlePositionSegment(seg As PositionSegment)
                If PositionOperator.Matrix = seg.[Operator] Then
                    GroupFormattingSegment(seg)
                    origin.Replace(seg.Translation)
                ElseIf PositionOperator.LineOffset = seg.[Operator] Then
                    If 0.0F <> seg.Translation.Y Then
                        GroupFormattingSegment(seg)
                        origin.Replace(seg.Translation)
                    End If
                Else
                    GroupFormattingSegment(seg)
                End If
            End Sub

            Private Sub HandleTextSegment(seg As TextSegment)
                Dim s As String = seg.Text
                Dim len As Single = MeasureString(s)

                If len + accLen > Width Then
                    HandleOverflow(New Tuple(Of TextSegment, Single)(seg, len))
                Else
                    GroupTextSegment(seg)
                    accLen += len
                End If
            End Sub

            Private Function MeasureString(s As String) As Single
                Dim fo As FontObject = fonts(font.FontName)
                Return DrawUtility.MeasureStringWidth(s, font.FontSize, fo)
            End Function

            Private Sub GroupTextSegment(seg As TextSegment)
                With groups.Last()
                    .TextSegments.Add(seg)
                    .SpaceCount += Constants.SpaceCount.Rex.Matches(seg.Text).Count
                End With
            End Sub

            Private Sub HandleOverflow(mseg As Tuple(Of TextSegment, Single))
                ' WARNING: Danger lies ahead!  You are about to enter the wrapping "algorithm" which is a cobbled together
                ' mess of code derived from experiment rather than being designed.  In particular, the splitting routines may
                ' never exit if given input that hasn't been seen before.
                '
                ' This is also special handling for spaces.  PDF renderers do not measure text the same as the .NET platform.
                ' Therefore, this code overestimates the length of text as compared to how it will be rendered in the PDF.
                ' The legacy code was aware of this problem as well and put in some measures to correct.  However, those
                ' measures were not 100%.  This code attempts to narrow the gap, which is especially noticeable when the
                ' text is right-aligned and contains many spaces.

                If TypeOf mseg.Item1 Is FieldSegment Then
                    If Not IsWrappable(DirectCast(mseg.Item1, FieldSegment)) Then
                        GroupTextSegment(mseg.Item1)
                        AddLine()
                        Return
                    End If
                Else
                    Dim m As Match = Constants.Punctuation.Rex.Match(mseg.Item1.Text)
                    If m.Success Then
                        JoinPunctuation(m)
                        Return
                    End If
                End If
                SplitSegmentText(New Tuple(Of String, Single)(mseg.Item1.Text, mseg.Item2))
            End Sub

            Private Function IsWrappable(seg As FieldSegment) As Boolean
                If Not seg.IsReplaced Then Return False
                If seg.IsLabel Then Return False
                Return True
            End Function

            Private Sub AddLine()
                CountSpaces()
                PositionText()
                MergeGroups()
                Reset()
            End Sub

            Private Sub CountSpaces()
                spaceAdjustment = 0.0F
                If TextAlignment.Left = alignment Then Return
                If Not groups.Any() Then Return
                spaceAdjustment = groups.Sum(Function(x) x.SpaceCount)
                If groups.Last().TextSegments.OfType(Of TextSegment).Any() Then
                    ' Don't count trailing spaces at the end of the line.
                    Dim m As Match = Constants.EndSpace.Rex.Match(groups.Last().TextSegments.OfType(Of TextSegment)().Last().Text)
                    If m.Success Then spaceAdjustment -= m.Value.Count
                End If
            End Sub

            Private Overloads Sub PositionText()
                Select Case alignment
                    Case TextAlignment.Center
                        AlignCenter()
                    Case TextAlignment.Right
                        AlignRight()
                    Case Else
                        Return
                End Select
            End Sub

            Private Sub AlignCenter()
                Dim x As Single = 0.0F
                Dim spaceAdj As Single = 0.0F

                If 0.0F = accLen Then
                    x = left - origin.Location.X
                Else
                    x = left + (Width - accLen) / 2.0F
                    x = (x - origin.Location.X) / scaling.Item1
                End If

                spaceAdj = spaceAdjustment / 2.0F
                PositionText(x, spaceAdj)
            End Sub

            Private Overloads Sub PositionText(pos As Single, spaceAdj As Single)
                If 0.0F <> pos Then
                    If 0.0F <> spaceAdj Then
                        Dim adj As Single = spaceAdj + (spaceAdj - 1.0F) / Constants.SpaceAdjustmentFactor
                        Dim factor As Single = font.FontSize / Constants.FontAdjustmentFactor
                        spaceAdj = Enumerable.Range(1, CInt(spaceAdj)).Aggregate(0.0F, Function(acc, x) acc + x / adj * factor)
                        If 0.0F > spaceAdj Then spaceAdj = 0.0F
                    End If
                    If 0.0F <> spaceAdj Then pos -= spaceAdj
                    AddOffsetSegment(pos)
                    origin.Move(New SizeF(pos, 0.0F))
                End If
            End Sub

            Private Sub AlignRight()
                Dim x As Single = 0.0F
                Dim spaceAdj As Single = 0.0F

                If 0.0F = accLen Then
                    x = left - origin.Location.X
                Else
                    x = left + Width - accLen
                    x = (x - origin.Location.X) / scaling.Item1
                End If

                If 0.0F < spaceAdjustment Then spaceAdj = spaceAdjustment
                PositionText(x, spaceAdj)
            End Sub

            Private Sub AddOffsetSegment(offset As Single)
                Dim seg As New PositionSegment() With { _
                    .Text = String.Format(Constants.DisplayFormat.Offset, offset), _
                    .OpString = FormattingSegment.Constants.Token.Draw, _
                    .Translation = New PointF(offset, 0) _
                }
                groups.First().FormatSegments.Add(seg)
                DrawUtility.Origin.Reflowed = True
            End Sub

            Private Sub MergeGroups()
                FixFontSegments()
                For Each g As Group In groups
                    AddFormattingSegments(g.FormatSegments)
                    AddTextSegments(g.TextSegments)
                    insertNewLineMarker = False
                Next
            End Sub

            Private Sub FixFontSegments()
                Dim prevSeg As FontSegment = groups.First().FormatSegments.OfType(Of FontSegment).SingleOrDefault()
                Dim nextSeg As FontSegment
                Dim fix As Boolean

                For Each g As Group In groups.Skip(1)
                    nextSeg = g.FormatSegments.OfType(Of FontSegment).SingleOrDefault()
                    fix = True
                    If prevSeg Is Nothing Then fix = False
                    If fix Then fix = Not String.IsNullOrEmpty(prevSeg.Text)
                    If fix Then fix = prevSeg Is Nothing
                    If fix Then fix = Not String.IsNullOrEmpty(prevSeg.Text)
                    If fix Then fix = prevSeg.FontSize <> nextSeg.FontSize OrElse 0 <> StringComparer.InvariantCulture.Compare(prevSeg.FontName, nextSeg.FontName)
                    If fix Then nextSeg.Text = String.Format(Constants.DisplayFormat.Font, nextSeg.FontName, nextSeg.FontSize)
                    prevSeg = nextSeg
                Next
            End Sub

            Private Sub AddFormattingSegments(segments As IList(Of Segment))
                reflowed.Add(segments)
            End Sub

            Private Sub AddTextSegments(segments As IList(Of Segment))
                Dim hasText = segments.Any()
                If hasText Then segments.Insert(0, New BeginStringMarker())
                If insertNewLineMarker Then segments.Insert(0, New NewLineMarker())
                If hasText Then segments.Add(New EndStringMarker())
                If segments.Any() Then reflowed.Add(segments)
            End Sub

            Private Sub Reset()
                InitGroups()
                insertNewLineMarker = True
                accLen = 0.0F
            End Sub

            Private Sub JoinPunctuation(m As Match)
                Dim g As Group = groups.LastOrDefault(Function(x) x.HasText)
                Dim t As TextSegment

                If Not g Is Nothing Then
                    t = CType(g.TextSegments.Last(), TextSegment)
                    g.TextSegments.RemoveAt(g.TextSegments.Count - 1)
                    GroupTextSegment(New TextSegment() With {.Text = t.Text & m.Groups(Constants.Punctuation.PunctuationGroupName).Value})
                    If Not String.IsNullOrEmpty(m.Groups(Constants.Punctuation.TextGroupName).Value) Then
                        SplitSegmentText(New Tuple(Of String, Single)(m.Groups(Constants.Punctuation.RemainderGroupName).Value, -1.0F))
                    Else
                        AddLine()
                    End If
                Else
                    SplitSegmentText(New Tuple(Of String, Single)(m.Value, -1.0F))
                End If
            End Sub

            Private Sub SplitSegmentText(measuredText As Tuple(Of String, Single))
                Dim parts As IList(Of Tuple(Of String, Single)) = SplitText(measuredText)
                Dim fmt As IList(Of Segment) = groups.Last().FormatSegments.Where(Function(x) TypeOf x Is FontSegment).ToList()

                AddTrailer(parts.First())
                If 1 = parts.Count Then Return
                AddNewLines(parts.Skip(1).Take(parts.Count - 2).ToList())
                StartNextLine(fmt, parts.Last())
            End Sub

            Private Overloads Function SplitText(measuredText As Tuple(Of String, Single)) As IList(Of Tuple(Of String, Single))
                Dim text As String = measuredText.Item1
                Dim parts As New List(Of Tuple(Of String, Single))()
                Dim mark As Integer = 0
                Dim acc As Single = accLen
                Dim part As Tuple(Of String, Single)
                Dim len As Integer

                Do Until text.Length = mark
                    part = SplitText(text, mark, acc)
                    len = part.Item1.Length
                    If 0 <> len Then
                        parts.Add(part)
                        ' Start a new line here.
                        mark += len
                        acc = 0
                    Else
                        If -1.0F = measuredText.Item2 Then
                            Dim tlen As Single = MeasureString(text)
                            If acc + tlen < Width Then
                                parts.Add(New Tuple(Of String, Single)(text, tlen))
                                Exit Do
                            End If
                        End If

                        If 0 = acc Then
                            ' Cannot fit text into region for some reason so throw out text completely.
                            Exit Do
                        End If

                        ' Cannot add anything more to line so add an empty part for the current line
                        ' and start a new line.
                        parts.Add(part)
                        acc = 0
                    End If
                Loop

                Return parts
            End Function

            Private Overloads Function SplitText(text As String, mark As Integer, acc As Single) As Tuple(Of String, Single)
                Dim at As Integer = text.IndexOf(" "c, mark)
                Dim foundSpace As Boolean = -1 <> at
                Dim pos As Integer = mark
                Dim s As String
                Dim len As Integer
                Dim dx As Single, dx2 As Single
                Dim m As Match

                Do
                    If -1 = at Then
                        at = text.Length
                        len = 0
                    ElseIf foundSpace Then
                        len = 1
                    End If
                    len += at - pos
                    s = text.Substring(pos, len)
                    dx = MeasureString(s)
                    If dx + acc >= Width Then
                        m = Constants.EndSpace.Rex.Match(s)
                        If m.Success Then
                            s = text.Substring(pos, len - m.Value.Count)
                            dx2 = MeasureString(s)
                            If dx2 + acc >= Width Then Exit Do
                        Else
                            Exit Do
                        End If
                    End If
                    pos = at
                    acc += dx
                    If text.Length = pos Then Exit Do
                    foundSpace = True
                    pos += 1
                    at = text.IndexOf(" "c, pos)
                Loop

                If text.Length = at Then
                    If Not foundSpace Then
                        If dx > Width Then Return ChopText(text, mark, acc)
                        If dx <> acc Then
                            pos = 0
                            acc = 0.0F
                        End If
                    End If
                ElseIf mark <> pos Then
                    Do Until text.Length = pos
                        If " "c = text(pos) Then
                            pos += 1
                        Else
                            Exit Do
                        End If
                    Loop
                ElseIf 0.0F = acc Then
                    Return ChopText(text, mark, acc)
                End If

                Return New Tuple(Of String, Single)(text.Substring(mark, pos - mark), acc)
            End Function

            Private Function ChopText(text As String, mark As Integer, acc As Single) As Tuple(Of String, Single)
                Dim midp As Integer = (text.Length - mark) \ 2
                Dim at As Integer = text.Length
                Dim len As Single = MeasureString(text.Substring(mark, at - mark))
                Dim cutoff As Single = Width - acc

                Do
                    If text.Length = at Or 0 = midp Then
                        If cutoff >= len Then Exit Do
                    ElseIf mark > at Then
                        at = mark
                        len = 0
                        Exit Do
                    End If

                    If cutoff > len Then
                        at += midp
                    ElseIf cutoff < len Then
                        at -= Math.Max(midp, 1)
                    Else
                        Exit Do
                    End If

                    midp = midp \ 2
                    len = MeasureString(text.Substring(mark, at - mark))
                Loop

                Return New Tuple(Of String, Single)(text.Substring(mark, at - mark), len)
            End Function

            Private Sub AddTrailer(trailer As Tuple(Of String, Single))
                GroupTextSegment(New TextSegment() With {.Text = trailer.Item1})
                accLen = trailer.Item2
                AddLine()
            End Sub

            Private Sub AddNewLines(parts As IList(Of Tuple(Of String, Single)))
                Dim h As Single = lineHeight

                lineHeight = 0.0F
                For Each part As Tuple(Of String, Single) In parts
                    groups.Last().TextSegments.Add(New TextSegment() With {.Text = part.Item1.TrimStart(" "c)})
                    accLen = part.Item2
                    AddLine()
                Next
                lineHeight = h
            End Sub

            Private Sub StartNextLine(fmt As IList(Of Segment), part As Tuple(Of String, Single))
                With groups.Last()
                    .FormatSegments = fmt
                    .TextSegments.Add(New TextSegment() With {.Text = part.Item1.TrimStart(" "c)})
                End With
                accLen = part.Item2
            End Sub

        End Class

    End Namespace
End Namespace
