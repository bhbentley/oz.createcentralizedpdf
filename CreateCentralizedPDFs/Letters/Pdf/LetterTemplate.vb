﻿Imports System.Drawing
Imports System.Linq

Namespace Letters
    Namespace Pdf

        Public Class BoundingBox
            Public LowerLeft As PointF
            Public UpperRight As PointF
        End Class

        Public Class LetterTemplate
            Implements Letters.LetterTemplate

            Public Class Constants
                Public Const ObjectNumberBase As Integer = 3
            End Class

            Private fonts As IList(Of FontObject)
            Private fontNames As IDictionary(Of String, Font)
            Private images As IList(Of ImageStream)
            Private pages As IList(Of Page)
            Private props As Page.Props
            Private mplt As IDictionary(Of Letters.LetterType, Integer)
            Private repeatingFields As IDictionary(Of String, Integer)

            Public Sub New(parser As TemplateParser.TemplateParser, maximumPagesPerLetterType As IDictionary(Of Letters.LetterType, Integer))
                parser.Parse()
                fonts = parser.Fonts
                images = parser.Images
                pages = New List(Of Page)()
                props = New Page.Props() With { _
                    .PageSize = New SizeF(parser.UserSettings.paperWd * DrawUtility.Constants.Dpi, parser.UserSettings.paperHt * DrawUtility.Constants.Dpi), _
                    .Margins = New Page.Margin() With { _
                        .Left = CInt(parser.UserSettings.marginLR * DrawUtility.Constants.Dpi), _
                        .Right = CInt(parser.UserSettings.marginLR * DrawUtility.Constants.Dpi), _
                        .Top = parser.UserSettings.marginTB * DrawUtility.Constants.Dpi, _
                        .Bottom = parser.UserSettings.marginTB * DrawUtility.Constants.Dpi _
                    } _
                }
                mplt = maximumPagesPerLetterType

                ReadRepeatingFields(parser.UserSettings)
                AssociatePagesWithStreams(parser)
                RenumberObjects()
            End Sub

            Private Sub ReadRepeatingFields(settings As TemplateParser.UserSettings)
                Me.repeatingFields = New Dictionary(Of String, Integer)()
                For Each fd In settings.ffs
                    If fd.repeating Then repeatingFields.Add(fd.name, fd.numTimesRepeat)
                Next
            End Sub

            Private Sub AssociatePagesWithStreams(parser As TemplateParser.TemplateParser)
                Dim map As IDictionary(Of Integer, PageStream) = New Dictionary(Of Integer, PageStream)()

                For Each ps In parser.PageContents
                    map.Add(ps.ObjectNumber, ps)
                Next

                For Each o In parser.Pages
                    pages.Add(New Page() With { _
                        .Node = o, _
                        .Stream = map(o.Content), _
                        .Properties = props _
                    })
                    With pages.Last()
                        .Number = pages.Count
                        .PopulateMaps()
                        .NameFonts(fonts)
                        .SetContentBounds()
                    End With
                Next
            End Sub

            Private Sub RenumberObjects()
                Dim n As Integer = Constants.ObjectNumberBase
                Dim fontMap As IDictionary(Of Integer, Integer) = New Dictionary(Of Integer, Integer)()
                Dim imageMap As IDictionary(Of Integer, Integer) = New Dictionary(Of Integer, Integer)()

                For Each f In fonts
                    fontMap.Add(f.ObjectNumber, n)
                    f.ObjectNumber = n
                    n += 1
                Next

                For Each im In images
                    imageMap.Add(im.ObjectNumber, n)
                    im.ObjectNumber = n
                    n += 1
                Next

                For Each pg In pages
                    pg.Node.ObjectNumber = n
                    pg.FontObjectMap = RenumberMap(pg.FontObjectMap, fontMap)
                    pg.ImageObjectMap = RenumberMap(pg.ImageObjectMap, imageMap)
                    pg.Stream.ObjectNumber = n + 1
                    pg.Node.Content = pg.Stream.ObjectNumber
                    n += 2
                Next
            End Sub

            Private Function RenumberMap(map As IDictionary(Of Integer, String), transform As IDictionary(Of Integer, Integer)) As IDictionary(Of Integer, String)
                Dim replacement As IDictionary(Of Integer, String) = New Dictionary(Of Integer, String)
                Dim name As String

                For Each kvp In transform
                    name = map(kvp.Key)
                    replacement.Add(kvp.Value, name)
                Next

                Return replacement
            End Function

            Public Function MakeLetter(type As LetterType, data As FieldData, replaceLabel As Boolean) As Letters.Letter Implements Letters.LetterTemplate.MakeLetter
                Dim ltr As Letter = New Letter() With { _
                    .Fonts = fonts, _
                    .Images = images, _
                    .Pages = pages, _
                    .Type = type, _
                    .AvailablePageCount = mplt(type), _
                    .RepeatingFields = Me.repeatingFields _
                }
                ltr.Render(data, replaceLabel)
                Return ltr
            End Function

#Region "IDisposable Support"
            Private disposedValue As Boolean ' To detect redundant calls

            ' IDisposable
            Protected Overridable Sub Dispose(disposing As Boolean)
                If Not Me.disposedValue Then
                    If disposing Then
                        DrawUtility.ClearFontCache()
                    End If
                End If
                Me.disposedValue = True
            End Sub

            ' This code added by Visual Basic to correctly implement the disposable pattern.
            Public Sub Dispose() Implements IDisposable.Dispose
                ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub
#End Region

        End Class

    End Namespace
End Namespace
