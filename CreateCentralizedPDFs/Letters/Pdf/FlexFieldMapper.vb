﻿Imports System.Linq

Namespace Letters
    Namespace Pdf

        Public Class FlexFieldMapper
            Implements Letters.FieldMapper(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA))

            Public Class Constants
                Public Shared ReadOnly ExcludedColumns As ISet(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes) = New HashSet(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes)() From { _
                    Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.CREATED, _
                    Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.EXPORT_ID, _
                    Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.LETTER_ID, _
                    Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.LTR_LINE, _
                    Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.LTR_NUM, _
                    Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.LTR_TYPE_GROUP, _
                    Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.LTR_TYPE_ID, _
                    Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.PROCESSED, _
                    Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.VERSION _
                }
            End Class

            Private key As TemplateKey
            Private columnMap As IDictionary(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes, String)
            Private repeatMap As IDictionary(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes, String)
            Private data As FieldData

            Public Sub New()
                key = New TemplateKey()
            End Sub

            Public Sub Init(key As TemplateKey, definitions As IList(Of FieldDefinition)) Implements Letters.FieldMapper(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)).Init
                If Me.key.Equals(key) Then Return

                Dim columns As Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes() = CType(System.Enum.GetValues(GetType(Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes)), Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes())
                Dim n As String

                columnMap = New Dictionary(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes, String)()
                repeatMap = New Dictionary(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes, String)()

                data = New FieldData() With { _
                    .RegularFields = New Dictionary(Of String, String)(), _
                    .RepeatingFields = New Dictionary(Of String, IList(Of String))() _
                }

                For Each column In columns
                    If Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.FLEX_01 > column Or Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.FLEX_25 < column Then
                        If Not Constants.ExcludedColumns.Contains(column) Then
                            n = System.Enum.GetName(GetType(Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes), column).ToLower()
                            columnMap.Add(column, n)
                            data.RegularFields.Add(n, String.Empty)
                        End If
                    End If
                Next

                For Each def As FieldDefinition In definitions
                    If Not def.IsRepeat Then
                        columnMap.Add(CType(Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.FLEX_01 + def.Number - 1, Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes), def.Name)
                        data.RegularFields.Add(def.Name, String.Empty)
                    Else
                        repeatMap.Add(CType(Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes.FLEX_01 + def.Number - 1, Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA.ColumnIndexes), def.Name)
                        data.RepeatingFields.Add(def.Name, New List(Of String)())
                    End If
                Next

                Me.key = key
            End Sub

            Public Function Map(letterData As IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)) As FieldData Implements FieldMapper(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)).Map
                If Not letterData.Any() Then
                    ClearData()
                Else
                    FillRegularFields(letterData.First())
                    FillRepeatingFields(letterData)
                End If

                Return data
            End Function

            Private Sub ClearData()
                Dim keys As IList(Of String) = data.RegularFields.Select(Function(x) x.Key).ToList()

                For Each key As String In keys
                    data.RegularFields(key) = String.Empty
                Next
                ClearRepeatingLists()
            End Sub

            Private Sub ClearRepeatingLists()
                Dim keys As IList(Of String) = data.RepeatingFields.Select(Function(x) x.Key).ToList()

                For Each key As String In keys
                    data.RepeatingFields(key).Clear()
                Next
            End Sub

            Private Sub FillRegularFields(letterData As Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)
                Dim v As Object

                For Each kvp In columnMap
                    v = letterData(kvp.Key)
                    If v Is Nothing Then v = String.Empty
                    data.RegularFields(kvp.Value) = v.ToString()
                Next
            End Sub

            Private Sub FillRepeatingFields(letterData As IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA))
                Dim v As Object

                ClearRepeatingLists()
                For Each row In letterData
                    For Each kvp In repeatMap
                        v = row(kvp.Key)
                        If v Is Nothing Then v = String.Empty
                        data.RepeatingFields(kvp.Value).Add(v.ToString())
                    Next
                Next
            End Sub
        End Class

    End Namespace
End Namespace
