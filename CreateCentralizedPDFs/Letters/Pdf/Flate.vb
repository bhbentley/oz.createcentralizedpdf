﻿Imports System.IO

Imports ComponentAce.Compression.Libs.zlib

Namespace Letters
    Namespace Pdf

        Public Module Flate
            Public Enum Mode
                Compress = 0
                Uncompress
            End Enum

            Private Const BufferExpansionFactor As Integer = 2
            Private cbuf As Byte() = New Byte(1023) {}

            Public ReadOnly Property CompressionBuffer As Byte()
                Get
                    Return cbuf
                End Get
            End Property

            Public Function Compress(b As Byte(), contentLength As Integer) As Integer
                Return Compress(b, contentLength, cbuf)
            End Function

            Public Function Compress(s As String, ByRef buffer As Byte()) As Integer
                Dim b As Byte() = System.Text.ASCIIEncoding.Default.GetBytes(s)
                Return Compress(b, b.Length, buffer)
            End Function

            Public Function Compress(b As Byte(), contentLength As Integer, ByRef buffer As Byte()) As Integer
                If 0 > contentLength Then Return 0
                If 0 = b.Length Or 0 = contentLength Then Return 0
                If contentLength > b.Length Then contentLength = b.Length
                Using uncompressed As New MemoryStream(b, 0, contentLength)
                    Dim compressed As MemoryStream = Nothing
                    Dim zs As ZOutputStream = Nothing
                    Do
                        Try
                            compressed = New MemoryStream(buffer)
                            zs = New ZOutputStream(compressed, zlibConst.Z_DEFAULT_COMPRESSION)
                            Transform(uncompressed, CInt(uncompressed.Length), zs, Mode.Compress)
                            Return CInt(zs.TotalOut)
                        Catch ex As NotSupportedException
                            buffer = New Byte(buffer.Length * BufferExpansionFactor - 1) {}
                            uncompressed.Seek(0, SeekOrigin.Begin)
                        Finally
                            If Not zs Is Nothing Then zs.Close()
                            If Not compressed Is Nothing Then compressed.Close()
                        End Try
                    Loop
                End Using
            End Function

            Private Sub Transform(stm As Stream, length As Integer, zs As ZOutputStream, mode As Mode)
                Const ReadBufferSize As Integer = 4 * 1024
                Dim readBuffer As Byte() = New Byte(ReadBufferSize - 1) {}
                Dim iterations As Integer = 0
                Dim read As Integer

                read = stm.Read(readBuffer, 0, ReadBufferSize)
                Do Until 0 = read
                    length -= read
                    If 0 = length And 0 = iterations Then Exit Do
                    If 0 = length And 0 < iterations Then zs.FlushMode = zlibConst.Z_FINISH
                    zs.Write(readBuffer, 0, read)
                    read = stm.Read(readBuffer, 0, ReadBufferSize)
                    iterations += 1
                Loop
                If 0 = iterations And 2 <= read Then
                    zs.Write(readBuffer, 0, read - 1)
                    If mode.Compress = mode Then zs.FlushMode = zlibConst.Z_FINISH
                    zs.Write(readBuffer, read - 1, 1)
                End If
                zs.Flush()
            End Sub

            Public Function Decompress(ByVal b As Byte(), contentLength As Integer, ByRef buffer As Byte()) As Integer
                Using compressed As New MemoryStream(b, 0, contentLength)
                    Dim zs As ZOutputStream = Nothing
                    Dim uncompressed As MemoryStream = Nothing
                    Do
                        Try
                            uncompressed = New MemoryStream(buffer)
                            zs = New ZOutputStream(uncompressed)
                            Transform(compressed, contentLength, zs, Mode.Uncompress)
                            Return CInt(zs.TotalOut)
                        Catch ex As NotSupportedException
                            buffer = New Byte(buffer.Length * BufferExpansionFactor - 1) {}
                            compressed.Seek(0, SeekOrigin.Begin)
                        Finally
                            If Not zs Is Nothing Then zs.Close()
                            If Not uncompressed Is Nothing Then uncompressed.Close()
                        End Try
                    Loop
                End Using
            End Function
        End Module

    End Namespace
End Namespace
