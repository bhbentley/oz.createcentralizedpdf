﻿Imports System.IO

Namespace Letters
    Namespace Pdf

        Public Class TemplateBuilder
            Implements Letters.TemplateBuilder

            Public Function Build(definition As TemplateDefinition) As Letters.LetterTemplate Implements Letters.TemplateBuilder.Build
                Using stm As New MemoryStream(definition.pdf)
                    Dim p As New Letters.Pdf.TemplateParser.TemplateParser(stm)
                    Return New Letters.Pdf.LetterTemplate(p, definition.MaximumPageCountPerLetterType)
                End Using
            End Function
        End Class

    End Namespace
End Namespace
