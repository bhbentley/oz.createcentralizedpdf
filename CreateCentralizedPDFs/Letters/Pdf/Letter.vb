﻿Namespace Letters
    Namespace Pdf

        Public Class Letter
            Implements Letters.Letter

            Public Fonts As IList(Of FontObject)
            Public Images As IList(Of ImageStream)
            Public Pages As IList(Of Page)
            Public Type As LetterType
            Public AvailablePageCount As Integer
            Public RepeatingFields As IDictionary(Of String, Integer)

            Public Sub Write(stream As System.IO.Stream) Implements Letters.Letter.Write
                Dim w As New Letters.Pdf.LetterWriter(Me, stream)
                w.Write()
            End Sub

            Public Sub Render(data As FieldData, replaceLabel As Boolean) Implements Letters.Letter.Render
                Dim info As New PageRenderer.Info() With { _
                    .Type = Me.Type, _
                    .AvailablePages = AvailablePageCount, _
                    .FreePages = Math.Max(AvailablePageCount - Pages.Count, 0), _
                    .Pages = Me.Pages, _
                    .RegularFields = data.RegularFields, _
                    .RepeatingFields = data.RepeatingFields, _
                    .RepeatingFieldCounts = Me.RepeatingFields _
                }
                Dim renderer As New PageRenderer(info)

                renderer.Render(replaceLabel)
                Pages = renderer.Pages
            End Sub
        End Class

    End Namespace
End Namespace
