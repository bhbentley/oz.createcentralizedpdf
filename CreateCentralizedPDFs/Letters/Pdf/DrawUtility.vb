﻿Imports System.Drawing

Namespace Letters
    Namespace Pdf

        Public Module DrawUtility
            Public Class Constants
                Public Const Dpi As Single = 72.0F
                Public Shared ReadOnly FontMap As IDictionary(Of String, String) = New Dictionary(Of String, String)() From { _
                    {"Arial", "Arial"}, _
                    {"CourierNew", "Courier New"}, _
                    {"TimesNewRoman", "Times New Roman"} _
                }
            End Class

            Private fontCache As IDictionary(Of Tuple(Of Integer, Single), Font)
            Private backingBmp As Bitmap
            Private gfx As Graphics
            Private sfmt As StringFormat

            Public Function MeasureStringWidth(s As String, fontSize As Single, fo As FontObject) As Single
                Dim g As Graphics = GetGraphics()
                Dim fnt As Font = GetFont(fontSize, fo)
                Dim pt As New PointF(0.0F, 0.0F)
                Dim sz As SizeF = g.MeasureString(s, fnt, pt, sfmt)

                Return (sz.Width)
            End Function

            Private Function GetGraphics() As Graphics
                If backingBmp Is Nothing Then
                    backingBmp = New Bitmap(1, 1)
                End If
                If gfx Is Nothing Then
                    gfx = Graphics.FromImage(backingBmp)
                    gfx.PageUnit = GraphicsUnit.Point
                    gfx.TextRenderingHint = Text.TextRenderingHint.AntiAlias
                End If
                If sfmt Is Nothing Then
                    sfmt = StringFormat.GenericTypographic
                    sfmt.FormatFlags = sfmt.FormatFlags Or StringFormatFlags.MeasureTrailingSpaces
                End If

                Return gfx
            End Function

            Private Function GetFont(fontSize As Single, fo As FontObject) As Font
                Dim fnt As Font = Nothing
                Dim familyName As String = Nothing

                If fontCache Is Nothing Then fontCache = New Dictionary(Of Tuple(Of Integer, Single), Font)()
                If Not fontCache.TryGetValue(New Tuple(Of Integer, Single)(fo.ObjectNumber, fontSize), fnt) Then
                    If Not Constants.FontMap.TryGetValue(fo.BaseFont, familyName) Then familyName = fo.BaseFont
                    fnt = New Font(familyName, fontSize, fo.Style, GraphicsUnit.Point)
                    fontCache.Add(New Tuple(Of Integer, Single)(fo.ObjectNumber, fontSize), fnt)
                End If

                Return fnt
            End Function

            Public Sub ClearFontCache()
                If fontCache Is Nothing Then Return
                For Each kvp In fontCache
                    kvp.Value.Dispose()
                Next
                fontCache = Nothing
            End Sub

            Public Sub Release()
                ClearFontCache()
                If Not gfx Is Nothing Then
                    gfx.Dispose()
                    gfx = Nothing
                End If
                If Not backingBmp Is Nothing Then
                    backingBmp.Dispose()
                    backingBmp = Nothing
                End If
            End Sub

            Private drawOrigin As New Origin(New PointF(0, 0))
            Public ReadOnly Property Origin As Origin
                Get
                    Return drawOrigin
                End Get
            End Property
        End Module

    End Namespace
End Namespace
