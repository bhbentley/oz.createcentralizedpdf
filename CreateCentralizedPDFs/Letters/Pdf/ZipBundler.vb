﻿Imports System.IO
Imports System.Linq

Imports Ionic.Zip

Namespace Letters
    Namespace Pdf

        Public Class ZipBundler
            Implements Letters.LetterBundler

            Public Class Constants
                Public Const LetterGlob As String = "*.pdf"
                Public Const ZipGlob As String = "TNT_PDFS*.zip"
                Public Const FirstZipFileFormat As String = "TNT_PDFS{0:yyyy_MM_dd}_{1}.zip"
                Public Const ZipFileFormat As String = "TNT_PDFS{0:yyyy_MM_dd}_{1}_{2}.zip"
                Public Const NoDataFileName As String = "noData.txt"
                Public Const NoDataContents As String = "No tenant records in todays pull."
            End Class

            Public Event Notice(sender As Object, e As NotifyEventArgs) Implements Letters.LetterBundler.Notice

            Private info As BundleInfo
            Private letterNames As IList(Of String)
            Private zipFileName As String

            Public Sub Bundle(info As BundleInfo) Implements LetterBundler.Run
                Dim bundleNumber As Integer = 0
                Dim letterCount As Integer = 0
                Dim sendNotice As Boolean = False
                Dim [error] As Exception = Nothing
                Dim selectedLetters As IList(Of String)

                Init(info)
                Clean()
                NotifyStart()
                Try
                    Do
                        zipFileName = BundleName(bundleNumber)
                        selectedLetters = letterNames.Skip(letterCount).Take(info.LettersPerBundle).ToList()
                        If 0 = selectedLetters.Count Then
                            If 0 = bundleNumber Then CreateEmptyZipFile(zipFileName)
                            Exit Do
                        End If
                        Using zf As New ZipFile(zipFileName)
                            zf.AddFiles(selectedLetters, False, String.Empty)
                            zf.Save()
                        End Using
                        Notify(zipFileName, selectedLetters.Count)
                        bundleNumber += 1
                        letterCount += selectedLetters.Count
                    Loop
                Catch ex As Exception
                    sendNotice = True
                    [error] = ex
                End Try

                If sendNotice Then Notify(zipFileName, [error])
                NotifyFinish()
            End Sub

            Private Sub Init(info As BundleInfo)
                Me.info = info
                Environment.CurrentDirectory = info.LetterFolderName
                letterNames = My.Computer.FileSystem.GetFiles(info.LetterFolderName, FileIO.SearchOption.SearchTopLevelOnly, Constants.LetterGlob).ToList()
            End Sub

            Private Sub Clean()
                Dim di As New DirectoryInfo(info.LetterFolderName)
                For Each fi As FileInfo In di.GetFiles(Constants.ZipGlob)
                    fi.Delete()
                Next
            End Sub

            Private Sub NotifyStart()
                RaiseEvent Notice(Me, New NotifyEventArgs(EventID.BundlingStarted))
            End Sub

            Private Function BundleName(bundleNumber As Integer) As String
                Dim fn As String
                Dim fmt As String = Constants.FirstZipFileFormat
                Dim args As IList(Of Object) = New List(Of Object)() From {DateTime.Today, info.FileIdentifier}

                If 0 <> bundleNumber Then
                    fmt = Constants.ZipFileFormat
                    args.Add(bundleNumber)
                End If
                fn = String.Format(fmt, args.ToArray())

                Return Path.Combine(info.LetterFolderName, fn)
            End Function

            Private Sub CreateEmptyZipFile(zipFileName As String)
                My.Computer.FileSystem.WriteAllText(Constants.NoDataFileName, Constants.NoDataContents, False)
                Using zf As New ZipFile(zipFileName)
                    zf.AddFile(Constants.NoDataFileName, String.Empty)
                    zf.Save()
                End Using
                Notify(zipFileName, 0)
            End Sub

            Private Overloads Sub Notify(fn As String, count As Integer)
                Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
                    {EventData.FileName, fn}, _
                    {EventData.Count, count} _
                }
                RaiseEvent Notice(Me, New NotifyEventArgs(EventID.BundleCreated, data))
            End Sub

            Private Overloads Sub Notify(fn As String, ex As Exception)
                Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
                    {EventData.FileName, fn} _
                }
                RaiseEvent Notice(Me, New NotifyEventArgs(EventID.BundleFailed, ex, data))
            End Sub

            Private Sub NotifyFinish()
                RaiseEvent Notice(Me, New NotifyEventArgs(EventID.BundlingFinished))
            End Sub
        End Class

    End Namespace
End Namespace
