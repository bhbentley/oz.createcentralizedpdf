﻿Imports System.Drawing
Imports System.Linq

Namespace Letters
    Namespace Pdf

        Public Class Page

            Public Class Constants
                Public Const LeftMarginAdjustment As Single = -2.0F
                Public Const WidthAdjustment As Single = 2.0F
                Public Const PostCardWrapAdjustment As Single = -10.0F
            End Class

            Public Class Margin
                Public Left As Single
                Public Right As Single
                Public Top As Single
                Public Bottom As Single
            End Class

            Public Class Props
                Public PageSize As SizeF
                Public Margins As Margin
            End Class

            Private contentBounds As RectangleF
            Private boundsFixed As Boolean

            Public Node As PageObject
            Public Stream As PageStream
            Public ImageObjectMap As IDictionary(Of Integer, String)
            Public FontObjectMap As IDictionary(Of Integer, String)
            Public NamedFonts As IDictionary(Of String, FontObject)
            Public Properties As Props
            Public Number As Integer

            Public Sub New()
                ImageObjectMap = New Dictionary(Of Integer, String)()
                FontObjectMap = New Dictionary(Of Integer, String)()
                NamedFonts = New Dictionary(Of String, FontObject)()
                boundsFixed = False
            End Sub

            Public Sub PopulateMaps()
                For Each xo In Node.Resources.ExternalObjects
                    ImageObjectMap.Add(xo.ObjectNumber, xo.Name)
                Next
                For Each f In Node.Resources.Fonts
                    FontObjectMap.Add(f.ObjectNumber, f.Name)
                Next
            End Sub

            Public Sub NameFonts(fonts As IList(Of FontObject))
                For Each fo As FontObject In fonts
                    Dim name As String = Nothing
                    If FontObjectMap.TryGetValue(fo.ObjectNumber, name) Then
                        NamedFonts.Add(name, fo)
                    End If
                Next
            End Sub

            Public Function MatchFields(fieldNames As IList(Of String)) As IList(Of String)
                Return Stream.FieldNames.Intersect(fieldNames).ToList()
            End Function

            Public Sub ReplaceFields(letterType As LetterType, fields As IList(Of IDictionary(Of String, String)), replaceLabel As Boolean)
                Dim replacement As New FieldReplacement() With { _
                    .Fields = fields, _
                    .Info = New FieldReplacement.PageInfo() With { _
                        .LetterType = letterType, _
                        .PageProperties = Me.Properties, _
                        .PageNumber = Me.Number _
                    } _
                }
                If Letters.LetterType.Postcard = letterType And Not boundsFixed Then
                    Dim sz As New SizeF(contentBounds.Width - Constants.WidthAdjustment, contentBounds.Height)
                    contentBounds.Size = sz
                    contentBounds.Offset(-Constants.LeftMarginAdjustment, 0)
                    Stream.SetContentBounds(contentBounds, contentBounds.Width + Constants.PostCardWrapAdjustment)
                    boundsFixed = True
                End If
                Stream.ReplaceFields(replacement, replaceLabel)
            End Sub

            Public Sub Render(letterType As LetterType)
                Stream.Render(NamedFonts)
            End Sub

            Public Sub AddWhiteOut(whiteOut As WhiteOutChunk)
                Stream.AddWhiteOut(whiteOut)
            End Sub

            Public Sub SetContentBounds()
                Dim left As Single
                Dim width As Single

                With Properties
                    left = .Margins.Left + Constants.LeftMarginAdjustment
                    width = .PageSize.Width - left - .Margins.Right + Constants.WidthAdjustment
                    contentBounds = New RectangleF(left, .Margins.Top, width, .PageSize.Height - .Margins.Top - .Margins.Bottom)
                    Stream.SetContentBounds(contentBounds, width)
                End With
            End Sub

            Public Function GetObjectNumbers() As Tuple(Of Integer, Integer)
                Return New Tuple(Of Integer, Integer)(Node.ObjectNumber, Stream.ObjectNumber)
            End Function

            Public Function Replicate(objectNumbers As Tuple(Of Integer, Integer), pageNumber As Integer) As Page
                Dim o As New Page() With { _
                    .ImageObjectMap = Me.ImageObjectMap, _
                    .FontObjectMap = Me.FontObjectMap, _
                    .NamedFonts = Me.NamedFonts, _
                    .Properties = Me.Properties, _
                    .Number = pageNumber, _
                    .Node = New PageObject() With { _
                        .ObjectNumber = objectNumbers.Item1, _
                        .MediaBox = Me.Node.MediaBox, _
                        .Resources = Me.Node.Resources, _
                        .Content = objectNumbers.Item2 _
                    }, _
                    .Stream = Me.Stream.Replicate(objectNumbers.Item2), _
                    .contentBounds = Me.contentBounds, _
                    .boundsFixed = Me.boundsFixed _
                }
                Return o
            End Function
        End Class

    End Namespace
End Namespace
