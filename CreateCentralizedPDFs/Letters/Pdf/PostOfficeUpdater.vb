﻿Imports System.Linq

Namespace Letters
    Namespace Pdf
        Public Class PostOfficeUpdater

            Public Class Constants
                Public Const UpdatesPerQuery As Integer = 500
                Public Const Schema As String = "post"
                Public Const Table As String = "post_office"
                Public Const LetterNumberColumn As String = "LTR_NUM"
                Public Const ProcessedColumn As String = "PROCESSED"
                Public Const UpdatedByColumn As String = "UPDATED_BY"
                Public Const CentralizedLettersPersonId As Long = 52
            End Class

            Private processed As IList(Of Long)

            Public Sub New(processed As IList(Of Long))
                Me.processed = processed
            End Sub

            Public Sub Update()
                Dim i As Integer = 0
                Dim tran As RDX.DQE.Transaction
                Dim ary As Long()

                If Not processed.Any() Then Return

                tran = New RDX.DQE.Transaction
                Do
                    ary = processed.Skip(i).Take(Constants.UpdatesPerQuery).ToArray()
                    If 0 = ary.Length Then Exit Do
                    AddUpdateToTransaction(ary, tran)
                    i += Constants.UpdatesPerQuery
                Loop
                RDX.Current.Provider.ExecuteTransaction(tran)
            End Sub

            Private Sub AddUpdateToTransaction(ary As Long(), tran As RDX.DQE.Transaction)
                Dim upd As New RDX.DQE.UpdateStatement(Constants.Schema, Constants.Table)

                upd.Filter = New RDX.DQE.InFilter(Constants.LetterNumberColumn, ary)
                upd.Set(Constants.ProcessedColumn, "Y")
                upd.Set(Constants.UpdatedByColumn, Constants.CentralizedLettersPersonId)
                tran.Add(upd)
            End Sub
        End Class

    End Namespace
End Namespace
