﻿Imports System.IO
Imports System.Linq

Namespace Letters
    Namespace Pdf

        Public Class LetterBatcher
            Implements Letters.LetterBatcher

            Public Class Constants
                Public Const DbTrue As String = "Y"
            End Class

            Private info As BatchInfo
            Private letterData As IList(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA))
            Private factory As Letters.TemplateFactory(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA))
            Private skip As ISet(Of Letters.TemplateKey)
            Private processed As IList(Of Long)
            Private emailSettingsChecker As EmailSettingsChecker

            Public Event Notice(sender As Object, e As NotifyEventArgs) Implements Letters.LetterBatcher.Notice

            Private ets As Action(Of Exception)
            Public WriteOnly Property ReportErrorMethod As Action(Of Exception) Implements Letters.LetterBatcher.ReportErrorMethod
                Set(value As Action(Of Exception))
                    ets = value
                End Set
            End Property

            Public Sub Run(info As BatchInfo) Implements Letters.LetterBatcher.Run
                Me.info = info
                Init()
                Try
                    SwitchToWorkingDirectory()
                    NotifyRunStart()
                    LoadLetterData()
                    LoadTemplates()
                    MakeLetters()
                    UpdateProcessedFlag()
                    NotifyRunEnd()
                Catch ex As Exception
                    Notify(New NotifyEventArgs(EventID.BatchFailed, ex))
                    Throw ex
                End Try
            End Sub

            Private Sub Init()
                Me.skip = New HashSet(Of Letters.TemplateKey)()
                Me.processed = New List(Of Long)()
            End Sub

            Private Sub Notify(notify As NotifyEventArgs)
                Dim raiseFailure As Exception = Nothing

                Try
                    RaiseEvent Notice(Me, notify)
                Catch ex As Exception
                    raiseFailure = ex
                End Try

                If Not raiseFailure Is Nothing Then
                    ReportError(raiseFailure)
                End If
            End Sub

            Private Sub ReportError(ex As Exception)
                ets(ex)
            End Sub

            Private Sub SwitchToWorkingDirectory()
                info.WorkingDirectory.Create()
                Environment.CurrentDirectory = info.WorkingDirectory.FullName
            End Sub

            Private Sub TurnOnWhiteOut()
                PageRenderer.WhiteOutType = info.WhiteOutType
            End Sub

            Private Sub TurnOffWhiteOut()
                PageRenderer.WhiteOutType = Nothing
            End Sub

            Private Sub NotifyRunStart()
                Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
                    {EventData.Info, info} _
                }
                Notify(New NotifyEventArgs(EventID.BatchStarted, data))
            End Sub

            Private Sub LoadLetterData()
                Dim settings As New Letters.Pdf.LetterLoader.LoadSettings() With { _
                    .EarliestScheduledDate = info.LetterDateRange.StartDate, _
                    .Processed = info.Processed, _
                    .ExportID = info.ExportID, _
                    .Templates = info.Templates _
                }
                Dim loader As New Letters.Pdf.LetterLoader(settings)
                letterData = loader.Load()
                NotifyDataLoaded()
            End Sub

            Private Sub NotifyDataLoaded()
                Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
                    {EventData.Count, letterData.Count} _
                }
                Notify(New NotifyEventArgs(EventID.LetterDataLoaded, data))
            End Sub

            Private Sub LoadTemplates()
                Dim loader As New Letters.Pdf.DatabaseTemplateLoader(letterData, info.MaximumPageCountPerLetterType)
                Dim impl As New Letters.TemplateFactory(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)).Details() With { _
                    .Builder = New Letters.Pdf.TemplateBuilder(), _
                    .Cache = New Letters.MostRecentTemplateCache(), _
                    .FieldMapper = New Letters.Pdf.FlexFieldMapper(), _
                    .Loader = loader _
                }

                AddHandler loader.Notice, AddressOf HandleBadTemplate
                factory = New Letters.TemplateFactory(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA))(impl)
                NotifyTemplatesLoaded()
            End Sub

            Private Sub HandleBadTemplate(sender As Object, e As NotifyEventArgs)
                Notify(e)
            End Sub

            Private Sub NotifyTemplatesLoaded()
                Notify(New NotifyEventArgs(EventID.TemplatesLoaded))
            End Sub

            Private Sub MakeLetters()
                Dim key As TemplateKey
                Dim notify As NotifyEventArgs = Nothing

                Dim rentalIds = letterData.SelectMany(Function(ltrData) ltrData) _
                                          .Select(Function(l) l.RENTAL_ID).Distinct().ToArray()

                Dim siteIds = letterData.SelectMany(Function(ltrData) ltrData) _
                                         .Select(Function(l) l.SITE_ID).Distinct().ToArray()

                Dim emailSettingsChecker = New EmailSettingsChecker(New EmailSettingsDataProvider(rentalIds, siteIds))

                For Each ld In letterData
                    key = New Letters.TemplateKey() With {.Identifier = ld.First().LTR_TYPE_ID, .Version = ld.First().VERSION}
                    If Not skip.Contains(key) Then notify = MakeLetter(key, ld, emailSettingsChecker)
                    If Not notify Is Nothing Then Me.Notify(notify)
                    notify = Nothing
                Next
                NotifyLettersDone()
            End Sub

            Private Function MakeLetter(key As Letters.TemplateKey, ld As IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA), _
                                        emailSettingsChecker As EmailSettingsChecker) As NotifyEventArgs
                Dim t As Tuple(Of Letters.LetterTemplate, FieldMapper(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)))
                Dim tmpl As Letters.LetterTemplate
                Dim mapper As FieldMapper(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA))

                Try
                    t = GetTemplate(key)
                Catch ex As Exception
                    skip.Add(key)
                    Return CreateBadTemplateNotice(ex, key)
                End Try

                If t.Item1 Is Nothing Then Return Nothing
                tmpl = t.Item1
                mapper = t.Item2

                Try
                    CreatePdf(tmpl, mapper, ld, emailSettingsChecker)
                    processed.Add(ld.First().LTR_NUM)
                Catch ex As Exception
                    Return CreateDroppedLetterNotice(ex, key, ld.First().LTR_NUM)
                End Try

                Return Nothing
            End Function

            Private Function GetTemplate(key As Letters.TemplateKey) As Tuple(Of Letters.LetterTemplate, FieldMapper(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)))
                Dim tmpl As Letters.LetterTemplate = Nothing
                Dim mapper As FieldMapper(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)) = Nothing

                tmpl = factory.CreateTemplate(key)
                If Not tmpl Is Nothing Then
                    mapper = factory.CreateFieldMapper(key)
                Else
                    skip.Add(key)
                End If

                Return New Tuple(Of Letters.LetterTemplate, FieldMapper(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)))(tmpl, mapper)
            End Function

            Private Function CreateBadTemplateNotice(ex As Exception, key As Letters.TemplateKey) As NotifyEventArgs
                Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
                    {EventData.TemplateKey, key} _
                }
                Return New NotifyEventArgs(EventID.BadTemplate, ex, data)
            End Function

            Private Function CreateDroppedLetterNotice(ex As Exception, key As Letters.TemplateKey, n As Long) As NotifyEventArgs
                Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
                    {EventData.TemplateKey, key}, _
                    {EventData.LetterNumber, n} _
                }
                Return New NotifyEventArgs(EventID.DroppedLetter, ex, data)
            End Function

            Private Sub CreatePdf(tmpl As Letters.LetterTemplate, mapper As FieldMapper(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)), ld As IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA), _
                                  emailSettingsChecker As EmailSettingsChecker)
                Dim type As Letters.LetterType = Letters.LetterType.PlainLetter
                Dim certified As Boolean = False
                Dim letterNumber As Long = 0
                Dim ltr As Letters.Letter
                Dim folder = Environment.CurrentDirectory
                Dim replaceLabel As Boolean = False

                With ld.First()
                    If Not String.IsNullOrEmpty(.CERTIFIED) Then certified = Constants.DbTrue = .CERTIFIED
                    If .PRINT_SPECIAL.HasValue Then
                        Select Case .PRINT_SPECIAL.Value
                            Case 0D
                                If certified Then type = Letters.LetterType.CertifiedLetter
                            Case 3D
                                type = Letters.LetterType.Postcard
                            Case 6D
                                type = Letters.LetterType.CertifiedLetter
                        End Select
                    ElseIf certified Then
                        type = Letters.LetterType.CertifiedLetter
                    End If
                    letterNumber = .LTR_NUM

                    If Not .IGNORE_EMAIL_SETTING AndAlso emailSettingsChecker IsNot Nothing AndAlso emailSettingsChecker.GetSetting(.RENTAL_ID, .SITE_ID, CType(.LTR_TYPE_GROUP, LetterTypeGroup)) Then
                        folder = GetEmailFolder()
                        TurnOffWhiteOut()
                        replaceLabel = True
                    Else
                        TurnOnWhiteOut()
                    End If
                End With

                ltr = tmpl.MakeLetter(type, mapper.Map(ld), replaceLabel)

                Using stm As New FileStream(Path.Combine(folder, String.Format("{0}.pdf", letterNumber)), FileMode.Create, FileAccess.Write, FileShare.Read)
                    ltr.Write(stm)
                End Using
                With ld.First()
                    NotifyLetterCreated(.LTR_TYPE_ID, .VERSION, .LTR_NUM, ld.Count)
                End With
            End Sub

            Private Function GetEmailFolder() As String
                Dim emailFolder = My.Settings.EmailFolder

                If Not Directory.Exists(emailFolder) Then
                    Directory.CreateDirectory(emailFolder)
                End If

                Return emailFolder
            End Function

            Private Sub NotifyLetterCreated(templateId As Long, templateVersion As Integer, letterNumber As Long, pageCount As Integer)
                Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
                    {EventData.TemplateKey, New TemplateKey() With {.Identifier = templateId, .Version = templateVersion}}, _
                    {EventData.LetterNumber, letterNumber}, _
                    {EventData.Count, pageCount} _
                }
                Notify(New NotifyEventArgs(EventID.LetterCreated, data))
            End Sub

            Private Sub NotifyLettersDone()
                Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
                    {EventData.Count, processed.Count} _
                }
                Notify(New NotifyEventArgs(EventID.LettersDone, data))
            End Sub

            Private Sub UpdateProcessedFlag()
                If Not info.Update Then Return
                Dim updater As New PostOfficeUpdater(processed)
                updater.Update()
            End Sub

            Private Sub NotifyRunEnd()
                Notify(New NotifyEventArgs(EventID.BatchFinished))
            End Sub
        End Class

    End Namespace
End Namespace
