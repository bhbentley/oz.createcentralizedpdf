﻿Imports System.Drawing
Imports System.IO
Imports System.Linq
Imports System.Text

Namespace Letters
    Namespace Pdf

        Public Class LetterWriter
            Public Class Constants
                Public Const LineTerminator As Byte = 10
                Public Const LineEnding As String = vbLf
                Public Const Header As String = "%PDF-1.6"
                Public Const Catalog As String = "1 0 obj << /Type /Catalog /Pages 2 0 R >> endobj"
                Public Const PageTreeRoot As String = "2 0 obj << /Type /Pages /Kids [{0}{2}{0}] /Count {1} >> endobj"
                Public Const FontKey As String = "/Font"
                Public Const ExternalObjectKey As String = "/XObject"
                Public Const ResourceList As String = "{0}{1}<<{0}{2}{0}>>{0}"
                Public Const PageObject As String = "{1} 0 obj{0}<< /Type /Page /Parent 2 0 R /MediaBox [{2}] /Resources{0}<<{0}{3}{4}/ProcSet [{5}]{0}>>{0}/Contents {6} 0 R{0}>>{0}endobj"
                Public Const FontObject As String = "{0} 0 obj << /Type /Font /BaseFont /{1}{2} /Subtype /TrueType /Encoding /WinAnsiEncoding >> endobj"
                Public Const ImageObject As String = "{0} 0 obj << /Type /XObject /Subtype /Image /Name /X /Width {1} /Height {2} /BitsPerComponent 8 /ColorSpace /DeviceRGB /Filter /DCTDecode /Length {3} >>"
                Public Const PageStream As String = "{0} 0 obj << /Filter /FlateDecode /Length {1} >>"
                Public Const StreamStart As String = "stream"
                Public Const StreamEnd As String = "endstream" & Constants.LineEnding & "endobj"
                Public Const XrefTableStart As String = "xref"
                Public Const Trailer As String = "trailer{0}<< /Size {1} /Root 1 0 R >>"
                Public Const StartXref As String = "startxref"
                Public Const EndOfFile As String = "%%EOF"
            End Class

            Private stream As Stream
            Private letter As Letter
            Private writer As BinaryWriter
            Private positionMap As IDictionary(Of Integer, Integer)
            Private xrefPosition As Integer

            Public Sub New(letter As Letter, stream As Stream)
                Me.letter = letter
                Me.stream = stream
                Me.positionMap = New Dictionary(Of Integer, Integer)()
            End Sub

            Public Sub Write()
                Using writer As New BinaryWriter(New BufferedStream(stream), Encoding.ASCII)
                    Me.writer = writer
                    writer.BaseStream.Seek(0, SeekOrigin.Begin)
                    WriteHeader()
                    WriteCatalog()
                    WritePageTreeRoot()
                    WriteFonts()
                    WriteImages()
                    WritePageObjects()
                    WritePageStreams()
                    WriteXrefTable()
                    WriteTrailer()
                    WriteEof()
                End Using
                Me.writer = Nothing
            End Sub

            Private Sub WriteHeader()
                WriteBytes(Constants.Header)
                NewLine()
            End Sub

            Private Sub WriteCatalog()
                MarkPosition(1)
                WriteBytes(Constants.Catalog)
                NewLine()
            End Sub

            Private Sub WritePageTreeRoot()
                Dim s As String = String.Format(Constants.PageTreeRoot, Constants.LineEnding, letter.Pages.Count, BuildPageReferences())

                MarkPosition(2)
                WriteBytes(s)
                NewLine()
            End Sub

            Private Function BuildPageReferences() As String
                Return String.Join(Constants.LineEnding, letter.Pages.OrderBy(Function(x) x.Node.ObjectNumber).Select(Function(x) String.Format("{0} 0 R", x.Node.ObjectNumber)).ToArray())
            End Function

            Private Sub WriteFonts()
                For Each f In letter.Fonts
                    Dim s As String = String.Format(Constants.FontObject, f.ObjectNumber, f.BaseFont, BuildStyle(f))

                    MarkPosition(f.ObjectNumber)
                    WriteBytes(s)
                    NewLine()
                Next
            End Sub

            Private Function BuildStyle(f As FontObject) As String
                Dim sb As New StringBuilder()

                If FontStyle.Bold = (f.Style And FontStyle.Bold) Then sb.Append([Enum].GetName(GetType(FontStyle), FontStyle.Bold))
                If FontStyle.Italic = (f.Style And FontStyle.Italic) Then sb.Append([Enum].GetName(GetType(FontStyle), FontStyle.Italic))
                If 0 <> sb.Length Then sb.Insert(0, ",")

                Return sb.ToString()
            End Function

            Private Sub WriteImages()
                For Each im In letter.Images
                    Dim s As String = String.Format(Constants.ImageObject, im.ObjectNumber, im.Width, im.Height, im.Content.Length)

                    MarkPosition(im.ObjectNumber)
                    WriteBytes(s)
                    NewLine()
                    WriteBytes(Constants.StreamStart)
                    NewLine()
                    writer.Write(im.Content)
                    NewLine()
                    WriteBytes(Constants.StreamEnd)
                    NewLine()
                Next
            End Sub

            Private Sub WritePageObjects()
                For Each pg In letter.Pages
                    Dim s As String = String.Format(Constants.PageObject, _
                        Constants.LineEnding, _
                        pg.Node.ObjectNumber, _
                        BuildBoxCoordinates(pg.Node.MediaBox), _
                        BuildResource(Constants.FontKey, pg.FontObjectMap), _
                        BuildResource(Constants.ExternalObjectKey, pg.ImageObjectMap), _
                        String.Join(" ", pg.Node.Resources.ProcessingSet), _
                        pg.Node.Content
                    )

                    MarkPosition(pg.Node.ObjectNumber)
                    WriteBytes(s)
                    NewLine()
                Next
            End Sub

            Private Sub WritePageStreams()
                Dim b As Byte() = New Byte(4095) {}

                For Each pg In letter.Pages
                    Dim ps As PageStream = pg.Stream
                    Dim n As Integer = ps.CopyTo(b)
                    Dim s As String

                    n = Flate.Compress(b, n)
                    s = String.Format(Constants.PageStream, ps.ObjectNumber, n)
                    MarkPosition(ps.ObjectNumber)
                    WriteBytes(s)
                    NewLine()
                    WriteBytes(Constants.StreamStart)
                    NewLine()
                    writer.Write(Flate.CompressionBuffer, 0, n)
                    NewLine()
                    WriteBytes(Constants.StreamEnd)
                    NewLine()
                Next
            End Sub

            Private Sub WriteXrefTable()
                Dim s As String

                xrefPosition = CInt(writer.BaseStream.Position)
                WriteBytes(Constants.XrefTableStart)
                NewLine()
                s = String.Format("0 {0}", positionMap.Count + 1)
                WriteBytes(s)
                NewLine()
                s = "0000000000 65535 f "
                WriteBytes(s)
                NewLine()
                For Each kvp In positionMap.OrderBy(Function(x) x.Key)
                    s = String.Format("{0:0000000000} 00000 n ", kvp.Value)
                    WriteBytes(s)
                    NewLine()
                Next
            End Sub

            Private Sub WriteTrailer()
                Dim s As String = String.Format(Constants.Trailer, Constants.LineEnding, positionMap.Count + 1)
                WriteBytes(s)
                NewLine()
                WriteBytes(Constants.StartXref)
                NewLine()
                s = String.Format("{0}", xrefPosition)
                WriteBytes(s)
                NewLine()
            End Sub

            Private Sub WriteEof()
                WriteBytes(Constants.EndOfFile)
                NewLine()
            End Sub

            Private Function BuildBoxCoordinates(bb As BoundingBox) As String
                Return String.Format("{0} {1} {2} {3}", bb.LowerLeft.X, bb.LowerLeft.Y, bb.UpperRight.X, bb.UpperRight.Y)
            End Function

            Private Function BuildResource(name As String, objects As IDictionary(Of Integer, String)) As String
                If 0 = objects.Count Then Return String.Empty
                Return String.Format("{1}{0}<<{0}{2}{0}>>{0}", Constants.LineEnding, name, BuildNamedObjectList(objects))
            End Function

            Private Function BuildNamedObjectList(objects As IDictionary(Of Integer, String)) As String
                Return String.Join(Constants.LineEnding, objects.OrderBy(Function(x) x.Value).Select(Function(x) String.Format("{1} {0} 0 R", x.Key, x.Value)).ToArray())
            End Function

            Private Sub MarkPosition(objectNumber As Integer)
                positionMap.Add(objectNumber, CInt(writer.BaseStream.Position))
            End Sub

            Private Sub WriteBytes(s As String)
                writer.Write(Encoding.ASCII.GetBytes(s))
            End Sub

            Private Sub NewLine()
                writer.Write(Constants.LineTerminator)
            End Sub
        End Class

    End Namespace
End Namespace
