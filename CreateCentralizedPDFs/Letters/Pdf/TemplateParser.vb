﻿Imports System.IO
Imports System.Linq
Imports System.Text

Namespace Letters
    Namespace Pdf
        Namespace TemplateParser

#Region "Constants"

            Public Class Constants
                Public Class ParseOrder
                    Public Const Header As Integer = 1
                    Public Const PageTree As Integer = 2
                    Public Const Catalog As Integer = 3
                    Public Const PageObjects As Integer = 4
                    Public Const ImageStreams As Integer = 5
                    Public Const Fonts As Integer = 6
                    Public Const PageStreams As Integer = 7
                    Public Const UserSettings As Integer = 8
                    Public Const XRef As Integer = 9
                    Public Const TrailingComments As Integer = 10
                End Class
                Public Class Token
                    Public Const ObjectStart As String = "obj"
                    Public Const ObjectEnd As String = "endobj"
                    Public Const StreamStart As String = "stream"
                    Public Const StreamEnd As String = "endstream"
                    Public Const EndOfFile As String = "%%EOF"
                    Public Const DictionaryStart As String = "<<"
                    Public Const DictionaryEnd As String = ">>"
                    Public Const ArrayStart As String = "["
                    Public Const ArrayEnd As String = "]"
                    Public Const ExternalObject As String = "/XObject"
                End Class
            End Class

#End Region

#Region "Utility Routines"

            Public Module Utility
                Public Function ReadLine(reader As BinaryReader) As String
                    Const cr As Integer = Asc(vbCr)
                    Const lf As Integer = Asc(vbLf)
                    Dim sb As New StringBuilder()
                    Dim c As Byte

                    Do While reader.BaseStream.Length > reader.BaseStream.Position
                        c = reader.ReadByte()
                        If cr <> c Then
                            sb.Append(Convert.ToChar(c))
                        Else
                            If reader.BaseStream.Length = reader.BaseStream.Position Then Throw New TemplateException("broken final end-of-line marker")
                            c = reader.ReadByte()
                            If lf = c Then Exit Do
                            sb.Append(vbCr)
                            sb.Append(c)
                        End If
                    Loop

                    Return sb.ToString()
                End Function

                Public Function ReadStream(reader As BinaryReader) As String
                    Return ReadToToken(reader, Constants.Token.StreamStart)
                End Function

                Public Function ReadObject(reader As BinaryReader) As String
                    Return ReadToToken(reader, Constants.Token.ObjectEnd)
                End Function

                Public Function ReadEof(reader As BinaryReader) As String
                    Return ReadToToken(reader, Constants.Token.EndOfFile)
                End Function

                Private Function ReadToToken(reader As BinaryReader, token As String) As String
                    Dim sb As New StringBuilder()
                    Dim found As Boolean = False
                    Dim n As Integer = token.Length
                    Dim np2 As Integer = n + 2
                    Dim k As Integer, l As Integer
                    Dim s As String

                    Do
                        sb.AppendLine(ReadToEolm(reader))
                        l = sb.Length
                        If l >= n Then
                            k = Math.Min(l, np2)
                            s = sb.ToString(l - k, k)
                            If s.Contains(token) Then
                                found = True
                                Exit Do
                            End If
                        End If
                    Loop While reader.BaseStream.Length > reader.BaseStream.Position

                    If Not found Then Throw New TemplateException(String.Format("token [{0}] not found", token))

                    Return sb.ToString()
                End Function

                Private Function ReadToEolm(reader As BinaryReader) As String
                    Const cr As Integer = Asc(vbCr)
                    Const lf As Integer = Asc(vbLf)
                    Dim sb As New StringBuilder()
                    Dim c As Byte

                    Do While reader.BaseStream.Length > reader.BaseStream.Position
                        c = reader.ReadByte()
                        If cr <> c And lf <> c Then
                            sb.Append(Convert.ToChar(c))
                        Else
                            If cr = c And reader.BaseStream.Length > reader.BaseStream.Position Then
                                c = reader.ReadByte()
                                If lf <> c Then reader.BaseStream.Seek(reader.BaseStream.Position - 1, SeekOrigin.Begin)
                            End If
                            Exit Do
                        End If
                    Loop

                    Return sb.ToString()
                End Function

                Public Function GetArray(s As String, token As String) As IList(Of String)
                    Return GetCollection(s, token, Constants.Token.ArrayStart, Constants.Token.ArrayEnd, "array")
                End Function

                Public Function GetDictionary(s As String, token As String) As IList(Of String)
                    Return GetCollection(s, token, Constants.Token.DictionaryStart, Constants.Token.DictionaryEnd, "dictionary")
                End Function

                Private Function GetCollection(s As String, token As String, collectionStart As String, collectionEnd As String, type As String) As IList(Of String)
                    Dim i As Integer = s.IndexOf(token)
                    Dim parts As String() = Nothing
                    Dim j As Integer

                    If -1 = i Then Return New List(Of String)()
                    i = s.IndexOf(collectionStart, i + token.Length)
                    If -1 = i Then Throw New TemplateException(String.Format("malformed {0}", type))
                    i += collectionStart.Length
                    j = s.IndexOf(collectionEnd, i)
                    If -1 = j Then Throw New TemplateException(String.Format("malformed {0}", type))
                    parts = s.Substring(i, j - i).Split({" "}, StringSplitOptions.RemoveEmptyEntries)

                    Return parts
                End Function
            End Module

#End Region

            Public Class TemplateParser
                Private Class TemplateContent
                    Public fonts As IList(Of FontObject)
                    Public images As IList(Of ImageStream)
                    Public pages As IList(Of PageObject)
                    Public pageContents As IList(Of PageStream)
                    Public settings As UserSettings
                End Class

                Private stream As Stream
                Private content As TemplateContent

                Public Sub New(stream As Stream)
                    Me.stream = stream
                    Me.content = New TemplateContent()
                End Sub

                Public ReadOnly Property Fonts As IList(Of FontObject)
                    Get
                        Return content.fonts
                    End Get
                End Property

                Public ReadOnly Property Images As IList(Of ImageStream)
                    Get
                        Return content.images
                    End Get
                End Property

                Public ReadOnly Property Pages As IList(Of PageObject)
                    Get
                        Return content.pages
                    End Get
                End Property

                Public ReadOnly Property PageContents As IList(Of PageStream)
                    Get
                        Return content.pageContents
                    End Get
                End Property

                Public ReadOnly Property UserSettings As UserSettings
                    Get
                        Return content.settings
                    End Get
                End Property

                Public Sub Parse()
                    ' This is not how PDFs should be read but I'm just following what went before to maintain as much compatibility as possible.
                    Using reader As New BinaryReader(New BufferedStream(stream), Encoding.ASCII)
                        Dim parsers As New Dictionary(Of Type, ObjectParser)() From { _
                            {GetType(HeaderParser), New HeaderParser(reader)}, _
                            {GetType(PageTreeParser), New PageTreeParser(reader)}, _
                            {GetType(CatalogParser), New CatalogParser(reader)}, _
                            {GetType(PageObjectParser), New PageObjectParser(reader)}, _
                            {GetType(ImageStreamParser), New ImageStreamParser(reader)}, _
                            {GetType(FontParser), New FontParser(reader)}, _
                            {GetType(PageStreamParser), New PageStreamParser(reader)}, _
                            {GetType(UserSettingsParser), New UserSettingsParser(reader)}, _
                            {GetType(XrefParser), New XrefParser(reader)}, _
                            {GetType(TrailingCommentParser), New TrailingCommentParser(reader)} _
                        }
                        Dim n As Integer

                        reader.BaseStream.Seek(0, SeekOrigin.Begin)
                        For Each parser As ObjectParser In parsers.OrderBy(Function(x) x.Value.Order).Select(Function(x) x.Value)
                            parser.Parse()
                        Next

                        n = CType(parsers(GetType(PageTreeParser)), PageTreeParser).PageCount
                        content.pages = CType(parsers(GetType(PageObjectParser)), PageObjectParser).PageObjects
                        If content.pages Is Nothing OrElse n <> content.pages.Count Then Throw New TemplateException(String.Format("bad page count, expected {1} page{2}, got {0}", content.pages.Count, n, IIf(1 = n, String.Empty, "s")))
                        content.pageContents = CType(parsers(GetType(PageStreamParser)), PageStreamParser).Streams
                        content.fonts = CType(parsers(GetType(FontParser)), FontParser).AvailableFonts
                        If 0 = content.fonts.Count Then Throw New TemplateException("no fonts defined")
                        content.images = CType(parsers(GetType(ImageStreamParser)), ImageStreamParser).Streams
                        content.settings = CType(parsers(GetType(UserSettingsParser)), UserSettingsParser).UserSettings
                    End Using
                End Sub
            End Class

        End Namespace
    End Namespace
End Namespace
