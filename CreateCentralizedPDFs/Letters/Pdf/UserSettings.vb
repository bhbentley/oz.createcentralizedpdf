﻿Imports System.IO
Imports System.Runtime.Serialization
Imports System.Xml.Serialization

Namespace Letters
    Namespace Pdf
        Namespace TemplateParser

            Public Module Deserializer
                Private xs As XmlSerializer = Nothing

                Public Function XmlToObject(Of T)(s As String) As T
                    If xs Is Nothing Then xs = New XmlSerializer(GetType(T))
                    Using sr As New StringReader(s)
                        Return DirectCast(xs.Deserialize(sr), T)
                    End Using
                End Function
            End Module

            <System.Serializable(), _
                 XmlRoot("clsPostDefineCol"), _
                 XmlType("clsPostDefineCol"), _
                 DataContract(name:="clsPostDefineCol", Namespace:="http://www.centershift.com/STORE40/")> _
            Public NotInheritable Class FlexDefine
                '--items that will be stored in the post_defines table for custom flex fields
                <XmlElement("name"), _
                 DataMember(name:="name", EmitDefaultValue:=False)> _
                Public name As String           '--label displayed/replaced in the pdf
                <XmlElement("dataAccess"), _
                 DataMember(name:="dataAccess", EmitDefaultValue:=False)> _
                Public dataAccess As String     '--typical name of function/prodedure/view/tbl
                <XmlElement("ffType"), _
                 DataMember(name:="ffType", EmitDefaultValue:=False)> _
                Public ffType As String         '--</base> or </flex> or clsFlexFields.FLEX_END_TAG or BASE_END_TAG
                <XmlElement("repeating"), _
                 DataMember(name:="repeating", EmitDefaultValue:=False)> _
                Public repeating As Boolean     '--repeating flexField
                <XmlElement("numTimesUsed"), _
                 DataMember(name:="numTimesUsed", EmitDefaultValue:=False)> _
                Public numTimesUsed As Integer  '--must be zero before it can be removed from the collection
                <XmlElement("numTimesRepeat"), _
                 DataMember(name:="numTimesRepeat", EmitDefaultValue:=False)> _
                Public numTimesRepeat As Integer '--only applies to repeating flexfields
            End Class

            <Serializable(), _
             XmlRoot("pdfUserSettings"), _
             XmlType("pdfUserSettings"), _
             DataContract(Name:="pdfUserSettings", Namespace:="http://www.centershift.com/STORE40/")> _
            Public Structure UserSettings
                <XmlElement("pkOverrideId"), _
                 DataMember(Name:="pkOverrideId", EmitDefaultValue:=False)> _
                Public pkOverrideId As Long '--only used to override the trigger on generic templates (ltr_type_id or lease_def_id)
                <XmlElement("newTemplate"), _
                 DataMember(Name:="newTemplate", EmitDefaultValue:=False)> _
                Public newTemplate As Boolean
                <XmlElement("databaseName"), _
                 DataMember(Name:="databaseName", EmitDefaultValue:=False)> _
                Public databaseName As String
                <XmlElement("displayName"), _
                 DataMember(Name:="displayName", EmitDefaultValue:=False)> _
                Public displayName As String
                <XmlArray("ffs"), _
                 DataMember(Name:="ffs", EmitDefaultValue:=False)> _
                Public ffs As List(Of FlexDefine)
                <XmlElement("letterType"), _
                 DataMember(Name:="letterType", EmitDefaultValue:=False)> _
                Public letterType As Integer '--lookup 1076
                <XmlElement("printSpecial"), _
                 DataMember(Name:="printSpecial", EmitDefaultValue:=False)> _
                Public printSpecial As Integer '--lookup 1228
                <XmlElement("envelopeType"), _
                 DataMember(Name:="envelopeType", EmitDefaultValue:=False)> _
                Public envelopeType As Integer '--value_id
                <XmlElement("language"), _
                 DataMember(Name:="language", EmitDefaultValue:=False)> _
                Public language As Integer
                <XmlElement("state"), _
                 DataMember(Name:="state", EmitDefaultValue:=False)> _
                Public state As Integer
                <XmlElement("paperWd"), _
                 DataMember(Name:="paperWd", EmitDefaultValue:=False)> _
                Public paperWd As Decimal
                <XmlElement("paperHt"), _
                 DataMember(Name:="paperHt", EmitDefaultValue:=False)> _
                Public paperHt As Decimal
                <XmlElement("marginLR"), _
                 DataMember(Name:="marginLR", EmitDefaultValue:=False)> _
                Public marginLR As Decimal
                <XmlElement("marginTB"), _
                 DataMember(Name:="marginTB", EmitDefaultValue:=False)> _
                Public marginTB As Decimal
                <XmlElement("siteId"), _
                 DataMember(Name:="siteId", EmitDefaultValue:=False)> _
                Public siteId As Long
            End Structure

        End Namespace
    End Namespace
End Namespace
