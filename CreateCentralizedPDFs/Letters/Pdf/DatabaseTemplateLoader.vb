﻿Imports System.Linq

Namespace Letters
    Namespace Pdf

        Public Class DatabaseTemplateLoader
            Implements Letters.TemplateLoader

            Public Class Constants
                Public Const TemplateArgumentName As String = "TEMPLATE_BLOB"
            End Class

            Private templates As System.Collections.Generic.IDictionary(Of TemplateKey, TemplateDefinition)
            Private mplt As IDictionary(Of Letters.LetterType, Integer)

            Public Event Notice(sender As Object, e As NotifyEventArgs)

            Public Sub New(letterData As IList(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)), maximumPagesPerLetterType As IDictionary(Of Letters.LetterType, Integer))
                Me.templates = New System.Collections.Generic.Dictionary(Of TemplateKey, TemplateDefinition)()
                Me.mplt = maximumPagesPerLetterType
                GetUniqueTemplateKeys(letterData)
            End Sub

            Private Sub GetUniqueTemplateKeys(letterData As IList(Of IList(Of Framework.Database.APPL.POST_CENTRALIZED_LTR_DATA)))
                Dim id As Long = 0
                Dim version As Integer = 0

                For Each ld In letterData
                    With ld.First()
                        If id <> .LTR_TYPE_ID Or version <> .VERSION Then
                            id = .LTR_TYPE_ID
                            version = .VERSION
                            templates.Add(New TemplateKey() With {.Identifier = id, .Version = version}, New TemplateDefinition() With {.Fields = New List(Of FieldDefinition)(), .MaximumPageCountPerLetterType = mplt})
                        End If
                    End With
                Next
            End Sub

            Public ReadOnly Property Definitions As System.Collections.Generic.IDictionary(Of TemplateKey, TemplateDefinition) Implements Letters.TemplateLoader.Definitions
                Get
                    Return templates
                End Get
            End Property

            Public Sub Load() Implements Letters.TemplateLoader.Load
                Dim badTemplates As IList(Of TemplateKey) = New List(Of TemplateKey)()

                For Each kvp In templates
                    If LoadTemplateBlob(kvp.Key, kvp.Key.Identifier, kvp.Key.Version) Then
                        LoadFlexFields(kvp.Key, kvp.Key.Identifier, kvp.Key.Version)
                    Else
                        badTemplates.Add(kvp.Key)
                    End If
                Next

                For Each key As TemplateKey In badTemplates
                    templates.Remove(key)
                Next
            End Sub

            Private Function LoadTemplateBlob(key As TemplateKey, id As Long, version As Integer) As Boolean
                Dim tmpl As New Framework.Database.APPL.POST_CENTRALIZED_TEMPLATES.Collection()
                Dim badTemplate As Boolean

                tmpl.LoadByLTR_TYPE_IDandVERSION(id, version)
                badTemplate = 0 = tmpl.Count
                If Not badTemplate Then badTemplate = tmpl(0).TEMPLATE_BLOB Is Nothing
                If Not badTemplate Then badTemplate = 0 = tmpl(0).TEMPLATE_BLOB.Length
                If Not badTemplate Then templates(key).pdf = tmpl(0).TEMPLATE_BLOB
                If badTemplate Then Notify(CreateBadTemplateNotice(key))

                Return Not badTemplate
            End Function

            Private Function CreateBadTemplateNotice(key As Letters.TemplateKey) As NotifyEventArgs
                Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
                    {EventData.TemplateKey, key} _
                }
                Return New NotifyEventArgs(EventID.BadTemplate, New ArgumentNullException(Constants.TemplateArgumentName), data)
            End Function

            Private Sub Notify(notify As NotifyEventArgs)
                RaiseEvent Notice(Me, notify)
            End Sub

            Private Sub LoadFlexFields(key As TemplateKey, id As Long, version As Integer)
                Dim col As New Framework.Database.APPL.POST_CENTRALIZED_FLEX.Collection()

                col.LoadByLTR_TYPE_IDandVERSION(id, version)
                For Each ff In col
                    templates(key).Fields.Add(New FieldDefinition() With { _
                        .Identifier = ff.FLEX_ID.Value, _
                        .Name = ff.FIELD_NAME, _
                        .Number = ff.FLEX_COLUMN.Value, _
                        .IsRepeat = Framework.TypeConverters.BooleanStringConverter.StringToBoolean(ff.IS_REPEAT), _
                        .RepeatMax = ff.MAX_COUNT.Value _
                    })
                Next
            End Sub
        End Class

    End Namespace
End Namespace
