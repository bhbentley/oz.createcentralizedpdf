﻿Imports System.Drawing
Imports System.Text
Imports System.Text.RegularExpressions

Namespace Letters
    Namespace Pdf

        Public Interface Segment
            Sub Render(sb As StringBuilder)
        End Interface

        Public Class TextSegment
            Implements Segment

            Public Class Constants
                Public Const TextStartToken As String = "("
                Public Const TextEndToken As String = ")Tj"

                Public Class Escape
                    Public Const Pattern As String = "([\\()])"
                    Public Shared ReadOnly Rex As New Regex(Pattern, RegexOptions.Compiled)
                    Public Const Replacement As String = "\$1"
                End Class

                Public Class Unescape
                    Public Const Pattern As String = "\\([()\\])"
                    Public Shared ReadOnly Rex As New Regex(Pattern, RegexOptions.Compiled)
                    Public Const Replacement As String = "$1"
                End Class
            End Class

            Public Text As String

            Public Sub Render(sb As StringBuilder) Implements Segment.Render
                sb.Append(Constants.Escape.Rex.Replace(Text, Constants.Escape.Replacement))
            End Sub

            Public Sub Unescape()
                Text = Constants.Unescape.Rex.Replace(Text, Constants.Unescape.Replacement)
            End Sub
        End Class

        Public Class FieldSegment
            Inherits TextSegment

            Enum FieldType
                Base = 0
                Flex
            End Enum

            Public Class FieldConstants
                Public Class AddressLabel
                    Public Class Region
                        Public Shared ReadOnly Letter As New SizeF(55, 217)
                        Public Shared ReadOnly PostCard As New SizeF(145, 217)
                    End Class
                    Public Shared ReadOnly Regions As IDictionary(Of LetterType, SizeF) = New Dictionary(Of LetterType, SizeF)() From { _
                        {LetterType.PlainLetter, Region.Letter}, _
                        {LetterType.CertifiedLetter, Region.Letter}, _
                        {LetterType.Postcard, Region.PostCard} _
                    }
                    Public Class Field
                        Public Class Site
                            Public Const Name As String = "site_name"
                            Public Const Addr1 As String = "site_addr1"
                            Public Const Addr2 As String = "site_addr2"
                            Public Const City As String = "site_city"
                            Public Const State As String = "site_state"
                            Public Const PostalCode As String = "site_zip"
                            Public Const Phone As String = "site_phone"
                        End Class
                        Public Class MailTo
                            Public Const Name As String = "mail_to_name"
                            Public Const Addr1 As String = "mail_to_addr1"
                            Public Const Addr2 As String = "mail_to_addr2"
                            Public Const Addr3 As String = "mail_to_addr3"
                            Public Const City As String = "mail_to_city"
                            Public Const State As String = "mail_to_state"
                            Public Const PostalCode As String = "mail_to_zip"
                        End Class
                    End Class
                    Public Shared ReadOnly Fields As IDictionary(Of String, Boolean) = New Dictionary(Of String, Boolean)() From { _
                        {Field.Site.Name, True}, _
                        {Field.Site.Addr1, True}, _
                        {Field.Site.Addr2, True}, _
                        {Field.Site.City, True}, _
                        {Field.Site.State, True}, _
                        {Field.Site.PostalCode, True}, _
                        {Field.Site.Phone, True}, _
                        {Field.MailTo.Name, True}, _
                        {Field.MailTo.Addr1, True}, _
                        {Field.MailTo.Addr2, True}, _
                        {Field.MailTo.Addr3, True}, _
                        {Field.MailTo.City, True}, _
                        {Field.MailTo.State, True}, _
                        {Field.MailTo.PostalCode, True} _
                    }
                End Class
            End Class

            Private replaced As Boolean
            Private ftext As String

            Public Name As String
            Public Tags As Tuple(Of String, String)
            Public Type As FieldType
            Public IsLabel As Boolean

            Public ReadOnly Property IsReplaced As Boolean
                Get
                    If replaced Then
                        If IsLabel Then Return 0 <> StringComparer.InvariantCulture.Compare(FieldText, Text)
                        Return True
                    End If
                    Return False
                End Get
            End Property

            Private ReadOnly Property FieldText As String
                Get
                    If String.IsNullOrEmpty(ftext) Then ftext = Tags.Item1 & Name & Tags.Item2
                    Return ftext
                End Get
            End Property

            Public Sub New()
                replaced = False
                ftext = String.Empty
            End Sub

            Public Sub Replace(s As String, info As FieldReplacement.PageInfo, position As PointF, replaceLabel As Boolean)
                Text = s
                Unescape()
                replaced = True

                ' Pages are numbered starting from 1.
                If 1 = info.PageNumber And Not replaceLabel Then
                    If IsLabel Then CheckLabelRegion(info.LetterType, info.PageProperties, position)
                End If
            End Sub

            Private Sub CheckLabelRegion(letterType As LetterType, props As Page.Props, position As PointF)
                Dim noSubRegion As SizeF
                Dim showTag As Boolean

                If FieldConstants.AddressLabel.Regions.TryGetValue(letterType, noSubRegion) Then
                    Dim y As Single = props.PageSize.Height - noSubRegion.Height
                    Dim nosub As New RectangleF(0.0F, y, noSubRegion.Width, noSubRegion.Height)

                    If nosub.Contains(position) Then
                        If FieldConstants.AddressLabel.Fields.TryGetValue(Name, showTag) Then
                            If showTag Then Text = FieldText
                        End If
                    End If
                End If
            End Sub
        End Class

        Public Class FormattingSegment
            Implements Segment

            Public Class Constants
                Public Class Token
                    Public Const Leading = "TL"
                    Public Const Font As String = "Tf"
                    Public Const Matrix As String = "Tm"
                    Public Const Draw As String = "Td"
                    Public Const DrawLeading As String = "TD"
                    Public Const WordSpacing As String = "Tw"
                End Class
            End Class

            Public Text As String

            Public Overridable Sub Render(sb As StringBuilder) Implements Segment.Render
                If Not String.IsNullOrEmpty(Text) Then sb.Append(Text).Append(vbLf)
            End Sub
        End Class

        Public Enum PositionOperator
            Unknown = 0
            LineOffset = 1
            Matrix = 2
        End Enum

        Public Class PositionSegment
            Inherits FormattingSegment

            Private posOp As PositionOperator
            Public ReadOnly Property [Operator] As PositionOperator
                Get
                    Return posOp
                End Get
            End Property
            Public WriteOnly Property OpString As String
                Set(value As String)
                    If 0 = StringComparer.InvariantCulture.Compare(FormattingSegment.Constants.Token.Matrix, value) Then
                        posOp = PositionOperator.Matrix
                    ElseIf 0 = StringComparer.InvariantCulture.Compare(FormattingSegment.Constants.Token.Draw, value) Then
                        posOp = PositionOperator.LineOffset
                    End If
                End Set
            End Property

            Public ScaleFactor As Single
            Public Alignment As TextAlignment
            Public Translation As PointF

            Public Overrides Sub Render(sb As StringBuilder)
                Select Case [Operator]
                    Case PositionOperator.LineOffset
                        If Translation.IsEmpty Then Exit Sub
                        DrawUtility.Origin.Move(New SizeF(Translation.X, 0))
                    Case PositionOperator.Matrix
                        DrawUtility.Origin.Replace(Translation)
                End Select
                MyBase.Render(sb)
            End Sub
        End Class

        Public Class LeadingSegment
            Inherits FormattingSegment

            Public Leading As Single
        End Class

        Public Class FontSegment
            Inherits FormattingSegment

            Public FontName As String
            Public FontSize As Single
            Public OriginalFontSize As Single
        End Class

        Public Class WordSpacingSegment
            Inherits FormattingSegment

            Public WordSpacing As Single
        End Class

        Public MustInherit Class MarkerSegment
            Implements Segment

            Private token As String
            Private appendLineFeed As Boolean

            Public Sub New(token As String, lineFeedTerminate As Boolean)
                Me.token = token
                Me.appendLineFeed = lineFeedTerminate
            End Sub

            Public Sub Render(sb As System.Text.StringBuilder) Implements Segment.Render
                sb.Append(token)
                If appendLineFeed Then sb.Append(vbLf)
            End Sub
        End Class

        Public Class NewLineMarker
            Inherits MarkerSegment

            Public Class Constants
                Public Const Token As String = "T*"
            End Class

            Public Sub New()
                MyBase.new(Constants.Token, False)
            End Sub
        End Class

        Public Class EndTextMarker
            Inherits MarkerSegment

            Public Class Constants
                Public Const Token As String = "ET"
            End Class

            Public Sub New()
                MyBase.new(Constants.Token, True)
            End Sub
        End Class

        Public Class BeginStringMarker
            Inherits MarkerSegment

            Public Class Constants
                Public Const Token As String = "("
            End Class

            Public Sub New()
                MyBase.new(Constants.Token, False)
            End Sub
        End Class

        Public Class EndStringMarker
            Inherits MarkerSegment

            Public Class Constants
                Public Const Token As String = ")Tj"
            End Class

            Public Sub New()
                MyBase.new(Constants.Token, True)
            End Sub
        End Class

        Public Class AlignmentSegment
            Implements Segment

            Public Alignment As TextAlignment

            Public Sub Render(sb As System.Text.StringBuilder) Implements Segment.Render
            End Sub
        End Class

        Public Class ScalingSegment
            Implements Segment

            Public ScaleFactor As Single
            Public ScaleWeight As Single

            Public Sub Render(sb As System.Text.StringBuilder) Implements Segment.Render
            End Sub
        End Class

    End Namespace
End Namespace
