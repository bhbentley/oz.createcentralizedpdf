﻿Imports System.Drawing

Namespace Letters
    Namespace Pdf

        Public Class Origin
            Private origin As PointF

            Public Sub New(location As PointF)
                Me.origin = location
                Reset()
            End Sub

            Public Sub New(o As Origin)
                Me.origin = o.origin
            End Sub

            Public Property Reflowed As Boolean

            Public ReadOnly Property Location As PointF
                Get
                    Return origin
                End Get
            End Property

            Public Sub Move(offset As SizeF)
                origin = PointF.Add(origin, offset)
            End Sub

            Public Sub Replace(pt As PointF)
                origin = pt
            End Sub

            Public Sub Reset()
                Reflowed = False
            End Sub
        End Class
    End Namespace
End Namespace
