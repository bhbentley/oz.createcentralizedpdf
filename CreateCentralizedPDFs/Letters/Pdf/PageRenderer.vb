﻿Imports System.Linq
Imports System.Reflection

Namespace Letters
    Namespace Pdf

        Public Class PageRenderer

            Public Class Constants
                Public Const RepeatingFieldNameFormat As String = "{0}_{1}"
            End Class

            Public Class Info
                Public Type As LetterType
                Public AvailablePages As Integer
                Public FreePages As Integer
                Public Pages As IList(Of Page)
                Public RegularFields As IDictionary(Of String, String)
                Public RepeatingFields As IDictionary(Of String, IList(Of String))
                Public RepeatingFieldCounts As IDictionary(Of String, Integer)
            End Class

            Private Shared whiteOut As WhiteOutChunk = Nothing
            Public Shared Property WhiteOutType As String
                Get
                    If whiteOut Is Nothing Then Return String.Empty
                    Return whiteOut.GetType().FullName
                End Get
                Set(value As String)
                    If String.IsNullOrEmpty(value) Then
                        whiteOut = Nothing
                        Return
                    End If

                    whiteOut = LoadType(value)
                End Set
            End Property

            Private Shared tempWhiteOut As WhiteOutChunk = Nothing
            Private Shared Function LoadType(typeName As String) As WhiteOutChunk
                If tempWhiteOut IsNot Nothing Then
                    Return tempWhiteOut
                End If

                Dim o As Object = Utility.LoadInstance(typeName)

                If o Is Nothing Then
                    Return Nothing
                End If

                tempWhiteOut = CType(o, WhiteOutChunk)
                Return tempWhiteOut
            End Function

            Private type As LetterType
            Private availablePages As Integer
            Private freePages As Integer
            Private blanks As IList(Of Page)
            Private regularFields As IDictionary(Of String, String)
            Private repeatCounts As IDictionary(Of String, Integer)
            Private repeaters As IDictionary(Of String, IDictionary(Of Integer, IList(Of Tuple(Of String, String))))
            Private processed As IList(Of Page)
            Private pageNumber As Integer
            Private flexPage As Integer
            Private totalFlexPages As Integer
            Private continueFlex As Boolean

            Public ReadOnly Property Pages As IList(Of Page)
                Get
                    Return processed
                End Get
            End Property

            Public Sub New(info As Info)
                Me.type = info.Type
                Me.availablePages = info.AvailablePages
                Me.freePages = info.FreePages
                Me.blanks = info.Pages
                Me.regularFields = info.RegularFields
                Me.repeatCounts = info.RepeatingFieldCounts
                Me.repeaters = ExpandRepeatingFields(info.RepeatingFields)
                Me.processed = New List(Of Page)()
                Me.pageNumber = 1
                Me.flexPage = 0
                Me.totalFlexPages = CountFlexPages()
                Me.continueFlex = False
            End Sub

            Private Function ExpandRepeatingFields(data As IDictionary(Of String, IList(Of String))) As IDictionary(Of String, IDictionary(Of Integer, IList(Of Tuple(Of String, String))))
                Dim fv As IDictionary(Of String, IDictionary(Of Integer, IList(Of Tuple(Of String, String)))) = New Dictionary(Of String, IDictionary(Of Integer, IList(Of Tuple(Of String, String))))()

                For Each kvp In data
                    fv.Add(kvp.Key, ExpandRepeatingField(kvp.Key, kvp.Value))
                Next

                Return fv
            End Function

            Private Function ExpandRepeatingField(fn As String, values As IList(Of String)) As IDictionary(Of Integer, IList(Of Tuple(Of String, String)))
                Dim fpv As IDictionary(Of Integer, IList(Of Tuple(Of String, String))) = New Dictionary(Of Integer, IList(Of Tuple(Of String, String)))()
                Dim k As Integer = repeatCounts(fn)
                Dim i As Integer = 0
                Dim page As Integer = 1

                ' There may be more rows of data than flex field lines on the page.  When this happens, a new page is created
                ' for the remaining rows of data.  As the new page is a copy of the old page (without the field replacments,
                ' obviously), the repeating flex fields will start from 1.

                Do While values.Count > i
                    fpv.Add(page, FillValuesForPage(fn, i, k, values))
                    page += 1
                    i += k
                Loop

                Return fpv
            End Function

            Private Function FillValuesForPage(fn As String, first As Integer, count As Integer, values As IList(Of String)) As IList(Of Tuple(Of String, String))
                Dim fv As IList(Of Tuple(Of String, String)) = New List(Of Tuple(Of String, String))(count)
                Dim i As Integer = 0
                Dim k As Integer = Math.Min(count, values.Count - first)

                Do Until i = k
                    fv.Add(New Tuple(Of String, String)(String.Format(Constants.RepeatingFieldNameFormat, fn, i + 1), values(first + i)))
                    i += 1
                Loop
                BlankUnfilledFields(fn, count, fv)

                Return fv
            End Function

            Private Sub BlankUnfilledFields(fn As String, count As Integer, fv As IList(Of Tuple(Of String, String)))
                For i As Integer = fv.Count + 1 To count
                    fv.Add(New Tuple(Of String, String)(String.Format(Constants.RepeatingFieldNameFormat, fn, i), String.Empty))
                Next
            End Sub

            Private Function CountFlexPages() As Integer
                Dim k As Integer = 0
                Dim rv = repeaters.Where(Function(r) r.Value.Any()).ToList()

                If rv.Any() Then k = rv.Max(Function(r) r.Value.Max(Function(fpv) fpv.Key))

                Return k
            End Function

            Public Sub Render(replaceLabel As Boolean)
                Dim objNum As Tuple(Of Integer, Integer)

                If processed.Any() Then Return
                If Not blanks.Any() Then Return

                objNum = GetNextObjectNumbers(blanks.Last())
                For Each p As Page In blanks
                    If pageNumber > availablePages Then Exit For
                    p.Number = pageNumber
                    ReplaceFields(p, replaceLabel)
                    RenderPage(p)
                    Do While continueFlex And 0 <> freePages
                        p = p.Replicate(objNum, pageNumber)
                        ReplaceFields(p, replaceLabel)
                        RenderPage(p)
                        objNum = GetNextObjectNumbers(p)
                        freePages -= 1
                    Loop
                Next
            End Sub

            Private Function GetNextObjectNumbers(p As Page) As Tuple(Of Integer, Integer)
                Dim objNum As Tuple(Of Integer, Integer) = p.GetObjectNumbers()
                Return New Tuple(Of Integer, Integer)(objNum.Item1 + 2, objNum.Item2 + 2)
            End Function

            Private Sub ReplaceFields(p As Page, replaceLabel As Boolean)
                Dim fv As IList(Of IDictionary(Of String, String)) = New List(Of IDictionary(Of String, String))() From {regularFields}

                fv = fv.Concat(GatherRepeatingFieldValues(p)).ToList()
                p.ReplaceFields(type, fv, replaceLabel)
            End Sub

            Private Function GatherRepeatingFieldValues(p As Page) As IList(Of IDictionary(Of String, String))
                Dim fv As IList(Of IDictionary(Of String, String)) = New List(Of IDictionary(Of String, String))()
                Dim fn As IList(Of String) = p.MatchFields(repeaters.Select(Function(x) x.Key).ToList())

                If Not fn.Any() Then Return fv

                flexPage += 1
                For Each n As String In fn
                    fv.Add(SelectRepeatingValuesForPage(n, repeaters(n)))
                Next
                continueFlex = totalFlexPages > flexPage

                Return fv
            End Function

            Private Function SelectRepeatingValuesForPage(fn As String, repeatValues As IDictionary(Of Integer, IList(Of Tuple(Of String, String)))) As IDictionary(Of String, String)
                Dim k As Integer = repeatCounts(fn)
                Dim fv As IDictionary(Of String, String) = New Dictionary(Of String, String)(k)

                For Each v In repeatValues(flexPage)
                    fv.Add(v.Item1, v.Item2)
                Next

                Return fv
            End Function

            Private Sub RenderPage(p As Page)
                If Not whiteOut Is Nothing Then
                    Dim wo As WhiteOutChunk = CType(whiteOut.Replicate(), WhiteOutChunk)
                    wo.LetterType = type
                    wo.PageNumber = p.Number
                    p.AddWhiteOut(wo)
                End If
                p.Render(type)
                processed.Add(p)
                pageNumber += 1
            End Sub
        End Class

    End Namespace
End Namespace
