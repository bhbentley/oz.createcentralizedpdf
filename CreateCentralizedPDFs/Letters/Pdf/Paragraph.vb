﻿Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Text.RegularExpressions

Namespace Letters
    Namespace Pdf

#Region "Support Types"

        Public Enum TextAlignment
            Unspecified = 0
            Left = 1
            Center = 2
            Right = 3
        End Enum

        Public Class FontSpec
            Private fn As String
            Private fs As Single
            Private scaled As Boolean

            Public Sub New()
                Me.scaled = False
                SetProperties(String.Empty, 0.0F)
            End Sub

            Public Sub New(fontName As String, fontSize As Single)
                SetProperties(fontName, fontSize)
            End Sub

            Public ReadOnly Property FontName As String
                Get
                    Return fn
                End Get
            End Property

            Public ReadOnly Property FontSize As Single
                Get
                    Return fs
                End Get
            End Property

            Public ReadOnly Property IsEmpty As Boolean
                Get
                    Return String.IsNullOrEmpty(fn) Or 0.0F = fs
                End Get
            End Property

            Public Sub SetProperties(fontName As String, fontSize As Single)
                If String.IsNullOrEmpty(fn) Then fn = fontName
                If 0.0F = fs Then fs = fontSize
            End Sub

            Public Sub Scale(scale As Single)
                If 0.0F >= scale Then Return
                If Not scaled Then fs *= scale
                If 1.0F <> scale Then scaled = True
            End Sub
        End Class

#End Region

        Public Class Paragraph
            Private continued As Boolean
            Private previousLineHeight As Single

            Private lines As IList(Of Line)

            Private alignment As TextAlignment
            Private doReflow As Boolean
            Private forcedReflow As Boolean
            Private reflowSegments As IList(Of Segment)

            Private seenSegments As Boolean
            Private currentLine As Line

            Public ReadOnly Property FinalLineHeight As Single
                Get
                    Return lines.Last().LineHeight
                End Get
            End Property

            Public ReadOnly Property FinalFont As FontSpec
                Get
                    Return lines.Last().LineFont
                End Get
            End Property

            Public ReadOnly Property IsEmpty As Boolean
                Get
                    Return Not seenSegments
                End Get
            End Property

            Public ReadOnly Property HasTextSegments As Boolean
                Get
                    Return lines.Any(Function(x) Not x.IsEmpty)
                End Get
            End Property

            Public ReadOnly Property HasText As Boolean
                Get
                    Return lines.Any(Function(x) x.HasText)
                End Get
            End Property

            Public ReadOnly Property HasEndText As Boolean
                Get
                    Return lines.Any(Function(x) x.HasEndText)
                End Get
            End Property

            Public ReadOnly Property IsTextContinued As Boolean
                Get
                    Return continued
                End Get
            End Property

            Public Sub New()
                Init()
                Me.currentLine = AddLine(New FontSpec(), lines)
            End Sub

            Private Sub Init()
                Me.continued = False
                Me.previousLineHeight = 0.0F
                Me.lines = New List(Of Line)()
                Me.alignment = TextAlignment.Left
                Me.doReflow = False
                Me.forcedReflow = False
                Me.reflowSegments = Nothing
                Me.seenSegments = False
            End Sub

            Public Sub New(fontSpec As FontSpec, lineHeight As Single)
                Init()
                Me.previousLineHeight = lineHeight
                Me.currentLine = AddLine(fontSpec, lines)
            End Sub

            Public Sub ForceReflow()
                forcedReflow = True
            End Sub

            Public Sub Unescape()
                For Each line As Line In lines
                    line.Unescape()
                Next
            End Sub

            Public Sub AddSegment(segment As Segment)
                ' VB.Net does not have multimethods hence the type switch.
                If TypeOf segment Is FieldSegment Then
                    HandleFieldSegment(DirectCast(segment, TextSegment))
                ElseIf TypeOf segment Is TextSegment Then
                    HandleTextSegment(DirectCast(segment, TextSegment))
                ElseIf TypeOf segment Is NewLineMarker Then
                    HandleLineBreakMarker(DirectCast(segment, NewLineMarker))
                ElseIf TypeOf segment Is PositionSegment Then
                    HandlePositionSegment(DirectCast(segment, PositionSegment))
                Else
                    currentLine.AddSegment(segment)
                End If
                seenSegments = True
            End Sub

            Private Sub HandleFieldSegment(segment As TextSegment)
                doReflow = True
                HandleTextSegment(segment)
            End Sub

            Private Sub HandleTextSegment(segment As TextSegment)
                currentLine.AddSegment(segment)
                continued = False
            End Sub

            Private Sub HandleLineBreakMarker(segment As NewLineMarker)
                If Not currentLine.IsEmpty Then AddLine()
                currentLine.AddSegment(segment)
                continued = True
            End Sub

            Private Sub HandlePositionSegment(segment As PositionSegment)
                alignment = segment.Alignment
                currentLine.AddSegment(segment)
            End Sub

            Private Overloads Sub AddLine()
                currentLine = AddLine(currentLine, lines)
            End Sub

            Private Overloads Function AddLine(font As FontSpec, lineCol As IList(Of Line)) As Line
                Dim line As New Line(font)
                Return AddLine(line, lineCol)
            End Function

            Private Overloads Function AddLine(line As Line, lineCol As IList(Of Line)) As Line
                lineCol.Add(New Line(line.LineFont))
                Return lineCol.Last()
            End Function

            Public Function ReplaceFields(replacement As FieldReplacement, replaceLabel As Boolean) As Boolean
                If Not doReflow Then Return False
                For Each line As Line In lines
                    line.ReplaceFields(replacement, replaceLabel)
                Next
                Return True
            End Function

            Public Sub Render(fonts As IDictionary(Of String, FontObject), leftEdge As Single, widths As Tuple(Of Single, Single), sb As StringBuilder)
                If doReflow Or forcedReflow Then
                    Reflow(fonts, leftEdge, widths)
                    If reflowSegments Is Nothing Then Return
                    For Each seg As Segment In reflowSegments
                        seg.Render(sb)
                    Next
                Else
                    For Each line As Line In lines
                        line.Render(sb)
                    Next
                End If
                DrawUtility.Origin.Reflowed = doReflow
            End Sub

            Private Sub Reflow(fonts As IDictionary(Of String, FontObject), leftEdge As Single, widths As Tuple(Of Single, Single))
                Dim reflow As New WordWrapReflow(previousLineHeight)
                Dim first As Boolean = True

                For Each line As Line In lines
                    line.CollectSegments(reflow.Collector, first)
                    first = False
                Next
                reflow.Reflow(fonts, leftEdge, widths)
                reflowSegments = reflow.Segments
            End Sub
        End Class

    End Namespace
End Namespace
