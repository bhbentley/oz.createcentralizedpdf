﻿Imports System.Drawing
Imports System.Linq
Imports System.Text

Namespace Letters
    Namespace Pdf

        Public Class Line
            Private alignment As TextAlignment
            Private scaleFactor As Single
            Private scaleWeight As Single
            Private spans As IList(Of Span)

            Private currentSpan As Span

            Public ReadOnly Property LineHeight As Single
                Get
                    If Not spans.Any() Then Return 0.0F
                    Return spans.Last().Leading
                End Get
            End Property

            Public ReadOnly Property LineFont As FontSpec
                Get
                    Return spans.Last().FontSpec
                End Get
            End Property

            Public ReadOnly Property IsEmpty As Boolean
                Get
                    Return spans.All(Function(x) x.IsEmpty)
                End Get
            End Property

            Public ReadOnly Property HasText As Boolean
                Get
                    Return spans.Any(Function(x) x.HasText)
                End Get
            End Property

            Public ReadOnly Property HasEndText As Boolean
                Get
                    Return spans.Any(Function(x) x.HasEndText)
                End Get
            End Property

            Public Sub New(fontSpec As FontSpec)
                Me.alignment = TextAlignment.Left
                Me.scaleFactor = 1.0F
                Me.scaleWeight = 1.0F
                Me.spans = New List(Of Span)()
                Me.currentSpan = Nothing
                AddSpan(fontSpec)
            End Sub

            Private Overloads Sub AddSpan()
                AddSpan(currentSpan.FontSpec)
            End Sub

            Private Overloads Sub AddSpan(fontSpec As FontSpec)
                Dim leading As Single = 0.0F

                If Not currentSpan Is Nothing Then leading = currentSpan.Leading
                AddSpan(New Span(fontSpec, scaleFactor, leading))
            End Sub

            Private Overloads Sub AddSpan(span As Span)
                AddHandler span.AlignmentChanged, AddressOf HandleAlignmentChanged
                AddHandler span.ScaleChanged, AddressOf HandleScaleChanged
                AddHandler span.WordSpacingChanged, AddressOf HandleWordSpacingChanged
                spans.Add(span)
                currentSpan = span
            End Sub

            Private Sub HandleAlignmentChanged(span As Span, alignment As TextAlignment)
                Me.alignment = alignment
            End Sub

            Private Sub HandleScaleChanged(span As Span, scaleFactor As Single)
                If 0.0F = scaleFactor Then Return
                Me.scaleFactor = scaleFactor
            End Sub

            Private Sub HandleWordSpacingChanged(wordSpacing As Single)
                If 0.0F < wordSpacing And 1.0F > wordSpacing Then Me.scaleWeight = 1.0F + wordSpacing
            End Sub

            Public Sub AddSegment(seg As Segment)
                If TypeOf seg Is FormattingSegment Then
                    If Not currentSpan.IsEmpty Then AddSpan()
                End If
                currentSpan.AddSegment(seg)
            End Sub

            Public Sub ReplaceFields(replacement As FieldReplacement, replaceLabel As Boolean)
                For Each span As Span In spans
                    span.ReplaceFields(replacement, replaceLabel)
                Next
            End Sub

            Public Sub CollectSegments(collector As IList(Of Segment), first As Boolean)
                InsertNewLineMarker(collector)
                collector.Add(New AlignmentSegment() With {.Alignment = Me.alignment})
                If 1.0F <> scaleFactor Or 1.0F <> scaleWeight Then collector.Add(New ScalingSegment() With {.ScaleFactor = Me.scaleFactor, .ScaleWeight = Me.scaleWeight})
                For Each span As Span In spans
                    span.CollectSegments(collector, first)
                    first = False
                Next
            End Sub

            Private Sub InsertNewLineMarker(collector As IList(Of Segment))
                If Not spans.Any() Then Return
                If spans.First().IsNewLine Then collector.Add(New NewLineMarker())
            End Sub

            Public Sub Render(sb As StringBuilder)
                For Each span As Span In spans
                    span.Render(sb)
                Next
            End Sub

            Public Sub Unescape()
                For Each span As Span In spans
                    span.Unescape()
                Next
            End Sub
        End Class

    End Namespace
End Namespace
