﻿Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Text.RegularExpressions

Namespace Letters
    Namespace Pdf

        Public Class NamedObject
            Public Name As String
            Public ObjectNumber As Integer
        End Class

        Public Class Resources
            Public ExternalObjects As IList(Of NamedObject)
            Public Fonts As IList(Of NamedObject)
            Public ProcessingSet As IList(Of String)
        End Class

        Public Class PageObject
            Public ObjectNumber As Integer
            Public MediaBox As BoundingBox
            Public Resources As Resources
            Public Content As Integer
        End Class

        Public Class ImageStream
            Public ObjectNumber As Integer
            Public Height As Single
            Public Width As Single
            Public Content As Byte()
        End Class

        Public Class PageStream

            Public Class Constants
                Public Const RepeatingNumberPattern As String = "_\d+$"
                Public Shared ReadOnly RepeatingNumberRegexp As New Regex(RepeatingNumberPattern, RegexOptions.Compiled)
            End Class

            Private chunkList As IList(Of ContentChunk)
            Private fn As ISet(Of String)

            Public Property FieldNames As ISet(Of String)
                Get
                    Return fn
                End Get
                Set(value As ISet(Of String))
                    fn = New HashSet(Of String)(value.Select(Function(x) Constants.RepeatingNumberRegexp.Replace(x, String.Empty)))
                End Set
            End Property

            Public Sub New()
                Me.chunkList = New List(Of ContentChunk)
                Me.fn = New HashSet(Of String)()
            End Sub

            Public ObjectNumber As Integer

            Public Function CopyTo(ByRef b As Byte()) As Integer
                Dim len = chunkList.Sum(Function(x) x.Length)
                Dim i As Integer = 0

                If b.Length < len Then b = New Byte(len - 1) {}
                For Each chunk In chunkList
                    i += chunk.CopyTo(b, i)
                Next

                Return len
            End Function

            Public Sub AddChunk(chunk As ContentChunk)
                chunkList.Add(chunk)
            End Sub

            Public Sub AddWhiteOut(whiteOut As WhiteOutChunk)
                Dim n As Integer = chunkList.Count
                If 0 = n Then Return
                If TypeOf (chunkList.Last()) Is WhiteOutChunk Then chunkList.RemoveAt(n - 1)
                chunkList.Add(whiteOut)
            End Sub

            Public Sub ReplaceFields(replacement As FieldReplacement, replaceLabel As Boolean)
                For Each chunk As ContentChunk In chunkList
                    chunk.ReplaceFields(replacement, replaceLabel)
                Next
            End Sub

            Public Sub Render(fonts As IDictionary(Of String, FontObject))
                For Each chunk As ContentChunk In chunkList
                    chunk.Render(fonts)
                Next
            End Sub

            Public Function Replicate(objectNumber As Integer) As PageStream
                Dim o As New PageStream() With { _
                    .chunkList = New List(Of ContentChunk)(Me.chunkList.Count), _
                    .fn = Me.fn, _
                    .ObjectNumber = objectNumber _
                }

                For Each chunk As ContentChunk In chunkList
                    If TypeOf (chunk) Is WhiteOutChunk Then Continue For
                    o.chunkList.Add(chunk.Replicate())
                Next

                Return o
            End Function

            Public Sub SetContentBounds(bounds As RectangleF, wrapWidth As Single)
                For Each chunk As ContentChunk In chunkList
                    chunk.SetHorizontalBounds(bounds.Left, bounds.Width, wrapWidth)
                Next
            End Sub
        End Class

        Public Class FontObject
            Public ObjectNumber As Integer
            Public BaseFont As String
            Public Style As FontStyle
        End Class

    End Namespace
End Namespace
