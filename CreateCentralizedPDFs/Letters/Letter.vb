﻿Imports System.IO

Namespace Letters

    Public Enum LetterType
        PlainLetter = 0
        Postcard
        CertifiedLetter
    End Enum

    Public Interface Letter
        Sub Render(data As FieldData, replaceLabel As Boolean)
        Sub Write(stream As Stream)
    End Interface

End Namespace