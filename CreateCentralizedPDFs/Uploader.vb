﻿Imports System.Diagnostics
Imports System.IO
Imports System.Linq

Public Class Uploader
    Implements Letters.Runnable(Of Integer)

    Public Class Constants
        Public Const MasterFileNameFormat As String = "masterZipFiles{0:yyyy_MM_dd}_{1}.txt"
        Public Const MillisecondsPerSecond As Double = 1000.0R
    End Class

    Public Class Settings
        Public Server As String
        Public Username As String
        Public Password As String
        Public Path As String
        Public FileIdentifier As String
        Public WorkingFolder As String
        Public Files As IList(Of String)
    End Class

    Private info As Settings

    Public Event Notice(sender As Object, e As NotifyEventArgs) Implements Letters.Runnable(Of Integer).Notice

    Public Sub New(settings As Settings)
        Me.info = settings
    End Sub

    Public Sub Upload(dummy As Integer) Implements Letters.Runnable(Of Integer).Run
        Dim nClient As New Renci.SshNet.SftpClient(info.Server, info.Username, info.Password)
        Dim timer As New Stopwatch()

        Environment.CurrentDirectory = info.WorkingFolder
        BuildFileList()
        NotifyUploadStart()

        Try
            nClient.Connect()
            If Not String.IsNullOrEmpty(info.Path) Then nClient.ChangeDirectory(info.Path)
            For Each fn As String In info.Files
                StartTransfer(fn)
                Using stm As New FileStream(fn, FileMode.Open, FileAccess.Read)
                    timer.Restart()
                    nClient.UploadFile(stm, Path.GetFileName(fn), False)
                    timer.Stop()
                End Using
                EndTransfer(fn, String.Format("transferred in {0:0.000} seconds", timer.ElapsedMilliseconds / Constants.MillisecondsPerSecond))
            Next
        Finally
            nClient.Disconnect()
        End Try

        NotifyUploadDone(info.Files.Count)
    End Sub

    Private Sub BuildFileList()
        Dim tmp As IList(Of String)

        If info.Files.Any() Then Return

        tmp = New List(Of String)(My.Computer.FileSystem.GetFiles(info.WorkingFolder, FileIO.SearchOption.SearchTopLevelOnly, "*.zip"))
        MakeMasterFile(tmp)
        info.Files = tmp
    End Sub

    Private Sub MakeMasterFile(files As IList(Of String))
        Dim mfn As String = MasterFileName

        Using stm As New StreamWriter(mfn)
            stm.WriteLine(String.Join(Environment.NewLine, files.Select(Function(x) Path.GetFileName(x))))
        End Using
        files.Insert(0, mfn)
    End Sub

    Private ReadOnly Property MasterFileName As String
        Get
            Return Path.Combine(info.WorkingFolder, String.Format(Constants.MasterFileNameFormat, DateTime.Today, info.FileIdentifier))
        End Get
    End Property

    Private Sub NotifyUploadStart()
        Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
            {EventData.UploadServer, info.Server}, _
            {EventData.UploadUser, info.Username}, _
            {EventData.UploadPath, info.Path} _
        }
        Notify(New NotifyEventArgs(EventID.UploadStart, data))
    End Sub

    Private Sub Notify(notify As NotifyEventArgs)
        RaiseEvent Notice(Me, notify)
    End Sub

    Private Sub StartTransfer(fileName As String)
        Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
            {EventData.UploadFile, fileName} _
        }
        Notify(New NotifyEventArgs(EventID.UploadFileStart, data))
    End Sub

    Private Sub EndTransfer(fileName As String, message As String)
        Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
            {EventData.UploadFile, fileName}, _
            {EventData.UploadMessage, message} _
        }
        Notify(New NotifyEventArgs(EventID.UploadFileEnd, data))
    End Sub

    Private Sub NotifyUploadDone(count As Integer)
        Dim data As IDictionary(Of String, Object) = New Dictionary(Of String, Object)() From { _
            {EventData.Count, count} _
        }
        Notify(New NotifyEventArgs(EventID.UploadFinish, data))
    End Sub

End Class