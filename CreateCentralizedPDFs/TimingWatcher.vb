﻿Imports System.Linq

Public Class TimingWatcher
    Implements Watcher

    Public Class Constants
        Public Const TraceSource As String = "TimingSource"
    End Class

    Private totalRunTime As Stopwatch
    Private timer As Stopwatch
    Private seen As IDictionary(Of Letters.TemplateKey, Integer)
    Private times As IDictionary(Of Letters.TemplateKey, IList(Of Long))
    Private lastKey As Letters.TemplateKey
    Private tracer As TraceSource

    Public Sub New()
        totalRunTime = New Stopwatch()
        timer = New Stopwatch()
        tracer = New TraceSource(Constants.TraceSource)
    End Sub

    Public Sub HandleNotice(sender As Object, e As NotifyEventArgs) Implements Watcher.HandleNotice
        If TypeOf sender Is Letters.LetterBatcher Then
            HandleBatchNotice(e)
        ElseIf TypeOf sender Is Letters.LetterBundler Then
            HandleBundleNotice(e)
        End If
    End Sub

    Private Sub HandleBatchNotice(e As NotifyEventArgs)
        Select Case e.EventID
            Case Letters.EventID.BatchStarted
                seen = New Dictionary(Of Letters.TemplateKey, Integer)()
                times = New Dictionary(Of Letters.TemplateKey, IList(Of Long))()
                lastKey = Nothing
                totalRunTime.Restart()
                timer.Restart()
            Case Letters.EventID.LetterDataLoaded
                timer.Stop()
                tracer.TraceInformation("Letters loaded: {0}", e.Data(Letters.EventData.Count))
                tracer.TraceInformation("Letters loaded in {0:#,###,##0}ms", timer.ElapsedMilliseconds)
                timer.Restart()
            Case Letters.EventID.TemplatesLoaded
                timer.Stop()
                tracer.TraceInformation("Templates loaded in {0:#,###,##0}ms", timer.ElapsedMilliseconds)
                timer.Restart()
            Case Letters.EventID.LetterCreated
                HandleSingleLetter(e)
            Case Letters.EventID.LettersDone
                timer.Stop()
                If Not lastKey Is Nothing Then
                    tracer.TraceInformation("Template letters: {0}", seen(lastKey))
                    tracer.TraceInformation("Letter time: {0:#,###,##0}ms", times(lastKey).Sum())
                End If
                tracer.TraceInformation("Letters processed: {0}", e.Data(Letters.EventData.Count))
                tracer.TraceInformation("Letters generated in {0:#,###,##0}ms", times.SelectMany(Function(x) x.Value).Sum())
                timer.Restart()
            Case Letters.EventID.BatchFinished
                timer.Stop()
                tracer.TraceInformation("Letter records updated in {0:#,###,##0}ms", timer.ElapsedMilliseconds)
                totalRunTime.Stop()
                tracer.TraceInformation("Total run time: {0:#,###,##0}ms", totalRunTime.ElapsedMilliseconds)
        End Select
    End Sub

    Private Sub HandleSingleLetter(e As NotifyEventArgs)
        Dim key As Letters.TemplateKey

        timer.Stop()
        key = CType(e.Data(Letters.EventData.TemplateKey), Letters.TemplateKey)
        If Not seen.ContainsKey(key) Then
            If Not lastKey Is Nothing Then
                tracer.TraceInformation("Template letters: {0}", seen(lastKey))
                tracer.TraceInformation("Letter time: {0:#,###,##0}ms", times(lastKey).Sum())
            End If
            lastKey = key
            seen.Add(key, 0)
            times.Add(key, New List(Of Long)())
            tracer.TraceInformation("Template {0}:{1}", key.Identifier, key.Version)
        End If
        seen(key) += 1
        times(key).Add(timer.ElapsedMilliseconds)
        timer.Restart()
    End Sub

    Private Sub HandleBundleNotice(e As NotifyEventArgs)
        Select Case e.EventID
            Case Letters.EventID.BundlingStarted
                totalRunTime.Restart()
                timer.Restart()
            Case Letters.EventID.BundlingFinished
                timer.Stop()
                totalRunTime.Stop()
                tracer.TraceInformation("All bundled: {0:#,###,##0}ms", totalRunTime.ElapsedMilliseconds)
            Case Letters.EventID.BundleCreated
                timer.Stop()
                tracer.TraceInformation("Bundle {0}: {1} file{2}; {3:#,###,##0}ms", e.Data(Letters.EventData.FileName), e.Data(Letters.EventData.Count), _
                                  IIf(1 = CInt(e.Data(Letters.EventData.Count)), String.Empty, "s"), timer.ElapsedMilliseconds)
                timer.Restart()
        End Select
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                tracer.Flush()
                tracer.Close()
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
