﻿Public Enum EventID
    Unknown = 0
    UploadStart = 100
    UploadFinish
    UploadFileStart
    UploadFileEnd
End Enum

Public Class EventData
    Public Const UploadServer As String = "UploadServer"
    Public Const UploadPath As String = "UploadPath"
    Public Const UploadUser As String = "UploadUsername"
    Public Const UploadFile As String = "UploadFile"
    Public Const UploadMessage As String = "UploadMessage"
    Public Const Count As String = "Count"
End Class
