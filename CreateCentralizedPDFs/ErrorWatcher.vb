﻿Public Class ErrorWatcher
    Implements Watcher

    Private tracker As ErrorTracker

    Public Sub New(tracker As ErrorTracker)
        Me.tracker = tracker
    End Sub

    Public Sub HandleNotice(sender As Object, e As NotifyEventArgs) Implements Watcher.HandleNotice
        If TypeOf sender Is Letters.LetterBatcher Then
            HandleBatchNotice(e)
        ElseIf TypeOf sender Is Letters.LetterBundler Then
            HandleBundleNotice(e)
        ElseIf TypeOf sender Is Uploader Then
            HandleUploadNotice(e)
        End If
    End Sub

    Private Sub HandleBatchNotice(e As NotifyEventArgs)
        Select Case e.EventID
            Case Letters.EventID.BatchFailed
                tracker.AddError(e.[Error])
            Case Letters.EventID.BadTemplate
                tracker.AddBadTemplate(CType(e.Data(Letters.EventData.TemplateKey), Letters.TemplateKey))
            Case Letters.EventID.DroppedLetter
                tracker.AddDroppedLetter(CType(e.Data(Letters.EventData.TemplateKey), Letters.TemplateKey), CType(e.Data(Letters.EventData.LetterNumber), Long))
        End Select
    End Sub

    Private Sub HandleBundleNotice(e As NotifyEventArgs)
        Select Case e.EventID
            Case Letters.EventID.BundleFailed
                tracker.AddBadBundle(CType(e.Data(Letters.EventData.FileName), String), e.[Error])
        End Select
    End Sub

    Private Sub HandleUploadNotice(e As NotifyEventArgs)
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' Release managed resources here.
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
