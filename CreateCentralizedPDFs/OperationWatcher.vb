﻿Public Class OperationWatcher
    Implements Watcher

    Public Class Constants
        Public Const TraceSource As String = "OperationSource"
        Public Const RootPath As String = "/"

        Public Class MessageFormat
            Public Const BatchStart As String = "Starting letter batching"
            Public Const BatchDone As String = "Letter batching done"
            Public Const BatchFatalError As String = "Fatal Error" & vbCrLf & "{0}"
            Public Const BatchLoadedLetterData As String = "Loaded data for {0} letter{1}"
            Public Const BatchTemplatesLoaded As String = "Successfully loaded templates from database"
            Public Const BatchNewTemplate As String = "Processing letters for template {0}:{1}"
            Public Const BatchBadTemplate As String = "Error when parsing template {0}:{1}" & vbCrLf & "{2}"
            Public Const BatchLetterDropped As String = "Error processing letter {2} using template {0}:{1}" & vbCrLf & "{3}"
            Public Const BatchTemplateProcessed As String = "Processed {2} letter{3} for template {0}:{1}"
            Public Const BatchLettersProcessedCount As String = "Processed {0} letter{1}"
            Public Const BundlingStart As String = "Starting to make zip bundles"
            Public Const BundlingEnd As String = "Made {0} zip bundles"
            Public Const BundleCreated As String = "Zip bundle {0}: {1} letter{2}"
            Public Const BundleFailed As String = "Bundling failed on {0}" & vbCrLf & "{1}"
            Public Const UploadStart As String = "Uploading files to {2}@{0}:{1}"
            Public Const UploadFinish As String = "Uploaded {0} file{1}"
            Public Const UploadFileStart As String = "Starting to upload {0}"
            Public Const UploadFileEnd As String = "Uploaded {0}: {1}"
        End Class
    End Class

    Private tracer As TraceSource
    Private bundleCount As Integer
    Private seen As IDictionary(Of Letters.TemplateKey, Integer)
    Private lastKey As Letters.TemplateKey

    Public Sub New()
        tracer = New TraceSource(Constants.TraceSource)
    End Sub

    Public Sub HandleNotice(sender As Object, e As NotifyEventArgs) Implements Watcher.HandleNotice
        If TypeOf sender Is Letters.LetterBatcher Then
            HandleBatchNotice(e)
        ElseIf TypeOf sender Is Letters.LetterBundler Then
            HandleBundleNotice(e)
        ElseIf TypeOf sender Is Uploader Then
            HandleUploadNotice(e)
        End If
    End Sub

    Private Sub HandleBatchNotice(e As NotifyEventArgs)
        Select Case e.EventID
            Case Letters.EventID.BatchStarted
                seen = New Dictionary(Of Letters.TemplateKey, Integer)()
                lastKey = Nothing
                tracer.TraceInformation(Constants.MessageFormat.BatchStart)
            Case Letters.EventID.BatchFinished
                tracer.TraceInformation(Constants.MessageFormat.BatchDone)
            Case Letters.EventID.BatchFailed
                tracer.TraceEvent(TraceEventType.Critical, Letters.EventID.BatchFailed, Constants.MessageFormat.BatchFatalError, e.[Error])
            Case Letters.EventID.LetterDataLoaded
                tracer.TraceInformation(Constants.MessageFormat.BatchLoadedLetterData, e.Data(Letters.EventData.Count), IIf(1 = CInt(e.Data(Letters.EventData.Count)), String.Empty, "s"))
            Case Letters.EventID.TemplatesLoaded
                tracer.TraceInformation(Constants.MessageFormat.BatchTemplatesLoaded)
            Case Letters.EventID.BadTemplate
                tracer.TraceEvent(TraceEventType.Error, Letters.EventID.BadTemplate, Constants.MessageFormat.BatchBadTemplate, CType(e.Data(Letters.EventData.TemplateKey), Letters.TemplateKey).Identifier, CType(e.Data(Letters.EventData.TemplateKey), Letters.TemplateKey).Version, e.[Error])
            Case Letters.EventID.LetterCreated
                HandleSingleLetter(e)
            Case Letters.EventID.DroppedLetter
                HandleDroppedLetter(e)
            Case Letters.EventID.LettersDone
                If Not lastKey Is Nothing Then tracer.TraceInformation(Constants.MessageFormat.BatchTemplateProcessed, lastKey.Identifier, lastKey.Version, seen(lastKey), IIf(1 = seen(lastKey), String.Empty, "s"))
                tracer.TraceInformation(Constants.MessageFormat.BatchLettersProcessedCount, e.Data(Letters.EventData.Count), IIf(1 = CInt(e.Data(Letters.EventData.Count)), String.Empty, "s"))
        End Select
    End Sub

    Private Sub HandleSingleLetter(e As NotifyEventArgs)
        Dim key As Letters.TemplateKey

        key = CType(e.Data(Letters.EventData.TemplateKey), Letters.TemplateKey)
        If Not seen.ContainsKey(key) Then
            If Not lastKey Is Nothing Then tracer.TraceInformation(Constants.MessageFormat.BatchTemplateProcessed, lastKey.Identifier, lastKey.Version, seen(lastKey), IIf(1 = seen(lastKey), String.Empty, "s"))
            lastKey = key
            seen.Add(key, 0)
            tracer.TraceInformation(Constants.MessageFormat.BatchNewTemplate, key.Identifier, key.Version)
        End If
        seen(key) += 1
    End Sub

    Private Sub HandleDroppedLetter(e As NotifyEventArgs)
        Dim key As Letters.TemplateKey = CType(e.Data(Letters.EventData.TemplateKey), Letters.TemplateKey)
        tracer.TraceEvent(TraceEventType.Warning, Letters.EventID.DroppedLetter, Constants.MessageFormat.BatchLetterDropped, key.Identifier, key.Version, e.Data(Letters.EventData.LetterNumber), e.[Error])
    End Sub

    Private Sub HandleBundleNotice(e As NotifyEventArgs)
        Select Case e.EventID
            Case Letters.EventID.BundlingStarted
                bundleCount = 0
                tracer.TraceInformation(Constants.MessageFormat.BundlingStart)
            Case Letters.EventID.BundlingFinished
                tracer.TraceInformation(Constants.MessageFormat.BundlingEnd, bundleCount)
            Case Letters.EventID.BundleCreated
                tracer.TraceInformation(Constants.MessageFormat.BundleCreated, e.Data(Letters.EventData.FileName), e.Data(Letters.EventData.Count), IIf(1 = CInt(e.Data(Letters.EventData.Count)), String.Empty, "s"))
                bundleCount += 1
            Case Letters.EventID.BundleFailed
                tracer.TraceInformation(Constants.MessageFormat.BundleFailed, e.Data(Letters.EventData.FileName), e.[Error])
        End Select
    End Sub

    Private Sub HandleUploadNotice(e As NotifyEventArgs)
        Dim s As String

        Select Case e.EventID
            Case EventID.UploadStart
                s = CStr(e.Data(EventData.UploadPath))
                If String.IsNullOrWhiteSpace(s) Then s = Constants.RootPath
                tracer.TraceInformation(Constants.MessageFormat.UploadStart, e.Data(EventData.UploadServer), s, e.Data(EventData.UploadUser))
            Case EventID.UploadFinish
                tracer.TraceInformation(Constants.MessageFormat.UploadFinish, e.Data(EventData.Count), IIf(1 = CInt(e.Data(EventData.Count)), String.Empty, "s"))
            Case EventID.UploadFileStart
                tracer.TraceInformation(Constants.MessageFormat.UploadFileStart, e.Data(EventData.UploadFile))
            Case EventID.UploadFileEnd
                tracer.TraceInformation(Constants.MessageFormat.UploadFileEnd, e.Data(EventData.UploadFile), e.Data(EventData.UploadMessage))
        End Select
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                tracer.Flush()
                tracer.Close()
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
