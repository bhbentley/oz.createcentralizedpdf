﻿Imports System.Linq
Imports System.Net.Mail
Imports System.Text

Imports Framework.Logic

Public Class ErrorTracker

    Public Class Constants
        Public Class MessageFormat
            Public Const ServerInstance As String = "Server: {0}"
            Public Const GeneralError As String = "# of general errors caught: {0}"
            Public Const BrokenBundle As String = "# of broken zip bundles: {0}"
            Public Const BadTemplate As String = "# of unparsable templates: {0}"
            Public Const DroppedLetters As String = "# of dropped letters for template {1}:{2}: {0}"
        End Class
        Public Const DefaultSubject As String = "CreateCentralizedPDFs Errors"
        Public Const CentralizedLetterUserId As Long = 52
    End Class

    Private sendEnabled As Boolean
    Private badTemplates As ISet(Of Letters.TemplateKey)
    Private droppedLetters As IDictionary(Of Letters.TemplateKey, IList(Of Long))
    Private badBundles As IDictionary(Of String, Exception)
    Private errors As IList(Of Exception)
    Private em As Action(Of Exception)

    Public Sub New()
        Me.sendEnabled = False
        Me.badTemplates = New HashSet(Of Letters.TemplateKey)()
        Me.droppedLetters = New Dictionary(Of Letters.TemplateKey, IList(Of Long))()
        Me.badBundles = New Dictionary(Of String, Exception)()
        Me.errors = New List(Of Exception)()
    End Sub

    Public WriteOnly Property ReportErrorMethod As Action(Of Exception)
        Set(value As Action(Of Exception))
            em = value
        End Set
    End Property

    Public Sub EnableSend()
        sendEnabled = True
    End Sub

    Public Sub AddBadTemplate(key As Letters.TemplateKey)
        badTemplates.Add(key)
    End Sub

    Public Sub AddDroppedLetter(key As Letters.TemplateKey, number As Long)
        If Not droppedLetters.ContainsKey(key) Then droppedLetters.Add(key, New List(Of Long)())
        droppedLetters(key).Add(number)
    End Sub

    Public Sub AddBadBundle(fn As String, ex As Exception)
        If Not badBundles.ContainsKey(fn) Then badBundles.Add(fn, ex)
    End Sub

    Public Sub AddError(ex As Exception)
        errors.Add(ex)
    End Sub

    Public Sub SendEmail()
        If Not sendEnabled Then
            If Not My.Settings.AlwaysEmailOnError Then Return
        End If
        If Not AnyErrors() Then Return
        If 0 = My.Settings.MailTo.Count Then Return
        If String.IsNullOrWhiteSpace(My.Settings.MailFrom) Then Return

        Dim msg As New Email()
        Dim subject As String = My.Settings.MailSubject

        If String.IsNullOrWhiteSpace(subject) Then subject = Constants.DefaultSubject

        msg.[From] = New MailAddress(My.Settings.MailFrom)
        msg.To.Add(String.Join(",", My.Settings.MailTo.Cast(Of String)()))
        msg.Subject = subject
        msg.Body = CreateMessageContent()
        msg.PersonID = Constants.CentralizedLetterUserId
        Try
            msg.Send()
        Catch ex As Exception
            em(ex)
        End Try
    End Sub

    Private Function AnyErrors() As Boolean
        Return badTemplates.Any() Or droppedLetters.Any() Or badBundles.Any() Or errors.Any()
    End Function

    Private Function CreateMessageContent() As String
        Dim sb As New StringBuilder()

        sb.AppendFormat(Constants.MessageFormat.ServerInstance, My.Settings.FileIdentifier).AppendLine()
        If errors.Any() Then AddCountLine(sb, errors, Constants.MessageFormat.GeneralError)
        If badBundles.Any() Then AddCountLine(sb, badBundles, Constants.MessageFormat.BrokenBundle)
        If badTemplates.Any() Then AddCountLine(sb, badTemplates, Constants.MessageFormat.BadTemplate)
        AddDroppedLetterLines(sb)

        Return sb.ToString()
    End Function

    Private Sub AddCountLine(Of T)(sb As StringBuilder, col As ICollection(Of T), fmt As String)
        If 0 <> col.Count Then sb.AppendFormat(fmt, col.Count).AppendLine()
    End Sub

    Private Sub AddDroppedLetterLines(sb As StringBuilder)
        For Each kvp In droppedLetters
            sb.AppendFormat(Constants.MessageFormat.DroppedLetters, kvp.Value.Count, kvp.Key.Identifier, kvp.Key.Version).AppendLine()
        Next
    End Sub
End Class
